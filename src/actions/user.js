
import type { Action } from './types';

export const SET_USER = 'SET_USER';

export function setUser(user:Object):Action {
  // console.log('setUser');
  // console.log(user);
  return {
    type: SET_USER,
    payload: user,
  };
}
