
export type Action =
  { type: 'PUSH_NEW_ROUTE', route: string }
    | { type: 'SET_USER', name: string}

export type Dispatch = (action:Action | Array<Action>) => any;
export type GetState = () => Object;
export type PromiseAction = Promise<Action>;
