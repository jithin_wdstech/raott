import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import LoginForm from './components/LoginForm';
// import CreateAccount from './components/CreateAccount';
// import ForgotPassword from './components/ForgotPassword';
// import Otp from './components/Otp';
// import Category from './components/TreasureHunt/Category.js';
// import MapUI from './components/TreasureHunt/MapUI.js';
import HomeScreen from './components/HomeScreen';
// import Deals from './components/TreasureChest/Deals.js';

const LoginStackRouter = createStackNavigator(
  {
    LoginForm: {screen: LoginForm},
    // CreateAccount: {screen: CreateAccount},
    // ForgotPassword: {screen: ForgotPassword},
    // Otp: {screen: Otp},
    // Category: {screen: Category},
    // MapUI: {screen: MapUI},
    HomeScreen: {screen: HomeScreen},
    // Deals: {screen: Deals},
  },
  {
    initialRouteName: 'HomeScreen',
    headerMode: 'none',
  },
);

LoginStackRouter.navigationOptions = {
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};

export default LoginStackRouter;
