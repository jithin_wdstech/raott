import React from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const Dialog = (props, children) => {
  return (
    <View style={styles.containerDialog}>
      <View style={styles.dialogViewStyle}>

        <View style={styles.closeImageView}>
          {/* <Text style={{ margin:10,color:'white',textAlign:'center'}} > {props.titleText}</Text> */}
          <TouchableOpacity onPress={() => props.method('Try')}>
            <Image style={styles.closeImage} source={require('../../../img/closeicon.png')}>
            </Image>
          </TouchableOpacity>
        </View>

        <Text style={styles.dialogMsgText}>
          {props.msg}
        </Text>

        <View style={styles.dialogButtonView}>
          {/* <TouchableOpacity style={{marginRight:40}} onPress={()=>props.method('OK')}>
            <Text style={{color:'black',fontSize:10 }}> OK </Text>
            </TouchableOpacity>  */}
          <TouchableOpacity style={styles.dialogButtonTouch} onPress={() => props.method('Try')}>
            <Text style={styles.dialogButtonText}> 
              Please Try Again 
            </Text>
          </TouchableOpacity>
        </View>

      </View>
    </View>

  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};

export default Dialog;
