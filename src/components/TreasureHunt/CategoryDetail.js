import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Icon,
    Left,
    Right,
    Body,
    Text,
    ListItem,
    List, CardItem, ScrollableTab, Tabs, Tab, Card
} from "native-base";
import { Dimensions, View, TouchableOpacity, Image, ScrollView, Alert, Platform, Button } from 'react-native'
import { DrawerNavigator } from 'react-navigation';
import axios from 'axios';
import styles from "./styles";
import Moment from 'moment';
import numeral from 'numeral';
import _ from 'lodash'

const deviceWidth = Dimensions.get('window').width;

class CategoryDetail extends Component {
    constructor(props) {
        super(props);
        this.clickDebounce = _.debounce(this.click.bind(this), 1000, { 'leading': true, 'trailing': false });
    }

    click(id) {
        this.props.onCardClick(id)
    }
    render() {
        const { category, category1 } = this.props;
        const { onCardClick } = this.props;
        return (
            <View style={styles.mainViewOfRow}>
                <View>
                    <View style={styles.tabStyle}>
                        <Image
                            style={styles.logoStyle}
                            source={require('../../../img/raott_logo.png')}
                        />
                        <Text style={styles.textStyle}>{category.category}</Text>
                        <View style={styles.redLine}></View>
                        <TouchableOpacity onPress={() => this.clickDebounce(category.id)} style={styles.playNowTouch}>
                            <Text style={styles.playNowButton}>
                                PLAY NOW
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.playNowTextView}>
                            <Text style={styles.playNowText}>{category.level}</Text>
                        </View>
                    </View>
                </View>

                {category1 &&
                    <View style={{ marginLeft: 10 }} >
                        <View style={styles.tabStyle}>
                            <Image
                                style={styles.logoStyle}
                                source={require('../../../img/raott_logo.png')} />
                            <Text style={styles.textStyle}>{category1.category}</Text>
                            <View style={styles.redLine}></View>
                            <TouchableOpacity onPress={() => this.clickDebounce(category1.id)} style={styles.playNowTouch}>
                                <Text style={styles.playNowButton}>
                                    PLAY NOW
                                  </Text>
                            </TouchableOpacity>
                            <View style={styles.playNowTextView}>
                                <Text style={styles.playNowText}>{category1.level}</Text>
                            </View>
                        </View>
                    </View>
                }
            </View>
        )
    }

}
export default CategoryDetail;