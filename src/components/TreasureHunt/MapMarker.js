//Libraries
import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MapView from "react-native-maps";
import userLocation from "../../../img/userLocation.png";
import ImageLoader from "react-native-image-progress";
//Styles
import styles from "./styles";

class MapMarker extends Component {
  markerPressed() {
    if (this.props.onPress) {
      this.props.onPress(this.props);
    }
  }
  constructor() {
    super();
    this.state = { load: true };
  }
  render() {
    const {
      coordinate,
      isUserMarker,
      icon,
      iconColor,
      coin,
      clickOnCoin,
      still_image
    } = this.props;
    // console.log(coin,' **********coin ', still_image )
    return (
      <MapView.Marker
        coordinate={coordinate}
        centerOffset={{ x: 0, y: -14 }}
        onPress={() => clickOnCoin()}
      >
        {this.props.message ? (
          <Text style={{ fontSize: 25, color: "white", textAlign: "center" }}>
            {this.props.message}
          </Text>
        ) : (
          <TouchableOpacity style={{ alignItems: "center" }}>
            {((icon && icon !== "") || (coin && coin !== "")) && (
              <View
                style={{
                  alignItems: "center",
                  paddingHorizontal: 15,
                  paddingTop: 30,
                  borderRadius: 4,
                  height: 250,
                  width: 250
                }}
              >
                <View>
                  {this.state.load && (
                    <ImageLoader
                      source={{ uri: still_image }}
                      style={{ height: 200, width: 200 }}
                      //   onLoad={()=>console.log('onLoadStart still')}
                      //   onLoadStart={()=>console.log('onLoadStart still')}
                      //   onProgress={()=>console.log('onProgress still')}
                      //   onError={()=>console.log('onError still')}
                    />
                  )}
                  <ImageLoader
                    source={{ uri: icon }}
                    style={{ height: 150, width: 150 }}
                    onLoad={() => this.onLoad()}
                    //   onLoadStart={()=>console.log('onLoadStart')}
                    //   onProgress={()=>console.log('onProgress')}
                    //   onError={()=>console.log('onError')}
                  />
                </View>
              </View>
            )}
            {/* {<View style={{ width: 24, height: 24, borderWidth: 2, borderColor: 'white', borderRadius: 12, backgroundColor: '#00bcd4' }} />} */}
          </TouchableOpacity>
        )}
      </MapView.Marker>
    );
  }
  onLoad() {
    this.setState({ load: !this.state.load });
  }
}

export default MapMarker;
