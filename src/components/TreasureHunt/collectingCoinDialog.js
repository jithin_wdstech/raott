import React, { Component } from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


// const Dialog = (props) => {
class collectingCoinDialog extends Component
{
    render()
    {
    return (
        <View style={styles.containerDialog}>


            <View style={{ width: deviceWidth - 40, backgroundColor: '#f7f7f7' }}>

                <Text style={{ margin: 10, color: 'black', fontSize: 15, textAlign: 'center' }}>{this.props.msg}</Text>

                    <TouchableOpacity style={{borderColor: 'black', borderRadius: 5, borderWidth: 2, padding: 5 }} 
                    onPress={() => this.props.method()}>
                        <Text style={{ color: 'black', fontSize: 15 }}>OK</Text>
                    </TouchableOpacity>
          

            </View>
        </View>

    )}
};

collectingCoinDialog.propTypes = {
    size: PropTypes.string,
};

collectingCoinDialog.defaultProps = {
    size: 'large',
};


export default collectingCoinDialog;


