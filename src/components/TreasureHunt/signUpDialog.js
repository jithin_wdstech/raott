import React from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const Dialog = (props) => {
// console.warn(deviceWidth)
// let direction = 'row'
// let ht=0
// if(deviceWidth <= 320) {
// direction = 'column',
// ht=180
// }
// console.warn(direction)
  return (
    <View style={styles.containerDialog}>


      <View style={{ width: deviceWidth - 40, backgroundColor: '#f7f7f7' }}>
        <View style={{
          width: deviceWidth - 40, backgroundColor: '#e02d2e', flexDirection: 'row',
          justifyContent: 'space-between', height: 50, alignItems: 'center'}}>
          <Text style={{ margin: 10, color: 'white', textAlign: 'center',fontSize: 15 }} > {props.titleText}</Text>
        </View>

        <Text style={{ margin: 10, color: 'black', fontSize: 13, textAlign: 'center' }}>{props.msg}</Text>
        {/* <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'center' }}>{props.finalmsg}</Text> */}

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>

          <TouchableOpacity style={{ borderColor:'black',borderRadius:5,borderWidth:1,padding:5}} onPress={() => props.method('Cancel')}>
            <Text style={{ color: 'black', fontSize: 13 }}> {props.leftButton} </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ borderColor:'black',borderRadius:5,borderWidth:1,padding:5}} onPress={() => props.method('OK')}>
            <Text style={{ color: 'black', fontSize: 13 }}>{props.rightButton} </Text>
          </TouchableOpacity>
        </View>

      </View>
      </View>

  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};


export default Dialog;


