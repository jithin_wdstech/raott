import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Dimensions,
  TouchableHighlight,
  Text,
  ToastAndroid,
  ToolbarAndroid,
  Platform,
  Alert
} from "react-native";
import { Button, Container, Content, Icon, Toast } from "native-base";
import styles from "./styles";
import Loading from "../../Loading/";
import PhotoView from "react-native-photo-view";
import Image from "react-native-image-zoom";
import { ViewPagerZoom } from "react-native-image-zoom";
// import { Actions } from 'react-native-router-flux'

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

class ImageView extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      isLoading: true,
      timePassed: false
    };
  }

  componentDidUpdate() {
    if (!this.state.isLoading) {
      setTimeout(() => {
        this.setTimePassed();
      }, 5000);
    }
  }

  setTimePassed() {
    this.setState({ timePassed: true });
  }

  render() {
    //  console.warn(this.props.imageUrl)
    return (
      <View style={styles.container}>
        <View style={styles.cancelButtonView}>
          <View style={styles.cancelButtonInnerView}>
            <Button
              style={{ alignSelf: "center" }}
              transparent
              onPress={() => this.props.closeImageView()}
            >
              <Icon name="md-close" style={{ color: "white" }} />
            </Button>
          </View>
        </View>

        {Platform.OS === "ios" && (
          <View>
            <PhotoView
              source={{ uri: this.props.imageUrl }}
              minimumZoomScale={0.85}
              maximumZoomScale={3}
              onLoad={() => this.onLoadPhoto()}
              style={{ width: deviceWidth, height: deviceHeight - 80 }}
            />
            {!this.state.timePassed && !this.state.isLoading && (
              <Text style={styles.zoomImageText}>Double Tap to Zoom</Text>
            )}
          </View>
        )}

        {Platform.OS === "android" && (
          <Image
            source={{ uri: this.props.imageUrl }}
            style={{ width: deviceWidth, height: deviceHeight - 80 }}
            onLoad={() => this.onLoadPhotoAndroid()}
          />
        )}

        <Loading isLoading={this.state.isLoading} />
      </View>
    );
  }

  onLoadPhotoAndroid() {
    ToastAndroid.show("Double Tap to zoom", ToastAndroid.LONG);
    this.setState({ isLoading: !this.state.isLoading });
  }
  onLoadPhoto() {
    this.setState({ isLoading: !this.state.isLoading });
  }
}

export default ImageView;
