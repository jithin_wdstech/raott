import React, { Component } from "react";
import {
  View,
  ScrollView,
  Linking,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Image,
  ActivityIndicator,
  Text,
  AsyncStorage,
  WebView,
  Platform,
  PermissionsAndroid,
  BackHandler,
  Keyboard
} from "react-native";
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  ListItem,
  List,
  Card,
  CardItem,
  Header
} from "native-base";
import Loading from "../Loading/";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import styles from "./styles";
import Dialog from "./Dialog";
import ClickCoinDialog from "./ClickCoinDialog";
// import signUpDialog from './signUpDialog'
// import collectingCoinDialog from './collectingCoinDialog'
import HTML from "react-native-render-html";
//Components
import MapMarker from "./MapMarker.js";
import MapView from "react-native-maps";
import axios from "axios";
import { connect } from "react-redux";
import ImageView from "./ImageView/";
import {
  checkPermission,
  requestPermission
} from "react-native-android-permissions";
//To get device width and height
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
const ASPECT_RATIO = deviceWidth / deviceHeight;
const LATITUDE = -25.274398;
const LONGITUDE = 133.775136;
// const LATITUDE = 37.78825;//previous
// const LONGITUDE = -122.4324;//previous
// const LATITUDE=20.7687591;//india
// const LONGITUDE=73.727286;//india
// const LATITUDE_DELTA = 0.0922;//previous
// const LONGITUDE_DELTA = 0.0421;//previous
const LATITUDE_DELTA = 100;
const LONGITUDE_DELTA = 100;

// import VideoPlayer from 'react-native-video-player';
import { InternetCheck, TimeOutError } from "../common/";
import LoginForm from "../LoginForm.js";

let deviceid = "";
let func = "",
  text = "";
let region = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA
};
let clueList = [];
let ipAddress = "";
// const VIMEO_ID = '179859217';

class MapUI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      showMap: false,
      showDialog: false,
      icon: "",
      coin: "",
      clue: 0,
      clickOnCoin: false,
      coinid: "",
      huntcoinid: "",
      coinCollect: false,
      membership_status: "",
      still_image: "",
      showMessageforGuestUser: false,
      checking: false,
      reset: "",
      alreadyCollected: false,
      collectAnyway: false,
      message: "",
      showClickCoinDialog: false,
      submitClicked: false,
      coinOnceClicked: false,
      showImageUrl: "",
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      video: { width: undefined, height: undefined, duration: undefined },
      thumbnailUrl: undefined,
      videoUrl: undefined,
      isLoading: true,
      questions: "",
      clues: [],
      nullText: false,
      showContinuePlaying: false,
      membership_status: "",
      previous_question: [],
      reset: false,
      wrongLat: "",
      wrongLng: "",
      zoom: 3,
      startScroll: false,
      endScroll: false,
      newMessage: ""
    };
    this.onRegionChange = this.onRegionChange.bind(this);
    this._backAndroidPress = this.backAndroidPress.bind(this);
    this.getdeviceId();
  }

  async getdeviceId() {
    deviceid = await AsyncStorage.getItem("@deviceid:key").then(data =>
      data ? JSON.parse(data) : null
    );
  }

  componentWillMount() {
    InternetCheck();
    this.getQuestions();
  }

  componentDidMount() {
    // console.log('Category page', this.props)
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.props.navigation.goBack();
    return true;
  }

  clues(clues) {
    clueList = [];
    Object.keys(clues).map(key => {
      clueList.push(clues[key]);
    });
  }

  clickOnCoin() {
    // console.log(this.state.membership_status, 'membership_status')
    if (this.state.coinOnceClicked === false) {
      if (
        (this.state.membership_status === "0") |
        (this.state.membership_status === "null")
      ) {
        this.setState({
          showClickCoinDialog: true,
          coinOnceClicked: true,
          checking: false
        });
      } else if (this.state.membership_status === "1") {
        this.collectCoin();
      }
    }
  }

  renderClickCoinDialog() {
    let msg = "",
      finalmsg = "",
      rightButton = "",
      leftButton = "";
    if (this.state.membership_status === "null") {
      msg = `WOW! You have found 1 hidden World Raott Coin. To collect coins, you need to Sign in or Sign up to World Raotts `;
      // finalmsg = 'Click OK to signin'
      rightButton = "Sign In";
      leftButton = "Sign Up";
    } else if (this.state.membership_status === "0") {
      msg = `Warning- You will lose all your World Raott Coins when you migrate from a Guest User to a Full Member and so it is best for you to become a full member now before you collect this coin `;
      // finalmsg='Confirm to continue anyway and cancel to become a full member now.'
      rightButton = "Become a Full Member now";
      leftButton = "Collect Anyway";
    }
    return (
      <ClickCoinDialog
        method={(...a) => this.toggleModalClickCoin(...a)}
        titleText=" Treasure Hunt"
        msg={msg}
        finalmsg={finalmsg}
        rightButton={rightButton}
        leftButton={leftButton}
      />
    );
  }

  toggleModalClickCoin(status) {
    this.setState({ showClickCoinDialog: !this.state.showClickCoinDialog });
    if (status === "OK") {
      if (this.state.membership_status == "null") {
        this.navigatingLoginForm();
      } else if (this.state.membership_status === "0") {
        this.navigatingInvite();
      }
    }
    if (status === "Cancel") {
      if (this.state.membership_status == "0") {
        this.collectCoin();
      } else if (this.state.membership_status == "null") {
        this.navigatingCreateAccount();
      }
    }
  }

  renderClueList() {
    let width = deviceWidth;
    if (clueList.length > 1) {
      width = deviceWidth - 80;
    }
    return clueList.map((clue, index) => {
      return (
        <View key={index} style={{ alignItems: "center", width: width }}>
          {/* {console.log(this.state.questions.answer, 'clueAnswer')} */}
          <Text style={styles.clueText}>Clue #{index + 1} </Text>
          {clueList[index].type == "text" && (
            <View style={styles.clueTextView}>
              <HTML html={clueList[index].media} />
            </View>
          )}

          {clueList[index].type == "img" && (
            <TouchableOpacity
              onPress={() =>
                this.imageClicked(clueList[index].media.split('"')[1])
              }
              style={styles.imgClueContainer}
            >
              <Image
                onLoad={() => this.setState({ isLoading: false })}
                style={styles.imageClueStyle}
                source={{ uri: clueList[index].media.split('"')[1] }}
              />
            </TouchableOpacity>
          )}

          {clueList[index].type == "youtube" && (
            <View key={i} style={styles.videoClueContainer}>
              <WebView
                source={{
                  uri: clueList[index].media.split('src="')[1].split('"')[0]
                }}
                style={styles.videoClueStyle}
              />
            </View>
          )}
        </View>
      );
    });
  }

  imageClicked(image) {
    // console.log(image, 'imageSplit')
    this.setState({ showImageUrl: image });
  }

  closeImageView() {
    this.setState({ showImageUrl: "" });
  }

  renderPrevious() {
    return this.state.previous_question.map((question, i) => {
      return (
        <View key={i} style={{ backgroundColor: "white" }}>
          <View style={styles.previousQuestionContainer}>
            <View style={styles.previousQuestionInfo}>
              <HTML html={question.question} />
            </View>
            <View
              style={{
                backgroundColor: "black",
                width: deviceWidth - 50,
                borderColor: "rgba(216,216,216,1)",
                borderWidth: 0.2
              }}
            />
            <View
              style={{
                width: deviceWidth - 52,
                backgroundColor: "rgba(244,243,244,1)"
              }}
            >
              {!question.already ? (
                <Text
                  style={{ textAlign: "center", marginTop: 5, marginBottom: 5 }}
                >{`World Raotts to be found - ${question.raotts} `}</Text>
              ) : (
                <Text
                  style={{ textAlign: "center", marginTop: 5, marginBottom: 5 }}
                >
                  Already Solved
                </Text>
              )}
            </View>
          </View>
        </View>
      );
    });
  }
  onRegionChange(region) {
    this.setState({ region: region });
  }
  renderMapView() {
    if (this.state.showMap) {
      return (
        <View>
          <MapView
            style={styles.mapViewContainer}
            region={this.state.region}
            mapType={"hybrid"}
            scrollEnabled={false}
            pitchEnabled={false}
            // minZoomLevel={parseInt(this.state.zoom)} maxZoomLevel={parseInt(this.state.zoom)}
            zoomEnabled={false}
            rotateEnabled={false}
          >
            {Platform.OS === "ios" && !this.state.disableCoin && (
              <MapMarker
                coordinate={this.state.region}
                message={this.state.newMessage}
                icon={this.state.icon}
                coin={this.state.coin}
                clickOnCoin={() => this.clickOnCoin()}
                still_image={this.state.still_image}
              />
            )}
          </MapView>
          {Platform.OS === "android" && !this.state.disableCoin && (
            <TouchableOpacity
              onPress={() => this.clickOnCoin()}
              style={styles.coinContainerInAndroid}
            >
              {this.state.newMessage && this.state.newMessage.length > 0 ? (
                <Text
                  style={{ fontSize: 25, color: "white", textAlign: "center" }}
                >
                  {this.state.newMessage}
                </Text>
              ) : (
                <Image
                  source={{ uri: this.state.still_image }}
                  style={styles.stilleCoinImage}
                />
              )}
            </TouchableOpacity>
          )}
        </View>
      );
    } else {
      return (
        <MapView
          style={styles.mapViewContainer}
          region={this.state.region}
          mapType={"hybrid"}
          onRegionChange={this.onRegionChange}
        >
          {this.state.wrongLat != "" && (
            <MapMarker
              clickOnCoin={() => {}}
              message={this.state.newMessage}
              coordinate={this.state.region}
            />
          )}
        </MapView>
      );
    }
  }

  render() {
    // console.warn(this.state.zoom)
    if (this.state.clues) {
      this.clues(this.state.clues);
    }
    let message = "";
    if (this.state.message) {
      message = this.state.message;
    }
    // else if (this.state.checking) {
    //     message = 'Finding Hidden World Raott Coins.....'
    // }
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={styles.goBackButton}
          >
            <Icon style={styles.arrowColor} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerView}>
            <Text style={[styles.arrowColor, { textAlign: "center" }]}>
              World Raott’s Biggest Treasure Hunt
            </Text>
          </View>
        </View>
        <ScrollView
          keyboardShouldPersistTaps="always"
          style={{ backgroundColor: "white" }}
        >
          {/* <View style={{  backgroundColor: "#FFF",flexDirection:'column', marginHorizontal:10}}> */}
          {this.state.questions != "null" &&
            this.state.questions != "" &&
            this.state.questions != null && (
              <View>
                {/* **************rendering the question**************  */}
                <View style={styles.questionView}>
                  <HTML html={this.state.questions.question} />
                </View>
                {/* **************rendering the clues**************  */}

                {clueList.length > 0 && (
                  <View style={styles.clueView}>
                    {clueList.length > 1 && (
                      <MaterialIcons
                        name="keyboard-arrow-left"
                        size={30}
                        style={[styles.clueMaterialIcons, { marginLeft: 5 }]}
                      />
                    )}
                    <ScrollView style={{ marginHorizontal: 5 }} horizontal>
                      {this.renderClueList()}
                    </ScrollView>
                    {clueList.length > 1 && (
                      <MaterialIcons
                        name="keyboard-arrow-right"
                        size={30}
                        style={[styles.clueMaterialIcons, { marginRight: 5 }]}
                      />
                    )}
                  </View>
                )}
                {/* **************rendering the answer edittext**************  */}
                <View style={styles.answerInputView}>
                  <TextInput
                    style={styles.textInputView}
                    onChangeText={text => this.setState({ text })}
                    value={this.state.text}
                    placeholder="Enter Your Answer"
                    underlineColorAndroid="transparent"
                    blurOnSubmit={true}
                  />
                </View>

                {/* **************rendering the submit button**************  */}
                <TouchableOpacity
                  style={[
                    styles.submitButtonView,
                    {
                      backgroundColor: this.state.submitClicked
                        ? "#dd7c7c"
                        : "#e02d2e"
                    }
                  ]}
                  disabled={this.state.submitClicked}
                  onPress={() =>
                    this.checkAnswer(
                      this.state.questions.answer,
                      this.state.questions.id
                    )
                  }
                >
                  <Text style={styles.submitButtonText}>Submit</Text>
                </TouchableOpacity>

                {message != "" && !this.state.disableCoin && (
                  <Text style={styles.messageTextStyle}>{message}</Text>
                )}

                <View>{this.renderMapView()}</View>

                <View style={{ marginBottom: 40 }}>
                  {this.renderPrevious()}
                </View>
                {this.state.previous_question.length > 0 && (
                  <View style={{ height: 40 }} />
                )}
              </View>
            )
          // <View style={{ justifyContent: 'center', marginBottom: 10, padding: 10, backgroundColor: '#d3d3d3' }}>
          //     <Text style={{ textAlign: 'center' }}> You have already found World Raott Coins in this category. Head over to another category to find hidden World Raott Coins.</Text>
          // </View>
          }

          {this.state.collectAnyway && (
            // <Text style={{ position: 'absolute', alignSelf: 'center', marginTop: deviceHeight * 0.7, backgroundColor: 'green' }}>Collecting hidden coin...</Text>
            <View style={styles.collectCoinViewContainer}>
              <View style={styles.collectCoinContainer}>
                <View style={styles.collectCoinInnerContainer}>
                  <Text
                    style={[styles.coinCollectText, { paddingVertical: 30 }]}
                  >
                    Collecting hidden coin...
                  </Text>
                  {/* <View style={{ alignItems: 'center', justifyContent: 'space-between', padding: 10 }}>
                                        <TouchableOpacity style={{ marginRight: 20, borderColor: 'black', borderRadius: 5, borderWidth: 2, padding: 10 }}
                                            onPress={() => this.setState({ collectAnyway: !this.state.collectAnyway, checking: false })}>
                                            <Text style={{ color: 'black', fontSize: 15 }}>OK</Text>
                                        </TouchableOpacity>
                                    </View> */}
                </View>
              </View>
            </View>
          )}
          {this.state.coinCollect && (
            <View style={styles.collectCoinViewContainer}>
              <View style={styles.collectCoinContainer}>
                <View style={styles.collectCoinInnerContainer}>
                  <Text style={styles.coinCollectText}>Coin Collected</Text>
                  <View style={styles.okViewStyle}>
                    <TouchableOpacity
                      style={styles.okInnerViewStyle}
                      onPress={() =>
                        this.setState({
                          coinCollect: !this.state.coinCollect,
                          checking: false
                        })
                      }
                    >
                      <Text style={styles.okTextStyle}>OK</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          )}
          {this.state.checking && (
            // <Text style={{ position: 'absolute', alignSelf: 'center', marginTop: deviceHeight * 0.8, backgroundColor: 'transparent' }}>Coins Collected</Text>
            <View style={styles.collectCoinViewContainer}>
              <View style={styles.collectCoinContainer}>
                <View style={styles.collectCoinInnerContainer}>
                  <Text style={styles.coinCollectText}>
                    Finding Hidden World Raott Coins....
                  </Text>
                  <View style={styles.okViewStyle}>
                    <TouchableOpacity
                      style={[styles.okInnerViewStyle, { marginRight: 20 }]}
                      onPress={() =>
                        this.setState({ checking: !this.state.checking })
                      }
                    >
                      <Text style={styles.okTextStyle}>OK</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          )}
          {this.state.alreadyCollected && (
            <Text style={styles.completedQuestionsTextStyle}>
              WOW!! You have got the right answer but all World Raott Coins have
              been found in this location. Head over to another location and
              find other World Raott Coins
            </Text>
          )}
        </ScrollView>

        {this.state.questions === "null" && (
          <View style={{ flexDirection: "column", height: deviceHeight }}>
            <Text
              style={{
                textAlign: "center",
                backgroundColor: "#d3d3d3",
                width: deviceWidth
              }}
            >
              You have already found World Raott Coins in this category. Head
              over to another category to find hidden World Raott Coins.
            </Text>
            <ScrollView style={{ backgroundColor: "white" }}>
              {this.renderPrevious()}
            </ScrollView>
            <View style={{ height: 60 }} />
          </View>
        )}
        {this.state.showImageUrl !== "" && (
          <ImageView
            imageUrl={this.state.showImageUrl}
            closeImageView={() => this.closeImageView()}
          />
        )}
        {this.state.showContinuePlaying && (
          <TouchableOpacity
            onPress={() => this.getQuestions()}
            style={{
              backgroundColor: "#e02d2e",
              position: "absolute",
              bottom: 0
            }}
          >
            <View
              transparent
              style={{
                width: deviceWidth,
                alignItems: "center",
                justifyContent: "center",
                margin: 10
              }}
            >
              <Text style={{ color: "rgba(236,188,78,1)" }}>
                Continue to find more coins!
              </Text>
            </View>
          </TouchableOpacity>
        )}
        {this.state.showDialog && this.renderDialog()}
        {this.state.showClickCoinDialog && this.renderClickCoinDialog()}

        <Loading isLoading={this.state.isLoading} />
      </View>
    );
  }

  momentScrollStart() {
    // console.warn('start scroll')
    this.setState({ startScroll: true });
  }
  momentScrollEnd() {
    // console.warn('end scroll')
    this.setState({ endScroll: true });
  }

  // renderCollectingDialog() {
  //     return (<collectingCoinDialog
  //         method={() => this.setState({ collectAnyway: !this.state.collectAnyway })}
  //         msg={'Collecting hidden coin...'} />)
  // }

  renderDialog() {
    return (
      <Dialog
        method={(...a) => this.toggleModal(...a)}
        titleText=" Bad Luck!!!"
        msg={`Incorrect Answer!`}
      />
    );
  }

  toggleModal(option) {
    this.setState({
      showDialog: !this.state.showDialog
    });
    if (option == "Try") {
      this.setState({ text: "" });
    } else {
    }
  }

  checkAnswer(answer, id) {
    Keyboard.dismiss();
    // console.warn(this.state.text,'*****')
    if (this.state.text != "") {
      this.setState({ checking: true, submitClicked: true });
      // this.callContinue(false)
      if (
        answer.toLowerCase().trim() === this.state.text.toLowerCase().trim()
      ) {
        // this.setState({ collectAnyway: !this.state.collectAnyway })
        // console.warn( 'answerApi*****')
        this.answerApi(answer, id);
      } else {
        // console.warn( 'wrongAnswer*****')
        // console.warn(this.state.text, '*****')
        this.wrongAnswer();
      }
    }
  }

  wrongAnswer() {
    const SELF = this;
    // SELF.setState({ checking: true, submitClicked: true })
    axios({
      url:
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        this.state.text +
        "&key=AIzaSyB9XINuYbm6geFgneSVek0FUOcEexUVcxE",
      type: "GET",
      method: "GET"
    })
      .then(function(response) {
        let clueList = [];
        // console.log('wrongAnswer response')
        // console.log(response)

        // console.log(response.data.results[0].geometry.location);
        if (response.data.results.length > 0) {
          // if(response.data.results[0].geometry.location !='')
          const region = {
            latitude: parseFloat(
              response.data.results[0].geometry.location.lat
            ),
            longitude: parseFloat(
              response.data.results[0].geometry.location.lng
            ),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          };
          SELF.setState({
            showDialog: true,
            checking: false,
            wrongLat: response.data.results[0].geometry.location.lat,
            wrongLng: response.data.results[0].geometry.location.lng,
            submitClicked: false,
            region: region,
            zoom: 15
          });
        } else {
          SELF.setState({
            showDialog: true,
            checking: false,
            wrongLat: "",
            submitClicked: false
          });
        }
      })
      .catch(error => {
        // console.log(error);
      });
  }

  getQuestions() {
    const SELF = this;
    SELF.setState({
      text: "",
      showMap: false,
      showDialog: false,
      icon: "",
      coin: "",
      clue: 0,
      clickOnCoin: false,
      coinid: "",
      huntcoinid: "",
      coinCollect: false,
      membership_status: "",
      still_image: "",
      showMessageforGuestUser: false,
      checking: false,
      reset: "",
      collectAnyway: false,
      message: "",
      showClickCoinDialog: false,
      submitClicked: false,
      disableCoin: false,
      alreadyCollected: false,
      coinOnceClicked: false,
      newMessage: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      video: { width: undefined, height: undefined, duration: undefined },
      thumbnailUrl: undefined,
      videoUrl: undefined,
      isLoading: true,
      questions: "",
      clues: [],
      nullText: false,
      showContinuePlaying: false,
      membership_status: "",
      previous_question: [],
      reset: false
    });
    AsyncStorage.getItem("apiToken")
      .then(value => {
        if (value) {
          value = value;
        } else {
          value = "Unauthenticated-user-no-api-token";
        }
        axios({
          url:
            "https://raott.com/api/v2/TreasureHuntQuestions/" +
            SELF.props.navigation.state.params.userid +
            "/" +
            SELF.props.navigation.state.params.id,
          type: "GET",
          method: "GET",
          timeout: 60000,
          headers: {
            RaottAuth: value,
            RAOTTUNI: deviceid
          }
        })
          .then(response => {
            let clueList = [];
            // console.log('Questions response')
            // console.log(response);
            // alert(JSON.stringify(response));
            let questionList = [];
            if (response.data.questions) {
              Object.keys(response.data.questions).map(key => {
                questionList.push(response.data.questions[key]);
              });
            }

            // alert(questionList.length);

            if (response.data.clues) {
              Object.keys(response.data.clues).map(key => {
                clueList.push(response.data.clues[key]);
              });
            }
            for (i = 0; i < clueList.length; i++) {
              if (clueList[i].type == "youtube") {
                videoUrl = clueList[i].media.split('src="')[1];
                videoUrl = videoUrl.split('"')[0];
              }
            }
            console.log("response -- -- : " + JSON.stringify(response.data));
            // response.data.question = null;
            SELF.setState({
              isLoading: false,
              questions: response.data.question,
              clues: clueList,
              previous_question: questionList,
              newMessage: "",
              disableCoin:
                response.data.question === null ||
                response.data.question === "null" ||
                response.data.question === ""
                  ? false
                  : false
            });
          })
          .catch(error => {
            // console.log(error);
            TimeOutError(error, () => SELF.getQuestions());
          });
      })
      .done(); // ending statement of asyncstorage
  }

  answerApi(answer, id) {
    console.log("Test");
    const SELF = this;
    let zoomExponent;
    // console.log('ANSWER', SELF.props.navigation.state.params.userid, id, answer, deviceid)
    let userid = "";

    // SELF.setState({ checking: true, submitClicked: true })
    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.log(value);
        if (value) {
          value = value;
        } else {
          value = "Unauthenticated-user-no-api-token";
        }
        axios({
          url: "https://raott.com/api/v2/TreasureHuntAnswer/",
          type: "POST",
          method: "POST",
          timeout: 30000,
          data: {
            userid: SELF.props.navigation.state.params.userid,
            questionid: id,
            answer: answer,
            ip_address: ipAddress
          },
          headers: {
            RaottAuth: value,
            RAOTTUNI: deviceid
          }
        })
          .then(function(response) {
            console.log("update: " + JSON.stringify(response));
            let clueList = [];
            let membership_status = "";
            if (response.data.membership_status != null) {
              membership_status =
                response.data.membership_status.membership_status;
            } else {
              membership_status = "null";
            }
            let noCoin = false;
            try {
              noCoin =
                response.data.coins_location_empty.length === 0 ? true : false;
            } catch (error) {}
            if (noCoin) {
              zoomExponent = 20 - response.data.question.zoom;
              let zoomScale = Math.pow(2, zoomExponent);
              let longitudeDelta = 0,
                latitudeDelta = 0;
              if (Platform.OS === "ios") {
                (latitudeDelta =
                  0.000215 * zoomScale * (deviceWidth / 320) * 2),
                  (longitudeDelta =
                    0.000148 * zoomScale * (deviceHeight / 480) * 2);
              } else {
                (latitudeDelta = 0.000215 * zoomScale * (deviceWidth / 320)),
                  (longitudeDelta =
                    0.000148 * zoomScale * (deviceHeight / 480));
              }
              const region = {
                latitude: parseFloat(response.data.question.lat),
                longitude: parseFloat(response.data.question.lon),
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
              };
              SELF.setState({
                showMap: false,
                icon: "",
                coin: "",
                still_image: "",
                coinid: "",
                huntcoinid: "",
                membership_status: membership_status,
                message: response.data.message,
                checking: false,
                showContinuePlaying: true,
                wrongLat: "Not",
                region: region,
                zoom: response.data.question.zoom,
                newMessage: response.data.coins_location_empty
              });
            } else if (response.data.coin != null) {
              zoomExponent = 20 - response.data.question.zoom;
              let zoomScale = Math.pow(2, zoomExponent);
              let longitudeDelta = 0,
                latitudeDelta = 0;
              if (Platform.OS === "ios") {
                (latitudeDelta =
                  0.000215 * zoomScale * (deviceWidth / 320) * 2),
                  (longitudeDelta =
                    0.000148 * zoomScale * (deviceHeight / 480) * 2);
              } else {
                (latitudeDelta = 0.000215 * zoomScale * (deviceWidth / 320)),
                  (longitudeDelta =
                    0.000148 * zoomScale * (deviceHeight / 480));
              }
              const region = {
                latitude: parseFloat(response.data.question.lat),
                longitude: parseFloat(response.data.question.lon),
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
              };

              SELF.setState({
                showMap: true,
                icon: "https:" + response.data.coin.rotating_image,
                coin: response.data.coin.coin,
                still_image: "https:" + response.data.coin.still_images,
                coinid: response.data.coin.id,
                huntcoinid: response.data.coin.huntcoinid,
                membership_status: membership_status,
                message: response.data.message,
                checking: false,
                showContinuePlaying: true,
                wrongLat: "",
                region: region,
                zoom: response.data.question.zoom,
                newMessage: ""
              });
            }
          })
          .catch(function(error) {
            alert(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.answerApi(answer, id));
          });
      })
      .done(); // ending statement of asyncstorage
  }

  collectCoin() {
    const SELF = this;
    SELF.setState({ collectAnyway: !this.state.collectAnyway });
    // console.log(SELF.props.navigation.state.params.userid, 'userid')
    // console.log(SELF.state.coinid, 'coinid')
    // console.log(SELF.state.huntcoinid, 'huntcoinid')
    AsyncStorage.getItem("apiToken")
      .then(value => {
        axios({
          url: "https://raott.com/api/v2/collectcoin/",
          type: "POST",
          method: "POST",
          timeout: 30000,
          data: {
            userid: SELF.props.navigation.state.params.userid,
            coinid: SELF.state.coinid,
            huntcoinid: SELF.state.huntcoinid
          },
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            // let clueList = [];
            // console.log('collect coins')
            // console.log(response, 'response123');
            if (response.data.status == "success") {
              SELF.setState({
                clickOnCoin: false,
                coinCollect: true,
                collectAnyway: false,
                coinOnceClicked: true,
                checking: false
              });
            } else if (response.data.status.includes("already")) {
              SELF.setState({
                clickOnCoin: false,
                alreadyCollected: true,
                collectAnyway: false,
                coinOnceClicked: true
              });
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.collectCoin());
          });
      })
      .done(); // ending statement of asyncstorage
  }

  navigatingLoginForm() {
    this.props.navigation.navigate("LoginForm");
  }
  navigatingInvite() {
    this.props.navigation.navigate("Invite");
  }
  navigatingCreateAccount() {
    this.props.navigation.navigate("CreateAccount");
  }
}
function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data
});

export default connect(mapStateToProps, bindAction)(MapUI);
