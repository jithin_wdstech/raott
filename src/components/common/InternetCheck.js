import React, {Component} from 'react';
import {Alert} from 'react-native';
import {NetInfo} from 'react-native-netinfo';

export const InternetCheck = () => {
  NetInfo.isConnected.fetch().then(isConnected => {
    // console.log('First, is ' + (isConnected ? 'online' : 'offline'));
  });
  function handleFirstConnectivityChange(isConnected) {
    // console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
    if (!isConnected) {
      Alert.alert('Alert!!', 'No Internet Connection. Try Again');
    }
    NetInfo.isConnected.removeEventListener(
      'change',
      handleFirstConnectivityChange,
    );
  }
  NetInfo.isConnected.addEventListener('change', handleFirstConnectivityChange);
};
