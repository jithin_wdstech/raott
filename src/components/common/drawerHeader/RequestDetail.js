import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Left,
    Right,
    Body,
    Text,
    ListItem,
    List, CardItem, ScrollableTab, Tabs, Tab, Card
} from "native-base";
import { Dimensions, View, TouchableOpacity, Image, ScrollView, Alert, Platform, AsyncStorage } from 'react-native'
import { DrawerNavigator } from 'react-navigation';
import axios from 'axios';
import styles from "./styles";

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
let setImage = null

class RequestDetail extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { request, onAccept, onDeny } = this.props;
        var icon = request.profile_image ? { uri: request.profile_image } : require('../../../../img/dummyProfile.png');
        return (
            <Content padder>
                <Card style={styles.cardStyle}>
                    <View style={styles.mainViewStyle}>
                        <Image style={styles.profileImage} source={require('../../../../img/dummyProfile.png')} />
                        <View style={styles.innerViewStyle}>
                            <Text style={styles.userNameTextStyle}>{request.username}</Text>

                            <View style={styles.buttonViewStyle}>
                                <View style={styles.buttonInnerViewStyle}>
                                    <TouchableOpacity onPress={() => onAccept(request)}>
                                        <Text style={styles.buttonTextStyle}>Accept</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.buttonInnerViewStyle}>
                                    <TouchableOpacity onPress={() => onDeny(request)}>
                                        <Text style={styles.buttonTextStyle}>Reject</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>

                        </View>
                    </View>
                </Card>
            </Content>
        )
    }
}

export default RequestDetail;
