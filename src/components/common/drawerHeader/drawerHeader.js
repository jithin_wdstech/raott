
import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List, Card, CardItem
} from "native-base";
import { Dimensions, View, TouchableOpacity, Image, ScrollView } from 'react-native'
import { DrawerNavigator } from 'react-navigation';
import FriendRequest from './FriendRequest'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width; ``

let icon = ''

class drawerHeader extends Component {
  render() {
    const { onClickFriendRequest } = this.props
    icon = this.props.pImage ? { uri: this.props.pImage } : require('../../../../img/dummyProfile.png');
    return (
      <View>
        <Header style={{ backgroundColor: '#e7e4e5' }} >
          <Left>
            <Button
              transparent 
              onPress={() => this.props.navigation.navigate('DrawerOpen', { test: '123' })} 
            >
              <Icon name="menu" style={{ fontSize: 25,color: '#e02d2e' }} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: '#e02d2e' }}></Title>
          </Body>
          <Right style={{ alignItems: 'center', }}>
            <Button 
              onPress={() => this.props.navigation.navigate("FriendRequest")} 
              transparent  
            >
              <Icon name="md-person-add" style={{ color: '#afadae' }} />
            </Button>
          </Right>
        </Header>

        <View style={{ width: deviceWidth, height: 100, backgroundColor: '#e02d2e', alignItems: 'center' }}>
          <View>
            <Text style={{ color: 'white', marginTop: 10, fontSize: 25, alignSelf: 'center' }}>
              {this.props.username}
            </Text>
            <Text style={{ color: 'white', fontSize: 10, alignSelf: 'center' }}>
              My Raott Account
            </Text>
          </View>
        </View>

        <View style={{ alignItems: 'center', marginTop: 20, position: 'absolute', alignSelf: 'center', top: 110 }} >
          <Image style={{ width: 80, height: 80, borderRadius: 40, borderWidth: 0 }} source={icon}></Image>
        </View>

      </View>
    )
  }
  // friendRequest()
  // {
  //  this.props.navigation.navigate("FriendRequest")

  // }
}
export default drawerHeader;
