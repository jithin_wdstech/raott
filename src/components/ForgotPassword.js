import React, { Component } from 'react'
import { ButtonPress } from './common'
import { Text, View, Dimensions, Platform, Alert, TouchableOpacity } from 'react-native';
import axios from 'axios';
import { Item, Container, Content, Header, Left, Button, Icon, Input } from 'native-base';
import Loading from "./Loading/"
// import DrawerHeader from './common/drawerHeader/drawerHeader'
import { InternetCheck, TimeOutError } from './common/'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      email: '',
      isLoading: false
    }
  }
  componentWillMount() {
    InternetCheck();
  }
  render() {
    const { iconStyle, container, contentStyle, mainView, headerView,headerStyle,goBackButton,headerText,
      textInputView,itemViewStyle,inputFieldStyle } = styles
    return (
      <Container style={container}>
        <View style={headerStyle}>
          <TouchableOpacity style={goBackButton} 
          onPress={() => this.props.navigation.goBack()}>
              <Icon style={{ color: 'white' }} name="arrow-back" />
          </TouchableOpacity>
          <View style={headerText}>
            <Text style={{color:'white'}}>FORGOT PASSWORD</Text>
          </View>
        </View>
        <Content style={contentStyle}>
          <View style={mainView}>
            <View style={textInputView}>
              <Text style={{ color: '#c2c2c2' }}> Email</Text>
              <Item style={itemViewStyle}>
                <Icon active name='md-person' style={iconStyle} />
                <Input placeholder='Enter Email' 
                  placeholderTextColor='#c2c2c2' 
                  style={inputFieldStyle}
                  onChangeText={(email) => this.setState({ email })}
                  value={SELF.state.email}
                />
              </Item>
            </View>
            {this.renderButton()}
          </View>
        </Content>
        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }
  renderButton() {
    return (
      <ButtonPress onPress={() => this.submitData()} >
        Reset Password
        </ButtonPress>
    );
  }
  submitData() {
    SELF.setState({
      isLoading: true
    });
    if (SELF.state.email == '') {
      Alert.alert('Invalid Credential', 'Please enter Email')
      SELF.setState({
        isLoading: false
      });
    }
    else {
      axios({
        url: 'https://raott.com/api/Auth/forgotPassword/',
        type: 'POST',
        method: 'POST',
        timeout: 30000,
        data: {
          email: SELF.state.email
        },
      })
        .then(function (response) {
          // console.log('response forgotPassword');
          // console.log(response);
          Alert.alert(response.data.status, response.data.email)
          SELF.setState({
            isLoading: false
          });
        })
        .catch(function (error) {
          // console.log(error);
          SELF.setState({ isLoading: false })
          TimeOutError(error,()=>SELF.submitData());
        });
    }
  }
}

const styles = {
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10
  },

  container: {
    width: deviceWidth,
    backgroundColor: 'white'
  },
  contentStyle: {
    marginTop: 60,
    marginBottom: 10
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: 'white'
  },
  headerView: {
    width: deviceWidth - 40,
    alignSelf: 'center'
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flexDirection:'row'

  },
  headerTextView: {
    width: deviceWidth - 80,
    marginTop: (Platform === 'ios') ? 20 : 10,
    position: 'absolute'
  }, headerText: {
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
  goBackButton:
  {
      height:44,
      width:44,
      alignItems:'center',
      justifyContent:'center'
  },
 
  headerText:
  {  
      height:44,
      width: deviceWidth - 98,
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      marginLeft: 5
  },
  textInputView:{ 
    marginTop: 10, 
    width: deviceWidth - 40, 
    alignSelf: 'center' 
  },
  itemViewStyle:{
    marginTop: 10,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5
  },
  inputFieldStyle:{ 
    alignSelf: 'center', 
    fontSize: 11 
  } 
}