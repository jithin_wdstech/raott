import React, { Component } from 'react'
import { ButtonPress } from './common'
import {
    Text, View, Dimensions, Image, ScrollView, WebView,BackHandler
} from 'react-native'
import {
    Container, Header, Title, Content, Button, Icon, Left, Right, Body, ListItem,
    List, Card, CardItem, Item, Input
} from "native-base";
import { InternetCheck } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
class FAQ extends Component {
    constructor(props) {
        super(props);
        this._backAndroidPress = this.backAndroidPress.bind(this);
    }
    componentWillMount() {
        InternetCheck();
    }    
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this._backAndroidPress)
    }

    backAndroidPress() {

        this.props.navigation.navigate("LandingPage");
        return true;
    }
    render() {
        return (
            <Container style={styles.mainContainer}>
            <View>
                <Header style={styles.headerStyle} >
                    <Left style={{flex:1}}>
                        <Button
                            transparent onPress={() => this.props.navigation.navigate('DrawerOpen', { test: '123' })} >
                            <Icon name="menu" style={{ fontSize: 25,color: '#e02d2e' }} />
                        </Button>
                    </Left>
                    <Body style={{flex:10, alignItems:'center'}}>
                        <Title style={{ color: '#e02d2e' }}>FAQ</Title>
                    </Body>
                    <Right style={{ alignItems: 'center', }}></Right>
                </Header>
            </View>

            <WebView
            source={{uri: 'https://raott.com/raottdev/faq'}}
            style={{marginTop:10}}
          />
           </Container>
        )
    }
}
const styles = {
    mainContainer: {
        flex: 1,
        width: deviceWidth,
        backgroundColor: 'white'
    },
    headerStyle: {
        backgroundColor: '#e7e4e5'
    }
}
FAQ.navigationOptions = {
    header: null,
    gesturesEnabled: false,
    drawerLockMode: 'locked-closed'
};
export default FAQ

