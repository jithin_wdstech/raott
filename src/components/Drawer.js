/* @flow */

import React from 'react';
import {createStackNavigator, DrawerNavigator} from 'react-navigation';

import SideBar from './sidebar/';
import LandingPage from './DrawerLandingPage/LandingPage';
import LoginForm from './LoginForm';
import CreateAccount from './CreateAccount';
import RoattCoins from './RaottCollections/RoattCoins.js';
import CoinTab from './CoinTab/CoinTab.js';
import EditProfile from './EditProfile.js';
import Picker from './Picker/Picker.js';
import ViewAnimation from './ViewAnimation.js';
import AccountsDetail from './ViewTransaction/AccountsDetail.js';
import Deals from './TreasureChest/Deals.js';
import Treasure from './TreasureChest/Treasure.js';
import TransferCoins from './TransferCoinToAny/TransferCoins.js';
import DealsDetails from './TreasureChest/DealsDetails.js';
import FriendsList from './TransferCoinToFriends/FriendsList.js';
import Category from './TreasureHunt/Category.js';
import MapUI from './TreasureHunt/MapUI.js';
import RaottCoinsDetail from './RaottCollections/RaottCoinsDetail.js';
import DealHistory from './DealHistory/DealHistory.js';
import Invite from './Invite.js';
import ForgotPassword from './ForgotPassword.js';
import Otp from './Otp.js';
import TransferCoinToAny from './TransferCoinToAny/';
import FriendRequest from './common/drawerHeader/FriendRequest';
import HomeScreen from './HomeScreen';
import HomeCurrency from './HomeCurrency';
import Geomint from './Geomint';
import FAQ from './FAQ';
import ContactUs from './ContactUs';
const DrawerExample = createStackNavigator(
  {
    LandingPage: {screen: LandingPage},
    RoattCoins: {screen: RoattCoins},
    CoinTab: {screen: CoinTab},
    EditProfile: {screen: EditProfile},
    RaottCoinsDetail: {screen: RaottCoinsDetail},
    AccountsDetail: {screen: AccountsDetail},
    Picker: {screen: Picker},
    ViewAnimation: {screen: ViewAnimation},
    Deals: {screen: Deals},
    Treasure: {screen: Treasure},
    TransferCoins: {screen: TransferCoins},
    FriendsList: {screen: FriendsList},
    DealsDetails: {screen: DealsDetails},
    DealHistory: {screen: DealHistory},
    TransferCoinToAny: {screen: TransferCoinToAny},
    Invite: {screen: Invite},
    ForgotPassword: {screen: ForgotPassword},
    Otp: {screen: Otp},
    FriendRequest: {screen: FriendRequest},
    MapUI: {screen: MapUI},
    Category: {screen: Category},
    HomeScreen: {screen: HomeScreen},
    HomeCurrency: {screen: HomeCurrency},
    Geomint: {screen: Geomint},
    FAQ: {screen: FAQ},
    ContactUs: {screen: ContactUs},
  },
  {
    initialRouteName: 'LandingPage',
    // initialRouteName: "HomeScreen",
    headerMode: 'none',
  },
);

export default DrawerExample;
