import { Dimensions, Platform } from 'react-native'
const React = require("react-native");

const { StyleSheet } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "#ffffff",
    marginTop: (Platform.OS === 'ios') ? 20 : 0,



    // left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,

  },
  cardStyle: { width: deviceWidth - 40, alignSelf: 'center' },

  headerStyle: {
  
    backgroundColor: "#e02d2e",
    flexDirection: 'row',
    height: 44,

  },
  // headerTextView: {
  //   width: deviceWidth - 80,
  //   justifyContent:'center',alignItems:'center'
  // },
  // headerText: {
  //   color: "white",
  //   fontSize: 20,
  //   textAlign: "center"
  // },
  infoView:{ 
    width: deviceWidth, 
    height: 60, 
    backgroundColor: '#f6f5f6',
     justifyContent: 'center',
      alignItems: 'center' 
    },
    balanceText:{ 
      fontSize: 12, 
      color: '#b2b0b0' 
    },
    roattCoinInfo:{ 
      fontSize: 20, 
      color: '#e02d2e' 
    },
    menuMainView:{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }
  ,
  menuBarStyle: style = {
    width: deviceWidth / 3,
    height: 30,
    backgroundColor: '#f6f5f6',
    borderWidth: 1,
    borderColor: '#c7c7c7', alignItems: 'center', justifyContent: 'center', flexDirection: 'row'
  },


  textStyle: {
    color: 'rgba(160,160,160,1)'
  },

  transactionTextStyle: {
    color: 'rgba(142,142,142,1)',
    fontSize: 12,
    fontFamily:'Helvetica',
    marginLeft:2
  

  },
  transMainView:{
    flex:5,
    flexDirection:'row', 
    paddingLeft:15,
    backgroundColor:'white'
  },
  raottImage:{
    width: 40,
    alignSelf:'center', 
    height: 40
  },
  usernameStyle:{
    flex:3,
    marginTop:10,
    marginBottom:10,
    alignItems:'flex-start',
    justifyContent:'center',
    marginLeft: 10

  },
   footerStyle: {
    backgroundColor: "#f5f5f5",
    height: (Platform.OS === 'android') ? 64 : 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0
  },
  cardRightPart:{
    flex:2,
    backgroundColor:'rgba(255,255,223,1)',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
 mainView: {
    backgroundColor: 'rgba(227,227,227,1)'
  },

  searchStyle: {
    backgroundColor: 'white',
    width: deviceWidth - 20,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    margin: 10
  },

 containerDialog: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(150,150,150, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  goBackButton:
    {
        height:44,
        width:44,
        alignItems:'center',
        justifyContent:'center'
    },
   
    headerText:
    {  
        height:44,
        width: deviceWidth - 98,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 5
    },


};
