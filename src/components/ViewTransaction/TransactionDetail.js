import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,Card,CardItem
} from "native-base";
import {Dimensions, View, TouchableOpacity, Image, ScrollView, Alert, AsyncStorage} from 'react-native'
import { DrawerNavigator } from 'react-navigation';

import styles from "./styles";
import Moment from 'moment';
import numeral from 'numeral';


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const depositIcon = require('../../../img/depositIcon.png')

const withdrawalIcon = require('../../../img/withdrawalIcon.png')


 class TransactionDetail extends Component{
   
   constructor(props) 
    {
        super(props);
        this.state = { fullName: ''}
    }
 

  componentWillMount() {

  

      AsyncStorage.getItem("fullName").then((value) => {
        // console.log(value)
        
        this.setState({ fullName : value })
    }).done();
  
    
    }

    
    render(){
        let username=''
        const {dealTransaction,onCardClick } = this.props; 
        const { Username,Withdrawal,Deposit,Description, Date, Details } = dealTransaction;
        
      
           let imageName = depositIcon
           let arithmeticSymbol = '+'
           let symbolColor = 'green'
           let amount = Deposit
      
         if(Withdrawal === '-')
        {
        
            imageName = depositIcon
            arithmeticSymbol = '+'
            symbolColor = 'green'
            amount = Deposit
        }
        else if(Deposit === '-')
        {
      
           imageName = withdrawalIcon
           arithmeticSymbol = '-'
           symbolColor = 'red'
           amount = Withdrawal

        }
       
if(Username !=this.state.fullName)
{
username = Username
}
else
{
username = 'World Raott Fund'
}

    return (

      <TouchableOpacity onPress={()=> onCardClick(dealTransaction.Details,dealTransaction.Category)} >
      
				<Card style={styles.cardStyle} >
	

			<View style={styles.transMainView}>  
				   <Image style={styles.raottImage} source={imageName}/>
			<View style={styles.usernameStyle}>

            <Text style={{color:symbolColor,fontFamily:'Helvetica',textAlign:'left'}}>{username}</Text>
            <Text style={styles.transactionTextStyle}>{Description}</Text>
            <Text style={styles.transactionTextStyle}>{Moment(Date).format('D MMM YYYY ')} </Text>
       </View>

       <View style={styles.cardRightPart}>
      
      <View style={{flexDirection:'row'}}>
        <Text style={{color:symbolColor,fontFamily:'Helvetica'}}>{arithmeticSymbol}</Text>
              <View style={{}}>

                
     
                <Text style={{width:50,fontSize:11,fontFamily:'Helvetica',textAlign:'center'}}>{amount}</Text>
              </View>
        {/*<Text style={[styles.transactionTextStyle, {marginLeft:10}]}> {homeCurrencyValue} AUD</Text>*/}
        </View>
        
 {/*<TouchableOpacity style={{justifyContent:'center'}} 
 onPress={()=> onCardClick(Details)}>*/}
            {/*<View style={{marginLeft:0,backgroundColor:'rgba(0,0,0,0)'}}>
              <Text style={{color:'rgba(142,142,142,1)'}}> ></Text>
            </View>*/}
            {/*</TouchableOpacity>*/}
       </View>

      </View>	
			</Card>
  </TouchableOpacity>

);}
  }


export default TransactionDetail;
   

