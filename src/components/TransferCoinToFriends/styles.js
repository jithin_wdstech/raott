import { Dimensions, Platform } from 'react-native'

const React = require("react-native");
const { StyleSheet } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "white",
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  headerLeftBtn: {
    height:44,
    width:44,
    alignItems:'center',
    justifyContent:'center'
  },
  headerTitle: {
    height:44,
    width: deviceWidth - 170,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 5
  },
  headerRightBtn: {
    alignSelf: 'flex-end',
    backgroundColor: 'white',
    width: 70,
    height: 30,
    right: 10,
    bottom:(Platform.OS === "ios")?5:5,
    borderRadius: 20,
    flexDirection: 'row',
  },
  rightIcon: {
    backgroundColor: 'rgba(0,0,0,0)',
    left: 10,
    top: 2,
    fontSize: 25,
    color: '#e02d2e'
  },
  rightAddIcon: {
    backgroundColor: 'rgba(0,0,0,0)',
    fontSize: 20,
    top: 5,
    color: '#e02d2e',
    left: 25
  },
  cardStyle: {
    width: deviceWidth - 80,
    alignSelf: 'center'
  },
  profileImage: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    left: 10,
    borderRadius: 30
  },
  trasnferBtn: {
    height: 30,
    width: deviceWidth*0.25,
    marginTop:5,
    backgroundColor: '#e02d2e',
    borderRadius: 15,justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
   transferText: {
    color: 'white',
    fontSize: 12,
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0)'
  },
  cardRightPortion: {
    backgroundColor: 'rgba(254,254,222,1)',
    width: deviceWidth * 0.231,
    height: 80,
    position:'absolute',
    right:0,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  coinStyle: {
    width: 60,
    height: 60,
    alignSelf: 'center',
  },
  nextForward: {
    color: 'rgba(188,183,183,1)',
    alignSelf: 'center',
    left:(Platform.OS === 'ios')?0:0
  },
    containerDialog: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(150,150,150, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainerDialog:{ 
    width: deviceWidth - 40, 
    backgroundColor: '#f7f7f7' 
  },
  messageHeaderView:{
    width: deviceWidth - 40, 
    backgroundColor: '#e02d2e', 
    flexDirection: 'row',
    justifyContent: 'space-between', 
    height: 40, 
    alignItems: 'center'
  },
  headerTextStyle:{ 
    margin: 10, 
    color: 'white', 
    textAlign: 'center', 
    fontSize: 15 
  },
  cancelImageStyle:{ 
    marginRight: 10, 
    width: 20, 
    height: 20, 
    marginTop: 0, 
    justifyContent: 'space-between',
  },
  msgTextStyle:{ 
    margin: 10, 
    color: 'red', 
    fontSize: 15
  },
  okButtonTextStyle:{ 
    margin: 10, 
    color: 'black', 
    fontSize: 10, 
    textAlign: 'right' 
  },
};
