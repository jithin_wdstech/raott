import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
  Card
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import axios from "axios";
import styles from "./styles";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
let setImage = null;
import _ from "lodash";

class FriendsDetail extends Component {
  constructor(props) {
    super(props);
    button = "";
    this.clickDebounce = _.debounce(this.click.bind(this), 1000, {
      leading: true,
      trailing: false
    });
  }

  click(friend) {
    this.props.onCardClick(friend);
  }

  render() {
    const { singleFriend, onCardClick, onTransferClick } = this.props;
    var icon = singleFriend.friend.profile_image
      ? { uri: singleFriend.friend.profile_image }
      : require("../../../img/dummyProfile.png");
    if (singleFriend.request_status_text == "Friend Request Sent") {
      button = "Request Pending";
    } else if (singleFriend.request_status_text == "Friend Request Received") {
      button = "Accept";
    } else {
      button = "Transfer";
    }
    var coin = singleFriend.friend.selected_symbol;
    if (coin == "Crown") {
      setImage = require("../../../img/rsz_gold_crown.png");
    } else if (coin == "Ring") {
      setImage = require("../../../img/rsz_gold_ring.png");
    } else if (coin == "Cup") {
      setImage = require("../../../img/rsz_gold_cup.png");
    } else {
      setImage = null;
    }
    return (
      <Content padder>
        <Card style={styles.cardStyle}>
          <View style={{ flexDirection: "row", height: 80 }}>
            <Image style={styles.profileImage} source={icon} />
            <View style={{ justifyContent: "center", marginLeft: 20 }}>
              <Text
                numberOfLines={1}
                style={{ marginLeft: 5, fontSize: 14, width: 80 }}
              >
                {singleFriend.friend.username}
              </Text>
              {button == "Request Pending" ? (
                <View
                  style={{
                    height: 35,
                    width: deviceWidth * 0.25,
                    marginTop: 5,
                    backgroundColor: "#e02d2e",
                    borderRadius: 15,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 10
                  }}
                >
                  <Text numberOfLines={2} style={styles.transferText}>
                    {button}
                  </Text>
                </View>
              ) : button == "Accept" ? (
                <TouchableOpacity
                  onPress={() =>
                    onTransferClick(
                      singleFriend.friend.username,
                      singleFriend.friend.userid,
                      singleFriend.friend.membership_status,
                      "Accept"
                    )
                  }
                >
                  <View style={styles.trasnferBtn}>
                    <Text style={styles.transferText}>{button}</Text>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() =>
                    onTransferClick(
                      singleFriend.friend.username,
                      singleFriend.friend.userid,
                      singleFriend.friend.membership_status,
                      button
                    )
                  }
                >
                  <View style={styles.trasnferBtn}>
                    <Text style={styles.transferText}>{button}</Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
            <TouchableOpacity
              onPress={() => this.clickDebounce(singleFriend)}
              style={styles.cardRightPortion}
            >
              <Image style={styles.coinStyle} source={setImage} />
              <TouchableOpacity
                style={{ justifyContent: "center" }}
                onPress={() => this.clickDebounce(singleFriend)}
              >
                <Icon style={styles.nextForward} name="arrow-forward" />
              </TouchableOpacity>
            </TouchableOpacity>
          </View>
        </Card>
      </Content>
    );
  }
}

export default FriendsDetail;
