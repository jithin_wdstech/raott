import { Dimensions, Platform } from "react-native";
const React = require("react-native");
const { StyleSheet } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
export default {
  container: {
    backgroundColor: "#FFF",
    height: deviceHeight
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: Platform.OS === "ios" ? 20 : 0,
    flexDirection: "row"
  },
  imagestyle: {
    width: 90,
    height: 90,
    alignSelf: "center"
  },
  clueText: {
    textAlign: "center",
    color: "#e02d2e",
    fontSize: 15
  },
  clueTextView: {
    marginVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    width: deviceWidth - 80,
    paddingHorizontal: 5
  },
  imgClueContainer: {
    // marginVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10
    // backgroundColor: "grey"
  },
  imageClueStyle: {
    alignSelf: "center",
    height: 200,
    backgroundColor: "white",
    width: deviceWidth - 120,
    resizeMode: "contain"
    // paddingHorizontal: 80
  },
  videoClueContainer: {
    marginVertical: 20,
    // paddingHorizontal: 50,
    justifyContent: "center",
    alignItems: "center",
    height: 200
  },
  videoClueStyle: {
    alignSelf: "center",
    height: 200,
    backgroundColor: "white",
    width: deviceWidth - 120
  },
  clueView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: deviceWidth
  },
  clueMaterialIcons: {
    color: "black",
    backgroundColor: "transparent"
  },
  textTotalStyle: {
    fontSize: 18,
    color: "#e02d2e"
  },
  textAudStyle: {
    fontSize: 12,
    color: "#91908c"
  },

  tabBarLine: {
    borderBottomWidth: 4,
    borderBottomColor: "#e02d2e"
  },

  blueColor: {
    backgroundColor: "rgba(247,247,247,1)"
  },
  whiteColor: {
    color: "black"
  },

  tabBarText: {
    color: "#e02d2e"
  },
  mainView: {
    flexDirection: "row",
    width: deviceWidth,
    paddingHorizontal: 10,
    justifyContent: "space-around"
    // backgroundColor: "red"
  },
  cardStyle: {
    flexDirection: "column"
  },
  imagestyle: {
    alignSelf: "center",
    width: 120,
    height: 120,
    resizeMode: "contain"
  },
  middleViewStyle: {
    marginTop: 10,
    alignItems: "center"
  },
  progressBar: {
    borderWidth: 1,
    borderColor: "rgba(226,218,218,1)",
    marginTop: 10,
    borderRadius: 10,
    width: deviceWidth / 2.4,
    backgroundColor: "rgba(240,233,233,1)"
  },
  progressBarstyle: {
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 10,
    width: 10,
    height: 6,
    borderColor: "rgba(216,0,0,1)",
    left: 2,
    top: 1,
    backgroundColor: "rgba(224,44,45,1)"
  },
  footerMain: {
    marginTop: 10,
    width: deviceWidth * 0.445,
    padding: 10,
    alignSelf: "center",
    backgroundColor: "rgba(255,255,223,1)",
    bottom: -10
  },
  dateStyle: {
    fontSize: 10,
    textAlign: "center",
    color: "rgba(156,157,146,1)"
  },
  seperator: {
    width: deviceWidth / 2 - 50,
    alignSelf: "center",
    margin: 5,
    height: 0.5,
    backgroundColor: "rgba(156,157,146,1)"
  },
  dealstextstyle: {
    fontSize: 10,
    textAlign: "center",
    color: "rgba(156,157,146,1)"
  },
  roattText: {
    fontSize: 11,
    color: "rgba(148,12,8,1)"
  },
  combinationText: {
    fontSize: 12,
    marginTop: 10,
    color: "rgba(230,191,80,1)",
    textAlign: "center"
  },

  coin_combination_button: {
    width: deviceWidth * 0.4,
    justifyContent: "center",
    height: 30,
    borderRadius: 20,
    backgroundColor: "rgba(223,37,38,1)"
  },
  containerDialog: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(150,150,150, 0.8)",
    alignItems: "center",
    justifyContent: "center"
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: "center",
    justifyContent: "center"
  },
  headerText: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginLeft: 5
  },
  innerContainerDialog: {
    width: deviceWidth - 40,
    backgroundColor: "#f7f7f7"
  },
  closeIcon: {
    color: "rgba(160,160,160,1)",
    width: 30,
    height: 30,
    alignSelf: "flex-end",
    marginRight: 10,
    marginTop: 20,
    marginBottom: 10
  },
  dialogTextHeader: {
    marginBottom: 25,
    color: "black",
    fontSize: 12,
    textAlign: "center",
    marginHorizontal: 20
  },
  headerLineStyle: {
    backgroundColor: "black",
    width: deviceWidth - 40,
    borderColor: "rgba(216,216,216,1)",
    borderWidth: 0.2,
    marginBottom: 10,
    marginTop: 10
  },
  scrollViewStyle: {
    height: deviceHeight * 0.45,
    width: deviceWidth * 0.8
  },
  closeButtonStyle: {
    width: 80,
    borderColor: "rgba(216,216,216,1)",
    borderWidth: 1,
    margin: 10,
    color: "black",
    fontSize: 10,
    textAlign: "center",
    padding: 10,
    backgroundColor: "rgba(238,238,238,1)"
  },
  coinDetailView: {
    flexDirection: "row",
    alignSelf: "center",
    justifyContent: "center"
  },
  coinDetailText: {
    color: "black",
    fontSize: 12,
    textAlign: "center"
  },
  coinImageStyle: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginVertical: 5
  },
  confirmBuyHeader: {
    marginBottom: 15,
    color: "black",
    fontSize: 14,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  textSubHeader: {
    marginBottom: 10,
    color: "black",
    fontSize: 12,
    textAlign: "center",
    margin: 10
  },
  boughtDialogMsgView: {
    width: deviceWidth - 100,
    alignSelf: "center",
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 10
  },
  boughtDialogHeaderView: {
    width: deviceWidth - 40,
    backgroundColor: "#e02d2e",
    flexDirection: "row",
    justifyContent: "space-between",
    height: 40,
    alignItems: "center"
  }
};
