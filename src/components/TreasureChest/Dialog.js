import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ScrollView
} from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

class Dialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCoin: ""
    };
  }
  componentWillMount() {
    AsyncStorage.getItem("selectedCoin", (error, value) => {
      this.setState({ selectedCoin: value || "" });
    });
  }
  rendercoinDontHaveImages() {
    let qu = 0;
    detailData = [];
    Object.keys(this.props.details).map(key => {
      detailData.push(this.props.details[key]);
    });
    return detailData.map((detail, i) => {
      if (detail.userNot > 0)
        return (
          <View key={i}>
            <View style={styles.coinDetailView}>
              <Text style={styles.coinDetailText}>{detail.id % 100}.</Text>
              <Text style={[styles.coinDetailText, { marginLeft: 3 }]}>
                {detail.coin}
              </Text>
            </View>
            <Image
              style={{ width: 80, height: 80, alignSelf: "center" }}
              source={{ uri: "https:" + detail.image }}
            />
            {/* {
                        this.props.buttonClicked == 'coinCombination' ?
                            <Text style={{ marginBottom: 35, color: 'black', fontSize: 12, textAlign: 'center' }}>No. of coins {detail.quantity}</Text>
                            : */}
            <Text style={[styles.coinDetailText, { marginBottom: 35 }]}>
              No. of coins {detail.userNot}
            </Text>
            {/* } */}
          </View>
        );
    });
  }
  renderCoinCombinationImages() {
    let qu = 0;
    detailData = [];
    Object.keys(this.props.details).map(key => {
      detailData.push(this.props.details[key]);
    });
    return detailData.map((detail, i) => {
      // qu = detail.userNot > 0 ? detail.userNot : 0;
      return (
        <View key={i}>
          <View style={styles.coinDetailView}>
            <Text style={styles.coinDetailText}>{detail.id % 100}.</Text>
            <Text style={[styles.coinDetailText, { marginLeft: 3 }]}>
              {detail.coin}
            </Text>
          </View>
          <Image
            style={{ width: 80, height: 80, alignSelf: "center" }}
            source={{ uri: "https:" + detail.image }}
          />

          {/* {
                        this.props.buttonClicked == 'coinCombination' ? */}
          <Text style={[styles.coinDetailText, { marginBottom: 35 }]}>
            No. of coins {detail.quantity}
          </Text>
          {/* :
                            <Text style={{ marginBottom: 35, color: 'black', fontSize: 12, textAlign: 'center' }}>No. of coins {qu}</Text>

                    } */}
        </View>
      );
    });
  }

  render() {
    // console.log(this.state.selectedCoin)
    const { details, method, buttonClicked } = this.props;

    return (
      <View style={styles.containerDialog}>
        <View style={{ width: deviceWidth - 40, backgroundColor: "#f7f7f7" }}>
          <TouchableOpacity onPress={() => this.props.method()}>
            <Image
              style={{
                width: 30,
                height: 30,
                alignSelf: "flex-end",
                margin: 5,
                justifyContent: "space-between"
              }}
              source={require("../../../img/exit.png")}
            />
          </TouchableOpacity>

          <View
            style={{
              width: deviceWidth - 40,
              backgroundColor: "#f7f7f7",
              alignItems: "center",
              marginTop: 10
            }}
          >
            {this.props.buttonClicked == "coinCombination" ? (
              <Text
                style={{
                  marginBottom: 35,
                  color: "black",
                  fontSize: 15,
                  textAlign: "center",
                  margin: 10
                }}
              >
                Details of the {this.state.selectedCoin} World Raott coin
                Combination to buy this deal.
              </Text>
            ) : (
              <Text
                style={{
                  marginBottom: 35,
                  color: "black",
                  fontSize: 12,
                  textAlign: "center",
                  margin: 10
                }}
              >
                You still need to earn the following coin(s) to buy this deal.
              </Text>
            )}
            <View
              style={{
                backgroundColor: "black",
                width: deviceWidth - 40,
                borderColor: "rgba(216,216,216,1)",
                borderWidth: 0.2,
                marginBottom: 10,
                marginTop: 5
              }}
            />
            <ScrollView
              style={{ height: deviceHeight * 0.45, width: deviceWidth * 0.8 }}
            >
              {this.props.buttonClicked == "coinCombination"
                ? this.renderCoinCombinationImages()
                : this.rendercoinDontHaveImages()}
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: "black",
              width: deviceWidth - 40,
              borderColor: "rgba(216,216,216,1)",
              borderWidth: 0.2,
              marginBottom: 10,
              marginTop: 10
            }}
          />
          <TouchableOpacity onPress={() => this.props.method()}>
            <Text
              style={{
                width: 80,
                borderColor: "rgba(216,216,216,1)",
                borderWidth: 1,
                margin: 10,
                color: "black",
                fontSize: 10,
                textAlign: "center",
                padding: 10,
                backgroundColor: "rgba(238,238,238,1)"
              }}
            >
              Close
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

Dialog.propTypes = {
  size: PropTypes.string
};

Dialog.defaultProps = {
  size: "large"
};

export default Dialog;
