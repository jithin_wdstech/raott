import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body } from "native-base";
import {
  Dimensions,
  Image,
  ScrollView,
  View,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import styles from "./styles";
import axios from "axios";
import Loading from "../Loading/";
import DealsInfoDetail from "./DealsInfoDetail.js";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default class Books extends Component {
  constructor(props) {
    super(props);
    SELF = this;
  }

  onCardClick(id, index) {
    if (this.props.screen === "LandingPage") {
      this.props.navigation.navigate("DealsDetails", {
        id: id,
        onRefresh: this.props.onRefresh,
        treasure: this.props.treasure || false,
        treasureData: this.props.list[index]
      });
    } else {
      this.props.navigation.navigate("LoginForm");
    }
  }

  renderDeals() {
    let dealArray = [];
    for (i = 0; i < this.props.list.length; i += 2) {
      prod = this.props.list[i];
      prod2 = this.props.list[i + 1];
      if (prod2 != null) {
        dealArray.push(
          <DealsInfoDetail
            treasure={this.props.treasure || false}
            key={i}
            deal={prod}
            deal1={prod2}
            currentIndex={prod.itemIndex || i}
            currentIndexOne={(prod2 && prod2.itemIndex) || i + 1}
            onCardClick={(id, index) => this.onCardClick(id, index)}
            onCardCoinCombinationClick={combination =>
              this.props.onCardCoinCombinationClick(combination)
            }
          />
        );
      } else {
        dealArray.push(
          <DealsInfoDetail
            key={i}
            deal={prod}
            currentIndex={prod.itemIndex || i}
            currentIndexOne={(prod2 && prod2.itemIndex) || i + 1}
            treasure={this.props.treasure || false}
            deal1={null}
            onCardClick={(id, index) => this.onCardClick(id, index)}
            onCardCoinCombinationClick={combination =>
              this.props.onCardCoinCombinationClick(combination)
            }
          />
        );
      }
    }
    return dealArray;
  }

  render() {
    // console.warn(this.props.screen)
    return (
      <Container>
        {this.props.list.length === 0 ? (
          <View />
        ) : (
          <Content>{this.renderDeals()}</Content>
        )}
        {this.props.noDataText && (
          <View
            style={{
              width: deviceWidth,
              height: deviceHeight,
              backgroundColor: "grey"
            }}
          >
            <Text
              style={{
                textAlign: "center",
                justifyContent: "center",
                top: 20
              }}
            >
              {" "}
              {this.props.treasure ? "No treasure found" : "No deals found"}
            </Text>
          </View>
        )}
      </Container>
    );
  }
}
