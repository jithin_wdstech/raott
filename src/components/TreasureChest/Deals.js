import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  AsyncStorage,
  NetInfo,
  BackHandler
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import styles from "./styles";
import Books from "./Books.js";
import Loading from "../Loading/";
import axios from "axios";
import CoinCombinationDialog from "./CoinCombinationDialog.js";
import { InternetCheck, TimeOutError } from "../common/";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

let categoryId = [],
  categoryName = [];
class Deals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      deals: [],
      dealCategory: [],
      noDataText: false,
      coin_combination: "",
      renderCoinCombination: false
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentWillMount() {
    InternetCheck();
    //**************** api call to fetch deals ************************/
    this.getDealsData();
    this.getdealCategories();
  }

  componentDidMount() {
    // console.log('Category page', this.props)
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  onRefresh = data => {
    this.getdealCategories();
    this.getDealsData();
  };

  renderCategories() {
    let noDataText;
    // if(this.state.dealCategory && this.state.dealCategory.length)
    // {
    //  this.state.dealCategory((cat) => {
    //  categoryId.push(deal.id);
    //  categoryName.push(deal.category_name);
    //  })
    // }
    // console.log(this.state.dealCategory)
    return (
      this.state.dealCategory &&
      this.state.dealCategory != "undefined" &&
      this.state.dealCategory.map((cat, i) => {
        let dealList = [];
        this.state.deals.map((singleDeal, j) => {
          if (cat.id == singleDeal.dealCategory) {
            dealList.push({ ...singleDeal, itemIndex: j });
          }
          if (dealList.length > 0) {
            noDataText = false;
          } else {
            noDataText = true;
          }
        });
        return (
          <Tab
            key={i}
            heading={cat.category_name.toUpperCase()}
            tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}
          >
            <Books
              navigation={this.props.navigation}
              list={dealList}
              noDataText={noDataText}
              startLoader={() => this.startLoader()}
              stopLoader={() => this.stopLoader()}
              onCardCoinCombinationClick={combination =>
                this.onCardCoinCombinationClick(combination)
              }
              screen={this.props.navigation.state.params.screen}
              onRefresh={this.onRefresh}
              navigateToHistory={() =>
                this.props.navigation.navigate("DealHistory", {
                  onSelect: null
                })
              }
            />
          </Tab>
        );
      })
    );
  }

  render() {
    let noDataText;
    let ReadyToBuyList = [];
    if (this.state.deals && this.state.deals.length) {
      this.state.deals.map(deal => {
        let quantity = 0,
          userHas = 0;
        deal._coinCombination.map((coin, i) => {
          if (coin.quantity < coin.userHas) {
            quantity = quantity + parseInt(coin.quantity);
            userHas = userHas + parseInt(coin.quantity);
          } else {
            quantity = quantity + parseInt(coin.quantity);
            userHas = userHas + parseInt(coin.userHas);
          }
        });
        // ************ segregating deal type according to ids of Deals *********************
        if (userHas >= quantity) {
          ReadyToBuyList.push(deal);
        }

        // if (deal.dealCategory === '9') {
        //   BooksList.push(deal)

        // }
        // else if (deal.dealCategory === '12') {
        //   CashList.push(deal)

        // }
        // else if (deal.dealCategory === '13') {
        //   GamesList.push(deal)

        // }
        // else if (deal.dealCategory === '14') {
        //   TravelList.push(deal)

        // }
        // else if (deal.dealCategory === '15') {
        //   ElectronicsList.push(deal)

        // }
        // else if (deal.dealCategory === '17') {
        //   CoffeeList.push(deal)

        // }
      });
    }

    if (ReadyToBuyList.length > 0) {
      noDataText = false;
    } else {
      noDataText = true;
    }
    return (
      <Container style={styles.container}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            onPress={() => this.goBack()}
            style={styles.goBackButton}
          >
            <Icon style={{ color: "white" }} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerText}>
            <Text style={{ color: "white", textAlign: "center" }}>DEALS</Text>
          </View>
        </View>

        <Tabs
          tabBarBackgroundColor="white"
          tabBarUnderlineStyle={styles.tabBarLine}
          renderTabBar={() => <ScrollableTab />}
        >
          <Tab
            heading="ALL DEALS"
            tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}
          >
            <Books
              navigation={this.props.navigation}
              list={this.state.deals}
              noDataText={this.state.noDataText}
              startLoader={() => this.startLoader()}
              stopLoader={() => this.stopLoader()}
              onCardCoinCombinationClick={combination =>
                this.onCardCoinCombinationClick(combination)
              }
              screen={this.props.navigation.state.params.screen}
              onRefresh={this.onRefresh}
              navigateToHistory={() =>
                this.props.navigation.navigate("DealHistory", {
                  onSelect: null
                })
              }
            />
          </Tab>

          <Tab
            heading="READY TO BUY DEALS"
            tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}
          >
            <Books
              navigation={this.props.navigation}
              list={ReadyToBuyList}
              noDataText={noDataText}
              startLoader={() => this.startLoader()}
              onRefresh={this.onRefresh}
              stopLoader={() => this.stopLoader()}
              screen={this.props.navigation.state.params.screen}
              onCardCoinCombinationClick={combination =>
                this.onCardCoinCombinationClick(combination)
              }
              navigateToHistory={() =>
                this.props.navigation.navigate("DealHistory", {
                  onSelect: null
                })
              }
            />
          </Tab>

          {this.renderCategories()}
          {/*   <Tab
            heading="BOOKS" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={BooksList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab>
          <Tab
            heading="CASH" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={CashList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab>
          <Tab
            heading="GAMES" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={GamesList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab> */}

          {/* <Tab
            heading="TRAVEL" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={TravelList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab>

          <Tab
            heading="ELECTRONICS" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={ElectronicsList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab>

          <Tab
            heading="COFFEE" tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}>
            <Books navigation={this.props.navigation} list={CoffeeList} startLoader={() => this.startLoader()} stopLoader={() => this.stopLoader()} />
          </Tab> */}
        </Tabs>
        {this.state.renderCoinCombination && this.renderCoinCombinationDialog()}
        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }
  onCardCoinCombinationClick(combination) {
    this.setState({
      coin_combination: combination,
      renderCoinCombination: true
    });
  }
  renderCoinCombinationDialog() {
    return (
      <CoinCombinationDialog
        details={this.state.coin_combination}
        method={() => this.toggleCoinCombinationDialog()}
      />
    );
  }
  toggleCoinCombinationDialog() {
    this.setState({ renderCoinCombination: false });
  }
  startLoader() {
    this.setState({
      isLoading: true
    });
  }
  stopLoader() {
    this.setState({
      isLoading: false
    });
  }

  getdealCategories() {
    const SELF = this;
    AsyncStorage.getItem("apiToken")
      .then(value => {
        //  console.log(value);
        if (value) {
          value = value;
        } else {
          value = "raott-app-deals-public-user";
        }
        axios({
          url: "https://raott.com/api/res/dealCategories/",
          type: "GET",
          method: "GET",
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            SELF.setState({
              isLoading: false
            });
            // console.log('responseCategoryData');
            //  console.log(response.data.result);
            let dealCategoryList = [];
            Object.keys(response.data.result).map(key => {
              dealCategoryList.push(response.data.result[key]);
            });
            SELF.setState({ dealCategory: dealCategoryList });
          })
          .catch(function(error) {
            // console.log(error);
          });
      })
      .done(); // ending statement of asyncstorage
  }

  getDealsData() {
    const SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({ isLoading: true });
    }
    AsyncStorage.getItem("apiToken")
      .then(value => {
        console.log("auth token: " + value);
        if (value) {
          value = value;
        } else {
          value = "raott-app-deals-public-user";
        }
        axios({
          url: "https://raott.com/api/v2/screenDeals/",
          type: "GET",
          method: "GET",
          timeout: 30000,
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            let dealList = [];
            // let data = {
            //   deals: {
            //     1: {
            //       dealCategory: 2,
            //       _coinCombination: [{ quantity: 5, userHas: 2 }],
            //       id: 1,
            //       image: "url",
            //       title: "title",
            //       _price: {
            //         raotts: "",
            //         currency: "10"
            //       },
            //       expiry: new Date(),
            //       number_of_deals: "20"
            //     }
            //   }
            // };

            console.log("response: " + JSON.stringify(response));

            Object.keys(response.data.deals).map(key => {
              dealList.push(response.data.deals[key]);
            });
            if (dealList.length > 0) {
              SELF.setState({
                isLoading: false,
                deals: dealList
              });
            } else {
              SELF.setState({
                isLoading: false,
                noDataText: true
              });
            }
          })
          .catch(function(error) {
            // alert(error);
            SELF.setState({ isLoading: false });
            // TimeOutError(error,()=>SELF.getDealsData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

export default Deals;
