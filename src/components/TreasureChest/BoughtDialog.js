import React from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { DrawerNavigator } from 'react-navigation';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Dialog = (props, children) => {
  return (
    <View style={styles.containerDialog}>
      <View style={styles.innerContainerDialog}>
        <View style={styles.boughtDialogHeaderView}>
          <Text style={{ margin: 10, color: 'white', textAlign: 'center', fontSize: 15 }}></Text>
          <TouchableOpacity onPress={() => props.method()}>
            <Image style={{ marginRight: 10, width: 20, height: 20, marginTop: 0, justifyContent: 'space-between' }} source={require('../../../img/closeicon.png')}>
            </Image>
          </TouchableOpacity>
        </View>

        <View style={styles.boughtDialogMsgView}>
          <Text style={{ color: '#e02d2e' }}>
            Congrats. You have successfully bought this deal. We have sent you an email with the deal details.
          </Text>
          <View style={{ width: deviceWidth - 80, alignSelf: 'flex-start', flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => props.navigateToDealHistory()}>
              <Text style={{ color: "rgba(230,191,80,1)" }}>Click here </Text>
            </TouchableOpacity>
            <Text style={{ color: '#e02d2e' }}>to view deal history.</Text>
          </View>
        </View>

        <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => props.method()}>
          <Text style={styles.closeButtonStyle}> CLOSE </Text>
        </TouchableOpacity>
      </View>

    </View>
  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};


export default Dialog;


