import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
  Card
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import axios from "axios";
import styles from "./styles";
import Moment from "moment";
import numeral from "numeral";
import _ from "lodash";

const deviceWidth = Dimensions.get("window").width;

class DealsInfoDetail extends Component {
  constructor(props) {
    super(props);
    this.clickDebounce = _.debounce(this.click.bind(this), 1000, {
      leading: true,
      trailing: false
    });
  }
  click(id, index) {
    this.props.onCardClick(id, index);
  }
  render() {
    const { deal, deal1 } = this.props;
    const { onCardClick } = this.props;
    let quantity = 0;
    let userHas = 0;
    let div = 0;
    let quantity1 = 0;
    let userHas1 = 0;
    let div1 = 0;
    let divison = 0;
    let divison1 = 0;
    if (deal._coinCombination) {
      deal._coinCombination.map((coin, i) => {
        if (coin.quantity < coin.userHas) {
          quantity = quantity + parseInt(coin.quantity);
          userHas = userHas + parseInt(coin.quantity);
        } else {
          quantity = quantity + parseInt(coin.quantity);
          userHas = userHas + parseInt(coin.userHas);
        }
        // if(coin.userNot>0)
        // {
        // quantity=quantity+parseInt(coin.quantity)
        // temp =coin.quantity-coin.userNot
        // userHas=userHas+parseInt(temp)
        // }
        // else{
        //     quantity=quantity+parseInt(coin.quantity)
        //     userHas=userHas+parseInt(coin.quantity)
        // }
      });
    }

    divison = (userHas / quantity) * 100;
    div = (userHas / quantity) * (deviceWidth / 2.4);
    if (div > 0) {
      div = div;
    } else {
      div = 0;
    }
    {
      deal1 &&
        deal1._coinCombination &&
        deal1._coinCombination.map((coin, i) => {
          // quantity1=quantity1+parseInt(coin.quantity)
          // userHas1=userHas1+parseInt(coin.userHas)

          if (coin.quantity < coin.userHas) {
            quantity1 = quantity1 + parseInt(coin.quantity);
            userHas1 = userHas1 + parseInt(coin.quantity);
          } else {
            quantity1 = quantity1 + parseInt(coin.quantity);
            userHas1 = userHas1 + parseInt(coin.userHas);
          }
        });
      divison1 = (userHas1 / quantity1) * 100;
      div1 = (userHas1 / quantity1) * (deviceWidth / 2.4);
      if (div1 > 0) {
        div1 = div1;
      } else {
        div1 = 0;
      }
    }
    return (
      <View style={styles.mainView}>
        <Card style={{ width: "48%" }}>
          <TouchableOpacity
            onPress={() =>
              this.clickDebounce(
                this.props.treasure ? deal.treasure_id : deal.id,
                this.props.currentIndex
              )
            }
          >
            <CardItem style={styles.cardStyle}>
              <Image
                style={styles.imagestyle}
                source={{
                  uri: this.props.treasure
                    ? deal.media[0].image.split('"')[1]
                    : deal.image
                }}
              />
              <View style={styles.middleViewStyle}>
                <Text
                  numberOfLines={1}
                  style={{ fontSize: 18, marginBottom: 1 }}
                >
                  {this.props.treasure ? deal.treasure_title : deal.title}
                </Text>
                <Text numberOfLines={1} style={styles.roattText}>
                  R{" "}
                  {numeral(
                    this.props.treasure
                      ? deal.treasure_amount
                      : deal._price.raotts
                  ).format("0,0")}{" "}
                  (
                  {numeral(
                    this.props.treasure
                      ? deal.home_currency
                      : deal._price.currency
                  ).format("0,0.00")}{" "}
                  {
                    (this.props.treasure
                      ? deal.home_currency
                      : deal._price.currency
                    ).split(" ")[1]
                  }
                  )
                </Text>
                {!this.props.treasure && (
                  <TouchableOpacity
                    onPress={combination =>
                      this.props.onCardCoinCombinationClick(
                        deal._coinCombination
                      )
                    }
                  >
                    <Text style={styles.combinationText}>
                      View Coin Combination
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              {!this.props.treasure && (
                <View style={styles.progressBar}>
                  <View
                    style={{
                      flexDirection: "row",
                      borderRadius: 10,
                      width: div,
                      height: 20,
                      borderColor: "rgba(216,0,0,1)",
                      backgroundColor: "green"
                    }}
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      justifyContent: "center",
                      marginLeft: 10,
                      marginTop: 3,
                      fontSize: 10,
                      position: "absolute",
                      backgroundColor: "transparent"
                    }}
                  >
                    {numeral(divison).format("0,0.00")} %
                  </Text>
                </View>
              )}

              <View style={styles.footerMain}>
                <Text style={styles.dateStyle}>
                  {this.props.treasure
                    ? deal.text1
                    : `Valid till ${Moment(deal.expiry).format("D MMM YYYY ")}`}
                </Text>
                <View style={styles.seperator} />
                <Text style={styles.dealstextstyle}>
                  {this.props.treasure
                    ? deal.text2
                    : `Number of deals left ${deal.number_of_deals}`}
                </Text>
              </View>
            </CardItem>
          </TouchableOpacity>
        </Card>

        {deal1 ? (
          <Card style={{ width: "48%" }}>
            <TouchableOpacity
              onPress={() =>
                this.clickDebounce(
                  this.props.treasure ? deal1.treasure_id : deal1.id,
                  this.props.currentIndexOne
                )
              }
            >
              <CardItem style={styles.cardStyle}>
                <Image
                  style={styles.imagestyle}
                  source={{
                    uri: this.props.treasure
                      ? deal1.media[0].image.split('"')[1]
                      : deal1.image
                  }}
                />
                <View style={styles.middleViewStyle}>
                  <Text
                    numberOfLines={1}
                    style={{ fontSize: 18, marginBottom: 1 }}
                  >
                    {this.props.treasure ? deal1.treasure_title : deal1.title}
                  </Text>

                  <Text numberOfLines={1} style={styles.roattText}>
                    R{" "}
                    {numeral(
                      this.props.treasure
                        ? deal1.treasure_amount
                        : deal1._price.raotts
                    ).format("0,0")}{" "}
                    (
                    {numeral(
                      this.props.treasure
                        ? deal1.home_currency
                        : deal1._price.currency
                    ).format("0,0.00")}{" "}
                    {
                      (this.props.treasure
                        ? deal1.home_currency
                        : deal1._price.currency
                      ).split(" ")[1]
                    }
                    )
                  </Text>

                  {!this.props.treasure && (
                    <TouchableOpacity
                      onPress={combination =>
                        this.props.onCardCoinCombinationClick(
                          deal1._coinCombination
                        )
                      }
                    >
                      <Text style={styles.combinationText}>
                        View Coin Combination
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
                {/* borderWidth: 1,
    borderColor: 'rgba(226,218,218,1)',
    marginTop: 10,
    borderRadius: 10,
    width: deviceWidth/2.4,
    backgroundColor: 'rgba(240,233,233,1)' */}
                {!this.props.treasure && (
                  <View style={styles.progressBar}>
                    <View
                      style={{
                        flexDirection: "row",
                        borderRadius: 10,
                        width: div1,
                        height: 20,
                        borderColor: "rgba(216,0,0,1)",
                        backgroundColor: "green"
                      }}
                    />
                    <Text
                      numberOfLines={1}
                      style={{
                        justifyContent: "center",
                        marginTop: 3,
                        marginLeft: 10,
                        fontSize: 10,
                        position: "absolute",
                        backgroundColor: "transparent"
                      }}
                    >
                      {numeral(divison1).format("0,0.00")} %
                    </Text>
                  </View>
                )}

                <View style={styles.footerMain}>
                  <Text style={styles.dateStyle}>
                    {this.props.treasure
                      ? deal1.text1
                      : `Valid till ${Moment(deal.expiry).format(
                          "D MMM YYYY "
                        )}`}
                  </Text>
                  <View style={styles.seperator} />
                  <Text style={styles.dealstextstyle}>
                    {this.props.treasure
                      ? deal1.text2
                      : `Number of deals left ${deal1.number_of_deals}`}
                  </Text>
                </View>
              </CardItem>
            </TouchableOpacity>
          </Card>
        ) : (
          <View style={{ flex: 1 }} />
        )}
      </View>
    );
  }
}

export default DealsInfoDetail;
