import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  List,
  Card,
  Right,
  Body,
  Text,
  ListItem,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  AsyncStorage,
  NetInfo,
  BackHandler,
  WebView,
  Linking
} from "react-native";
import { HtmlText } from "react-native-html-to-text";
import { DrawerNavigator } from "react-navigation";
import styles from "./styles";
import HTML from "react-native-render-html";
import Moment from "moment";
import Loading from "../Loading/";
import Dialog from "./Dialog.js";
import { SlideButton, SlideDirection } from "../TransferCoinToAny/slide.js";
import DialogNotMember from "../TransferCoinToFriends/DialogNotMember.js";
import numeral from "numeral";
import { InternetCheck, TimeOutError } from "../common/";
import { connect } from "react-redux";
import axios from "axios";
import ConfirmBuyDeal from "./ConfirmBuyDeal.js";
import BoughtDialog from "./BoughtDialog";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ImageView from "../TreasureHunt/ImageView";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

class DealsDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deals: [],
      isLoading: true,
      viewCoins: false,
      buttonClicked: "",
      isLoadingImage: true,
      buy: true,
      errorBuyingDeal: false,
      confirmBuyDialog: false,
      boughtDeal: false,
      showImageUrl: "",
      height: 20
    };
    title = "";
    image = "";
    expiry = "";
    number_of_deals = "";
    description = "";
    _price_raotts = "";
    _price_currency = "";
    errorBuyingDeal = false;

    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentDidMount() {
    // console.log('Category page', this.props)
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
    // Getting data from previous screen
    if (this.props.navigation.state.params.treasure) {
      console.log(
        "Treasure Data: " +
          JSON.stringify([this.props.navigation.state.params.treasureData])
      );
      this.setState({
        deals: [this.props.navigation.state.params.treasureData],
        isLoading: false
      });
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  componentWillMount() {
    InternetCheck();
    if (!this.props.navigation.state.params.treasure) {
      this.getDealsDetailData(this.props.navigation.state.params.id);
    }
  }

  view_coinCombination(buttonClicked) {
    this.setState({ viewCoins: true, buttonClicked: buttonClicked });
  }
  renderDialog(_coinCombination) {
    return (
      <Dialog
        details={_coinCombination}
        buttonClicked={this.state.buttonClicked}
        method={() => this.toggleModal()}
      />
    );
  }
  toggleModal() {
    this.setState({
      viewCoins: !this.state.viewCoins
    });
  }
  toggleBuyAlertModal() {
    this.setState({
      buy: !this.state.buy
    });
  }

  toggleErrorBuyingDeal() {
    this.setState({
      errorBuyingDeal: !this.state.errorBuyingDeal
    });
  }

  renderErrorBuyingDealDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleErrorBuyingDeal()}
        titleText="Error Buying Deal"
        msg={
          "Sorry. You cannot buy this deal. You are still a guest user of Raotts. Only Full members of Raotts can buy deals using World Raott Coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member. "
        }
      />
    );
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();

    if (navigation.state.params.onRefresh) {
      navigation.state.params.onRefresh();
    }
  }

  imageClicked(image) {
    this.setState({ showImageUrl: image });
  }

  closeImageView() {
    this.setState({ showImageUrl: "" });
  }

  renderDealsList(images) {
    let width = deviceWidth;
    if (images.length > 1) {
      width = deviceWidth - 80;
    }
    // alert(JSON.stringify(images));
    return images.map((clue, index) => {
      return (
        <View key={index} style={{ width: width }}>
          {/* <TouchableOpacity
            onPress={() => this.imageClicked(clue.split('"')[1])}
            style={styles.imgClueContainer}
          >
            <Image
              onLoad={() => this.setState({ isLoading: false })}
              onLoadStart={() => this.setState({ isLoading: true })}
              style={styles.imageClueStyle}
              source={{ uri: clue.split('"')[1] }}
            />
          </TouchableOpacity> */}
          {clue.youtube ? (
            <View key={i} style={styles.videoClueContainer}>
              <WebView
                source={{
                  uri: clue.youtube.split('src="')[1].split('"')[0]
                }}
                style={styles.videoClueStyle}
              />
            </View>
          ) : (
            <TouchableOpacity
              onPress={() =>
                this.imageClicked((clue.image || clue + "").split('"')[1])
              }
              style={styles.imgClueContainer}
            >
              <Image
                onLoad={() => this.setState({ isLoading: false })}
                style={styles.imageClueStyle}
                source={{ uri: (clue.image || clue + "").split('"')[1] }}
              />
            </TouchableOpacity>
          )}
        </View>
      );
    });
  }

  renderBuyAlertDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleBuyAlertModal()}
        titleText=""
        msg={
          "Sorry, Your World Raott coins combination to buy this deal is incomplete. Please try again."
        }
      />
    );
  }
  render() {
    let add = 0,
      count = 0,
      _coinCombination = [];
    let buyDeal = true;
    let isTreasureDetails = this.props.navigation.state.params.treasure;
    this.state.deals.map(deal => {
      title = isTreasureDetails ? deal.treasure_title : deal.title;
      image = deal.image;
      expiry = Moment(deal.expiry).format("D MMM YYYY ");
      number_of_deals = isTreasureDetails ? deal.text2 : deal.number_of_deals;
      treasureType = isTreasureDetails ? deal.text1 : "";
      description = isTreasureDetails ? deal.treasure_desc : deal.description;
      _price_currency = isTreasureDetails
        ? deal.treasure_amount
        : deal._price.currency;
      _price_raotts = isTreasureDetails
        ? deal.home_currency
        : deal._price.raotts;
      _coinCombination = deal._coinCombination;
    });

    if (_coinCombination && _coinCombination.length > 0) {
      _coinCombination.map(obj => {
        add = add + parseInt(obj.userNot);
        // alert("usernot: " + parseInt(obj.userNot));
        //********************* check of coinDont Have button  */
        if (obj.userNot > 0) {
          count++;
        }
      });

      if (this.props.user && this.props.user.membership_status == 0) {
        errorBuyingDeal = true;
      } else if (add > 0) {
        buyDeal = false;
      }
    }
    return (
      <Container style={[styles.container]}>
        {this.state.deals.length > 0 && (
          <View>
            <View style={styles.headerStyle}>
              <TouchableOpacity
                onPress={() => this.goBack()}
                style={styles.goBackButton}
              >
                <Icon style={{ color: "white" }} name="arrow-back" />
              </TouchableOpacity>
              <View transparent style={styles.headerText}>
                <Text style={{ color: "white", textAlign: "center" }}>
                  {this.props.navigation.state.params.treasure
                    ? "DETAILS OF TREASURE"
                    : "DETAILS OF DEAL"}
                </Text>
              </View>
            </View>

            {this.state.deals.length > 0 &&
              this.state.deals[0].images &&
              this.state.deals[0].images.length > 0 && (
                <View style={styles.clueView}>
                  {this.state.deals[0].images.length > 1 && (
                    <MaterialIcons
                      name="keyboard-arrow-left"
                      size={30}
                      style={[styles.clueMaterialIcons, { marginLeft: 5 }]}
                    />
                  )}
                  <ScrollView
                    style={{
                      // paddingHorizontal: 15,
                      width: "100%",
                      height: deviceHeight * 0.3
                    }}
                    horizontal
                  >
                    {this.renderDealsList(this.state.deals[0].images)}
                  </ScrollView>
                  {this.state.deals[0].images.length > 1 && (
                    <MaterialIcons
                      name="keyboard-arrow-right"
                      size={30}
                      style={[styles.clueMaterialIcons, { marginRight: 5 }]}
                    />
                  )}
                </View>
              )}

            {this.state.deals.length > 0 &&
              this.state.deals[0].media &&
              this.state.deals[0].media.length > 0 && (
                <View style={styles.clueView}>
                  {this.state.deals[0].media.length > 1 && (
                    <MaterialIcons
                      name="keyboard-arrow-left"
                      size={30}
                      style={[styles.clueMaterialIcons, { marginLeft: 5 }]}
                    />
                  )}
                  <ScrollView
                    style={{
                      // paddingHorizontal: 15,
                      width: "90%",
                      height: deviceHeight * 0.3
                    }}
                    horizontal
                  >
                    {this.renderDealsList(this.state.deals[0].media)}
                  </ScrollView>
                  {this.state.deals[0].media.length > 1 && (
                    <MaterialIcons
                      name="keyboard-arrow-right"
                      size={30}
                      style={[styles.clueMaterialIcons, { marginRight: 5 }]}
                    />
                  )}
                </View>
              )}

            <View
              style={{
                flexDirection: "column",
                width: deviceWidth - 60,
                alignSelf: "center",
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  color: "rgba(223,37,38,1)",
                  textAlign: "center",
                  fontSize: 16
                }}
              >
                {title}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  marginTop: 10
                }}
              >
                {!isTreasureDetails && (
                  <Text
                    style={{
                      fontSize: 12,
                      textAlign: "center",
                      marginTop: 2,
                      color: "rgba(134,134,134,1)"
                    }}
                  >
                    {`${!isTreasureDetails ? "Valid till " : ""}${expiry}`}
                  </Text>
                )}
                {!isTreasureDetails && (
                  <View
                    style={{
                      width: 1,
                      height: 15,
                      marginLeft: 5,
                      top: 2,
                      backgroundColor: "rgba(134,134,134,1)"
                    }}
                  />
                )}
                <Text
                  style={{
                    fontSize: 12,
                    textAlign: "center",
                    marginTop: 2,
                    color: "rgba(134,134,134,1)",
                    marginLeft: 5
                  }}
                >
                  {isTreasureDetails
                    ? `${treasureType} | ${number_of_deals}`
                    : `Number of deals left ${number_of_deals}`}
                </Text>
              </View>
            </View>

            <View>
              <ScrollView
                style={{
                  height: deviceHeight * 0.4,
                  marginBottom: Platform.OS === "ios" ? 10 : 7,
                  marginTop: 15
                }}
              >
                <View
                  style={{
                    width: deviceWidth - 60,
                    alignSelf: "center",
                    padding: 10,
                    backgroundColor: "rgba(255,255,223,1)"
                  }}
                >
                  <HTML
                    html={description}
                    onLinkPress={(link, data) => {
                      Linking.canOpenURL(data)
                        .then(() => {
                          Linking.openURL(data);
                        })
                        .catch(() => {});
                    }}
                    htmlStyles={{
                      fontSize: 10,
                      borderWidth: 1,
                      color: "rgba(134,134,134,1)",
                      textAlign: "center"
                    }}
                  />
                </View>
                <View>
                  {!isTreasureDetails && (
                    <Text
                      style={{
                        marginTop: 10,
                        alignSelf: "center",
                        fontSize: 12,
                        color: "rgba(134,134,134,1)"
                      }}
                    >
                      You need the following Cup World Raott Coin{" "}
                    </Text>
                  )}
                  {!isTreasureDetails && (
                    <Text
                      style={{
                        marginTop: 10,
                        alignSelf: "center",
                        fontSize: 12,
                        color: "rgba(134,134,134,1)"
                      }}
                    >
                      combination to buy this deal.
                    </Text>
                  )}

                  <Text
                    onPress={() => {
                      if (isTreasureDetails) {
                        this.props.navigation.navigate("HomeCurrency");
                      }
                    }}
                    numberOfLines={1}
                    style={{
                      color: "rgba(223,37,38,1)",
                      textAlign: "center",
                      marginTop: 10
                    }}
                  >
                    {isTreasureDetails
                      ? "Value of Treasure : "
                      : "Deal Amount : "}
                    R{" "}
                    {numeral(
                      isTreasureDetails ? _price_currency : _price_raotts
                    ).format("0,0")}{" "}
                    (
                    {numeral(
                      isTreasureDetails ? _price_raotts : _price_currency
                    ).format("0,0.00")}{" "}
                    {
                      (isTreasureDetails
                        ? _price_raotts
                        : _price_currency
                      ).split(" ")[1]
                    }
                    )
                  </Text>
                  {count > 0 ? (
                    <View
                      style={{
                        flexDirection: "row",
                        alignSelf: "center",
                        width: deviceWidth - 60,
                        marginTop: 10
                      }}
                    >
                      <TouchableOpacity
                        style={styles.coin_combination_button}
                        onPress={() =>
                          this.view_coinCombination("coinCombination")
                        }
                      >
                        <Text
                          style={{
                            fontSize: 10,
                            color: "white",
                            alignSelf: "center",
                            textAlign: "center"
                          }}
                        >
                          View Coin Combination
                        </Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={[
                          styles.coin_combination_button,
                          { marginLeft: 10 }
                        ]}
                        onPress={() =>
                          this.view_coinCombination("coinDontHave")
                        }
                      >
                        <Text
                          style={{
                            fontSize: 10,
                            color: "white",
                            alignSelf: "center"
                          }}
                        >
                          View Coin you don't have
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    !isTreasureDetails && (
                      <TouchableOpacity
                        style={{
                          width: deviceWidth * 0.4,
                          alignSelf: "center",
                          justifyContent: "center",
                          marginTop: 10,
                          height: 30,
                          borderRadius: 20,
                          backgroundColor: "rgba(223,37,38,1)"
                        }}
                        onPress={() =>
                          this.view_coinCombination("coinCombination")
                        }
                      >
                        <Text
                          style={{
                            fontSize: 10,
                            color: "white",
                            alignSelf: "center",
                            textAlign: "center"
                          }}
                        >
                          View Coin Combination
                        </Text>
                      </TouchableOpacity>
                    )
                  )}
                </View>
              </ScrollView>
            </View>

            <View
              style={{
                width: deviceWidth / 2,
                alignItems: "center",
                alignSelf: "center",
                borderWidth: 0
              }}
            >
              {!isTreasureDetails && (
                <View
                  style={{
                    backgroundColor: "rgba(224,37,38,1)",
                    alignItems: "center",
                    width: deviceWidth * 0.5,
                    height: 35,
                    borderRadius: 20,
                    flexDirection: "row",
                    marginBottom: 20
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      justifyContent: "center",
                      marginLeft: 40,
                      fontSize: 12,
                      position: "absolute"
                    }}
                  >
                    Slide to Buy Deal >>
                  </Text>
                  <SlideButton
                    slideDirection={SlideDirection.RIGHT}
                    onSlideSuccess={() => this.buyFunc(buyDeal)}
                    width={deviceWidth * 0.4994}
                    height={35}
                    borderRadius={20}
                    successfulSlideWidth={deviceWidth * 0.2}
                  >
                    <View style={[styles.btnStyle]}>
                      <View
                        style={{
                          width: 25,
                          height: 25,
                          borderRadius: 30,
                          backgroundColor: "white",
                          marginLeft: 5,
                          marginTop: 5
                        }}
                      />
                    </View>
                  </SlideButton>
                </View>
              )}
            </View>

            {this.state.viewCoins === true &&
              this.renderDialog(_coinCombination)}
            {this.state.buy === false && this.renderBuyAlertDialog()}
            {this.state.errorBuyingDeal && this.renderErrorBuyingDealDialog()}
            {this.state.confirmBuyDialog &&
              this.renderconfirmBuyDialog(_coinCombination)}

            {this.state.boughtDeal && this.renderboughtDealDialog()}
          </View>
        )}
        {this.state.showImageUrl !== "" && (
          <ImageView
            imageUrl={this.state.showImageUrl}
            closeImageView={() => this.closeImageView()}
          />
        )}
        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }

  buyFunc(buyDeal) {
    if (errorBuyingDeal === true) {
      this.setState({ errorBuyingDeal: true });
    } else if (buyDeal === false) {
      this.setState({ buy: false });
    } else {
      this.setState({ confirmBuyDialog: true });
    }
  }
  renderboughtDealDialog() {
    return (
      <BoughtDialog
        method={() => this.toggleboughtDealDialog()}
        navigateToDealHistory={() =>
          this.props.navigation.navigate("DealHistory", { onSelect: null })
        }
      />
    );
  }
  toggleboughtDealDialog() {
    this.setState({ boughtDeal: false });
    this.goBack();
  }

  getDealsDetailData(id) {
    let idFetch = id;
    const SELF = this;
    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.log(value);
        axios({
          url:
            (this.props.navigation.state.params.treasure
              ? "https://raott.com/api/v2/Treasures/"
              : "https://raott.com/api/v2/screenDeals/") + id,
          type: "GET",
          method: "GET",
          timeout: 30000,
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            let dealData = [];
            // console.log('deal detail')
            // console.log(response);
            Object.keys(response.data.deals).map(key => {
              dealData.push(response.data.deals[key]);
            });

            console.log("Deals data: " + JSON.stringify(dealData));
            SELF.setState({ deals: dealData, isLoading: false });
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.getDealsDetailData(idFetch));
          });
      })
      .done(); // ending statement of asyncstorage
  }
  toggleConfirmBuyModal(option) {
    if (option === "YES") {
      this.buyDealApi();
    } else {
      this.setState({ confirmBuyDialog: false });
    }
  }
  renderconfirmBuyDialog(_coinCombination) {
    return (
      <ConfirmBuyDeal
        details={_coinCombination}
        _price_currency={_price_currency}
        _price_raotts={_price_raotts}
        method={option => this.toggleConfirmBuyModal(option)}
      />
    );
  }
  buyDealApi() {
    const SELF = this;
    let coins = "";
    let quantity = "";
    let dealid = "";
    SELF.setState({ isLoading: true, confirmBuyDialog: false });

    SELF.state.deals.map(deal => {
      coins = deal.coins;
      (quantity = deal.quantity), (dealid = deal.id);
    });
    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.warn(value);
        // console.warn(coins,' coins')
        // console.warn(quantity,' quantity')
        // console.warn(dealid,' dealid')
        axios({
          url: "https://raott.com/api/v2/dealBuy/",
          type: "POST",
          method: "POST",
          timeout: 30000,
          data: {
            coins: coins,
            quantity: quantity,
            dealid: dealid
          },
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            // console.warn('response Buy Deal');
            // console.warn(response.data.status);
            if (response.data.status == "success") {
              SELF.setState({ isLoading: false, boughtDeal: true });
              //  Alert.alert('', 'Congrats. You have successfully bought this deal. We have sent you an email with the deal details. Click Here to view deal history.')
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.buyDealApi());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data
});
export default connect(
  mapStateToProps,
  bindAction
)(DealsDetails);
