import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  AsyncStorage,
  NetInfo,
  BackHandler
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import styles from "./styles";
import Books from "./Books.js";
import Loading from "../Loading/";
import axios from "axios";
import CoinCombinationDialog from "./CoinCombinationDialog.js";
import { InternetCheck, TimeOutError } from "../common/";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

let categoryId = [],
  categoryName = [];
class Treasure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      deals: [],
      dealCategory: [],
      noDataText: false,
      coin_combination: "",
      renderCoinCombination: false
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentWillMount() {
    InternetCheck();
    //**************** api call to fetch deals ************************/
    this.getdealCategories();
    this.getDealsData();
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  onRefresh = data => {
    this.getdealCategories();
    this.getDealsData();
  };

  renderCategories() {
    let noDataText;
    // if(this.state.dealCategory && this.state.dealCategory.length)
    // {
    //  this.state.dealCategory((cat) => {
    //  categoryId.push(deal.id);
    //  categoryName.push(deal.category_name);
    //  })
    // }
    // console.log(this.state.dealCategory)
    return (
      this.state.dealCategory &&
      this.state.dealCategory != "undefined" &&
      this.state.dealCategory.map((cat, i) => {
        let dealList = [];
        this.state.deals.map((singleDeal, j) => {
          if (cat.id == singleDeal.treasure_category) {
            dealList.push({ ...singleDeal, itemIndex: j });
          }
        });
        if (dealList.length > 0) {
          noDataText = false;
        } else {
          noDataText = true;
        }
        return (
          <Tab
            key={i}
            heading={cat.treasure_category_name.toUpperCase()}
            tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}
          >
            <Books
              navigation={this.props.navigation}
              list={dealList}
              treasure={true}
              noDataText={noDataText}
              startLoader={() => this.startLoader()}
              stopLoader={() => this.stopLoader()}
              onCardCoinCombinationClick={combination => {}}
              screen={this.props.navigation.state.params.screen}
              onRefresh={this.onRefresh}
              navigateToHistory={() => {
                this.props.navigation.navigate("DealHistory", {
                  onSelect: null
                });
              }}
            />
          </Tab>
        );
      })
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            onPress={() => this.goBack()}
            style={styles.goBackButton}
          >
            <Icon style={{ color: "white" }} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerText}>
            <Text style={{ color: "white", textAlign: "center" }}>
              Treasures
            </Text>
          </View>
        </View>

        <Tabs
          tabBarBackgroundColor="white"
          tabBarUnderlineStyle={styles.tabBarLine}
          renderTabBar={() => <ScrollableTab />}
        >
          <Tab
            heading="All Treasures"
            tabStyle={styles.blueColor}
            textStyle={styles.whiteColor}
            activeTabStyle={styles.blueColor}
            activeTextStyle={styles.tabBarText}
          >
            <Books
              navigation={this.props.navigation}
              list={this.state.deals}
              treasure={true}
              noDataText={this.state.noDataText}
              startLoader={() => this.startLoader()}
              stopLoader={() => this.stopLoader()}
              onCardCoinCombinationClick={combination =>
                this.onCardCoinCombinationClick(combination)
              }
              screen={this.props.navigation.state.params.screen}
              onRefresh={this.onRefresh}
              navigateToHistory={() =>
                this.props.navigation.navigate("DealHistory", {
                  onSelect: null
                })
              }
            />
          </Tab>

          {this.renderCategories()}
        </Tabs>
        {/* {this.state.renderCoinCombination && this.renderCoinCombinationDialog()} */}
        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }
  onCardCoinCombinationClick(combination) {
    this.setState({
      coin_combination: combination,
      renderCoinCombination: true
    });
  }
  renderCoinCombinationDialog() {
    return (
      <CoinCombinationDialog
        details={this.state.coin_combination}
        method={() => this.toggleCoinCombinationDialog()}
      />
    );
  }
  toggleCoinCombinationDialog() {
    this.setState({ renderCoinCombination: false });
  }
  startLoader() {
    this.setState({
      isLoading: true
    });
  }
  stopLoader() {
    this.setState({
      isLoading: false
    });
  }

  getdealCategories() {
    const SELF = this;
    AsyncStorage.getItem("apiToken")
      .then(value => {
        //  console.log(value);
        if (value) {
          value = value;
        } else {
          value = "raott-app-deals-public-user";
        }
        axios({
          url: "https://raott.com/api/v2/TreasureCategories/",
          type: "GET",
          method: "GET",
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            SELF.setState({
              isLoading: false
            });

            // console.log('responseCategoryData');
            //  console.log(response.data.result);
            // alert(JSON.stringify(response.data.treasure_categories));
            let dealCategoryList = [];
            if (response.data.status === "success") {
              response.data.treasure_categories.map(data => {
                dealCategoryList.push(data);
              });

              // alert(JSON.stringify(dealCategoryList));
              SELF.setState({ dealCategory: dealCategoryList });
            }
          })
          .catch(function(error) {
            // alert(JSON.stringify(error));
            // console.log(error);
          });
      })
      .done(); // ending statement of asyncstorage
  }

  getDealsData() {
    let SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({ isLoading: true });
    }
    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.log(value);
        if (value) {
          value = value;
        } else {
          value = "raott-app-deals-public-user";
        }
        axios({
          url: "https://raott.com/api/v2/Treasures/",
          type: "GET",
          method: "GET",
          timeout: 30000,
          headers: {
            RaottAuth: value
          }
        })
          .then(response => {
            let dealList = [];
            if (response.data.status === "success") {
              response.data.treasures_list.map(data => {
                dealList.push(data);
              });
            }

            if (dealList.length > 0) {
              this.setState({
                isLoading: false,
                deals: dealList,
                noDataText: false
              });
            } else {
              this.setState({
                isLoading: false,
                noDataText: true,
                noDataText: true
              });
            }
          })
          .catch(error => {
            // console.log(error);
            SELF.setState({ isLoading: false });
            // TimeOutError(error,()=>SELF.getDealsData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

export default Treasure;
