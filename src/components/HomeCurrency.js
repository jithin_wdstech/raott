import React, { Component } from 'react'
import { ButtonPress } from './common'
import {
    Text, View, Dimensions, Image, ScrollView,BackHandler
} from 'react-native'
import {
    Container, Header, Title, Content, Button, Icon, Left, Right, Body, ListItem,
    List, Card, CardItem, Item, Input
} from "native-base";
import { InternetCheck } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
class HomeCurrency extends Component {
    constructor(props) {
        super(props);
        this._backAndroidPress = this.backAndroidPress.bind(this);
    }
    componentWillMount() {
        InternetCheck();
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this._backAndroidPress)
    }

    backAndroidPress() {

        this.props.navigation.navigate("LandingPage");
        return true;
    }
    render() {
        return (
            <Container style={styles.mainContainer}>
                <View>
                    <Header style={styles.headerStyle} >
                        <Left style={{ flex: 1 }}>
                            <Button
                                transparent onPress={() => this.props.navigation.navigate('DrawerOpen', { test: '123' })} >
                                <Icon name="menu" style={{ fontSize: 25,color: '#e02d2e' }} />
                            </Button>
                        </Left>
                        <Body style={{ flex: 10, alignItems: 'center' }}>
                            <Title style={{ color: '#e02d2e', }}>Home Currency</Title>
                        </Body>
                        {/* <Right style={{ alignItems: 'center', }}></Right> */}
                    </Header>
                </View>

                <View style={styles.textViewStyle}>
                    <Text style={styles.textHeaderStyle}>
                        Important Note:
                    </Text>
                    <View style={styles.textContainer}>
                        <Text>{'\u2022'}</Text>
                        <Text style={styles.textStyle}>
                            The Company has denominated the value of Raott as 1 World Raott = 1 USD. However, Raott shall have an approximate value of home currency of each member.
                        </Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>{'\u2022'}</Text>
                        <Text style={styles.textStyle}>
                            The value of Raott coin denominated by the Company is only for the purpose of trading on deals, opening treasure chests and any other activities within the game, i.e. World Raott’s Biggest Treasure Hunt; and we do not provide for encashment of World Raott coins.
	                    </Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>{'\u2022'}</Text>
                        <Text style={styles.textStyle}>
                            Players are unable to convert World Raott Coins to cash unless it has been offered in the Treasure Chest or Deals section. World Raott Coins can be redeemed towards any of the available deals listed in the Treasure Chest or Deals section, and, only upon building the specific coin combination as required for that deal.
                        </Text>
                    </View>
                </View>
            </Container>

        )
    }
}
const styles = {
    mainContainer: {
        flex: 1,
        width: deviceWidth,
        backgroundColor: 'white'
    },
    headerStyle: {
        backgroundColor: '#e7e4e5'
    },
    textViewStyle: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10
    },
    textHeaderStyle: {
        color: '#e02d2e',
        fontSize: 18,
        lineHeight: 24,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20,
        // textDecorationLine: 'underline'
    },
    textStyle: {
        fontSize: 14,
        lineHeight: 18,
        marginLeft:8, 
        marginBottom: 10,
        textAlign: 'justify'
    },
    textContainer:{ 
        marginLeft: 15, 
        marginRight: 20, 
        flexDirection: 'row' 
    }
}

HomeCurrency.navigationOptions = {
    header: null,
    gesturesEnabled: false,
    drawerLockMode: 'locked-closed'
};
export default HomeCurrency

