import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Text,
  Right,
  Left,
  Body,
  TabHeading,
  ScrollableTab
} from "native-base";
import {
  View,
  Dimensions,
  AsyncStorage,
  Alert,
  ScrollView,
  BackHandler,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import axios from "axios";
import Common from "./Common.js";
import BalanceDetail from "./BalanceDetail";
import { SlideButton, SlideDirection } from "../TransferCoinToAny/slide.js";
import Dialog from "./Dialog.js";
import Loading from "../Loading/";
import array from "lodash/array";
import TransferCoinDialog from "./TransferCoinDialog.js";
import numeral from "numeral";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const blankCoin = require("../../../img/blank_coin.png");

let headerTitle = "";
let screen = "";
let oneRaott = "";
let currencyTransferBalance = "";
let catid = "";
let fid = "";
objArray = [];

class CoinTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coins: [],
      id: "",
      qty: "",
      isVisibleDialog: false,
      isLoading: true,
      currency: "",
      raotts: "",
      transferConfirmationDialog: false,
      isClearQty: true,
      noOfcoins: "",
      coinId: "",
      currentTab: 0,
      index: 0
    };
    isVisibleDialog = false;
    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentWillMount() {
    this.getWorldRaottsData();
  }

  componentDidMount() {
    console.log("Category page", this.props);
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  coinTransferClick(id, qty, image, coin) {
    this.setState({ id: id, qty: qty, isClearQty: false });

    index = array.findIndex(objArray, { id: id });
    if (index >= 0) {
      array.remove(objArray, [index]);
      objArray[index] = { id: id, qty: qty, image: image, coin: coin };
    } else {
      objArray.push({ id: id, qty: qty, image: image, coin: coin });
    }
  }

  onCardClick(rotating_image, coin, _raotts, id, index) {
    isVisibleDialog = true;
    this.setState({
      rotating_image: rotating_image,
      coin: coin,
      isVisibleDialog: true,
      noOfcoins: _raotts,
      coinId: id,
      index: index
    });
  }
  renderDialog() {
    return (
      <Dialog
        rotating_image={this.state.rotating_image}
        coin={this.state.coin}
        noOfcoins={this.state.noOfcoins}
        coinId={this.state.coinId}
        method={() => this.toggleModal()}
        index={this.state.index}
      />
    );
  }
  toggleModal() {
    this.setState({
      isVisibleDialog: !this.state.isVisibleDialog
    });
  }

  renderTransferConfirmationDialog() {
    let qtySum = 0;
    headerTitle = this.props.navigation.state.params.headerTitle;

    objArray.map(obj => {
      qtySum = qtySum + parseInt(obj.qty);
    });

    currencyTransferBalance = parseFloat(qtySum) * parseFloat(oneRaott);
    currencyTransferBalance = numeral(currencyTransferBalance).format("0,0.00");
    return (
      <TransferCoinDialog
        method={(...a) => this.toggleTranferConfirmation(...a)}
        titleText="Confirmation"
        coins={objArray}
        oneRaott={oneRaott}
        msg={`Are you sure you want to transfer ${qtySum} World Raott coins (${currencyTransferBalance} ${
          this.props.navigation.state.params.currency
        }) to${headerTitle.split("to")[1]} ?`}
      />
    );
  }

  toggleTranferConfirmation(choose) {
    if (choose == "Cancel") {
      objArray = [];
      this.setState({
        transferConfirmationDialog: !this.state.transferConfirmationDialog,
        isClearQty: true
      });
      // this.getWorldRaottsData();
    } else if (choose == "More") {
      this.setState({
        transferConfirmationDialog: !this.state.transferConfirmationDialog
      });
    } else if (choose == "Yes") {
      this.setState({
        transferConfirmationDialog: !this.state.transferConfirmationDialog
      });
      this.TransferRaotts();
    } else {
      this.setState({
        transferConfirmationDialog: !this.state.transferConfirmationDialog
      });
    }
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    objArray = [];
    if (navigation.state.params.onSelectTab) {
      navigation.state.params.onSelectTab();
    }
  }

  renderCoins(list, type) {
    let add = 0;
    if (type === "common") {
      add = 0;
    } else if (type === "rare") {
      add = 5;
    } else {
      add = 15;
    }

    if (fid == "") {
      return list.map((singleCoin, i) => (
        <Common
          key={i}
          isClearQty={this.state.isClearQty}
          singleCoin={singleCoin}
          screen={screen}
          catid={catid}
          collectionid={collectionid}
          coinTransferClick={(id, qty, image, coin) =>
            this.coinTransferClick(id, qty, image, coin)
          }
        />
      ));
    } else {
      return list.map((singleCoin, i) => (
        <Common
          key={i}
          isClearQty={this.state.isClearQty}
          singleCoin={singleCoin}
          screen={screen}
          onCardClick={(rotating_image, coin, _raotts, id, index) =>
            this.onCardClick(rotating_image, coin, _raotts, id, index)
          }
          index={i + add}
          coinTransferClick={(id, qty, image, coin) =>
            this.coinTransferClick(id, qty, image, coin)
          }
        />
      ));
    }
  }

  currentactiveTab(tabNumber) {
    isVisibleDialog = false;
    // this.setState({isVisibleDialog:false})
    // console.warn(tabNumber.i)
    // console.log(tabNumber)
  }

  render() {
    let swipe = false;
    if (screen == "FriendList") {
      swipe = true;
    }

    // console.log(this.state.currentTab,'TAB')

    let commonList = [];
    let rareList = [];
    let extremelyRareList = [];
    let raottBalance = 0;
    {
      this.state.coins &&
        this.state.coins.map((singleCoin, i) => {
          raottBalance = raottBalance + parseInt(singleCoin._raotts);

          if (i < 5) {
            commonList.push(singleCoin);
          } else if (i >= 5 && i < 15) {
            rareList.push(singleCoin);
          } else if (i > 14) {
            extremelyRareList.push(singleCoin);
          }
        });
    }

    headerTitle = this.props.navigation.state.params.headerTitle;
    screen = this.props.navigation.state.params.screen;
    catid = this.props.navigation.state.params.catid;
    fid = this.props.navigation.state.params.fid;
    oneRaott = parseFloat(this.state.currency) / parseFloat(this.state.raotts);

    const black = "black";
    return (
      <Container>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            style={styles.goBackButton}
            transparent
            onPress={() => this.goBack()}
          >
            <Icon
              style={{ color: "#e02d2e", alignSelf: "center" }}
              name="arrow-back"
            />
          </TouchableOpacity>

          <View style={styles.headerText}>
            <Text
              style={{
                color: "#e02d2e",
                textAlign: "center",
                fontSize: 14,
                fontWeight: "bold"
              }}
            >
              {headerTitle}
            </Text>
          </View>
        </View>
        <View style={{ backgroundColor: "white", flex: 1, width: deviceWidth }}>
          <Tabs
            locked={swipe}
            tabBarUnderlineStyle={styles.tabBarLine}
            // onChangeTab={(tab)=>this.currentactiveTab(tab)}
            style={{ elevation: 3, borderWidth: 0 }}
          >
            <Tab
              heading="Common"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}
            >
              {screen == "FriendList" && this.state.coins.length > 0 && (
                <BalanceDetail
                  currency={this.state.currency}
                  raotts={this.state.raotts}
                />
              )}

              <Content style={{ flex: 1 }}>
                {this.renderCoins(commonList, "common")}
              </Content>

              {screen == "FriendList" &&
                !this.state.transferConfirmationDialog &&
                !this.state.transferConfirmationDialog && (
                  <View
                    style={{
                      width: deviceWidth / 2,
                      alignItems: "center",
                      alignSelf: "center",
                      marginTop: 10,
                      borderWidth: 0,
                      zIndex: 2
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "rgba(224,37,38,1)",
                        alignItems: "center",
                        width: deviceWidth * 0.5,
                        height: 35,
                        marginBottom: 10,
                        borderRadius: 20,
                        flexDirection: "row"
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          textAlign: "center",
                          justifyContent: "center",
                          marginLeft: 40,
                          fontSize: 12,
                          position: "absolute"
                        }}
                      >
                        Slide to Transfer >>
                      </Text>
                      <SlideButton
                        slideDirection={SlideDirection.RIGHT}
                        onSlideSuccess={this.TransferRaotts.bind(this)}
                        width={deviceWidth * 0.4994}
                        height={35}
                        borderRadius={20}
                        successfulSlideWidth={deviceWidth * 0.2}
                      >
                        <View style={[styles.btnStyle]}>
                          <View
                            style={{
                              width: 25,
                              height: 25,
                              borderRadius: 30,
                              backgroundColor: "white",
                              marginLeft: 5,
                              marginTop: 5
                            }}
                          />
                        </View>
                      </SlideButton>
                    </View>
                  </View>
                )}

              {/* {
              this.state.isVisibleDialog  &&
              this.renderDialog()
            } */}
              {this.state.transferConfirmationDialog &&
                this.renderTransferConfirmationDialog()}

              <Loading isLoading={this.state.isLoading} />
            </Tab>
            <Tab
              heading="Rare"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}
            >
              {screen == "FriendList" && this.state.coins.length > 0 && (
                <BalanceDetail
                  currency={this.state.currency}
                  raotts={this.state.raotts}
                />
              )}

              <ScrollView style={{ flex: 1 }}>
                {this.renderCoins(rareList, "rare")}
              </ScrollView>

              {screen == "FriendList" &&
                !this.state.transferConfirmationDialog && (
                  <View
                    style={{
                      width: deviceWidth / 2,
                      alignItems: "center",
                      alignSelf: "center",
                      marginTop: 10,
                      borderWidth: 0,
                      zIndex: 2
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "rgba(224,37,38,1)",
                        alignItems: "center",
                        width: deviceWidth * 0.5,
                        height: 35,
                        marginBottom: 10,
                        borderRadius: 20,
                        flexDirection: "row"
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          textAlign: "center",
                          justifyContent: "center",
                          marginLeft: 40,
                          fontSize: 12,
                          position: "absolute"
                        }}
                      >
                        Slide to Transfer >>
                      </Text>
                      <SlideButton
                        slideDirection={SlideDirection.RIGHT}
                        onSlideSuccess={this.TransferRaotts.bind(this)}
                        width={deviceWidth * 0.4994}
                        height={35}
                        borderRadius={20}
                        successfulSlideWidth={deviceWidth * 0.2}
                      >
                        <View style={[styles.btnStyle]}>
                          <View
                            style={{
                              width: 25,
                              height: 25,
                              borderRadius: 30,
                              backgroundColor: "white",
                              marginLeft: 5,
                              marginTop: 5
                            }}
                          />
                        </View>
                      </SlideButton>
                    </View>
                  </View>
                )}

              {/* {
              this.state.isVisibleDialog &&
              this.renderDialog()
            } */}
              {this.state.transferConfirmationDialog &&
                this.renderTransferConfirmationDialog()}

              <Loading isLoading={this.state.isLoading} />
            </Tab>

            <Tab
              heading="Extremely Rare"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}
            >
              {screen == "FriendList" && this.state.coins.length > 0 && (
                <BalanceDetail
                  currency={this.state.currency}
                  raotts={this.state.raotts}
                />
              )}

              <ScrollView style={{ flex: 1 }}>
                {this.renderCoins(extremelyRareList, "extremelyRare")}
              </ScrollView>

              {screen == "FriendList" &&
                !this.state.transferConfirmationDialog && (
                  <View
                    style={{
                      width: deviceWidth / 2,
                      alignItems: "center",
                      alignSelf: "center",
                      marginTop: 10,
                      borderWidth: 0,
                      zIndex: 2
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "rgba(224,37,38,1)",
                        alignItems: "center",
                        width: deviceWidth * 0.5,
                        height: 35,
                        marginBottom: 10,
                        borderRadius: 20,
                        flexDirection: "row"
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          textAlign: "center",
                          justifyContent: "center",
                          marginLeft: 40,
                          fontSize: 12,
                          position: "absolute"
                        }}
                      >
                        Slide to Transfer >>
                      </Text>
                      <SlideButton
                        slideDirection={SlideDirection.RIGHT}
                        onSlideSuccess={this.TransferRaotts.bind(this)}
                        width={deviceWidth * 0.4994}
                        height={35}
                        borderRadius={20}
                        successfulSlideWidth={deviceWidth * 0.2}
                      >
                        <View style={[styles.btnStyle]}>
                          <View
                            style={{
                              width: 25,
                              height: 25,
                              borderRadius: 30,
                              backgroundColor: "white",
                              marginLeft: 5,
                              marginTop: 5
                            }}
                          />
                        </View>
                      </SlideButton>
                    </View>
                  </View>
                )}

              {/* {
              this.state.isVisibleDialog  &&
              this.renderDialog()
            } */}
              {this.state.transferConfirmationDialog &&
                this.renderTransferConfirmationDialog()}

              <Loading isLoading={this.state.isLoading} />
            </Tab>
          </Tabs>

          {/* {this.renderDialog()} */}
        </View>
        {this.state.isVisibleDialog && this.renderDialog()}
      </Container>
    );
  }

  TransferRaotts() {
    const SELF = this;
    if (this.state.transferConfirmationDialog) {
      SELF.setState({ isLoading: true });
      let idList = "",
        qtyList = "";
      i = 0;
      objArray.map(obj => {
        if (i == 0) {
          idList = `${obj.id}`;
          qtyList = `${obj.qty}`;
        } else {
          idList = `${idList},${obj.id}`;
          qtyList = `${qtyList},${obj.qty}`;
        }
        i += 1;
      });

      AsyncStorage.getItem("apiToken")
        .then(value => {
          console.log(value);

          // console.log('fid', fid)
          // console.log('collectionid', this.state.collectionId)
          // console.log('cat', catid)
          // console.log('coins', idList)
          // console.log('qty', qtyList)

          axios({
            url: "https://raott.com/api/v2/screenTransferRaotts/",
            type: "POST",
            method: "POST",
            timeout: 30000,
            data: {
              fid: fid,
              collectionid: this.state.collectionId,
              cat: catid,
              coins: idList,
              qty: qtyList
            },
            headers: {
              RaottAuth: value
            }
          })
            .then(function(response) {
              // console.log('response TransferRaotts');
              // console.log(response);
              if (response.data.status == "success") {
                SELF.setState({ isLoading: false, isClearQty: true });
                objArray = [];

                SELF.getWorldRaottsData();

                Alert.alert(
                  "Confirmation",
                  "World Raott coins have been transferred "
                );
              } else {
                Alert.alert("Error", "No coins selected to transfer");
              }
            })
            .catch(function(error) {
              // console.log(error);

              SELF.setState({ isLoading: false });
              TimeOutError(error, () => SELF.TransferRaotts());
            });
        })
        .done(); // ending statement of asyncstorage
    } else {
      let qtySum = 0;

      objArray.map(obj => {
        qtySum = qtySum + parseInt(obj.qty);
      });

      if (qtySum > 0) {
        SELF.setState({ transferConfirmationDialog: true });
      } else {
        Alert.alert(
          "",
          "Please select at least one World Raott Coin to transfer"
        );
      }
    }
  }

  getWorldRaottsData() {
    const SELF = this;
    SELF.setState({ isLoading: true });

    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.log(value);

        axios({
          url: "https://raott.com/api/v2//screenWorldRaotts/2/" + catid,
          type: "GET",
          method: "GET",
          timeout: 30000,
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            let coinsList = [];
            // console.log('World Raotts')
            // console.log(response);
            Object.keys(response.data.coins).map(key => {
              coinsList.push(response.data.coins[key]);
            });

            SELF.setState({
              coins: coinsList,
              collectionId: response.data.collectionid,
              isLoading: false,
              currency: response.data.currency,
              raotts: response.data.raotts,
              isClearQty: true
            });
          })
          .catch(function(error) {
            // console.log(error);

            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.getWorldRaottsData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

//<Icon name="coin" />
export default CoinTab;
