
import { View, ActivityIndicator, Dimensions,Text,Button,TouchableOpacity,Image, ScrollView } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


class TransferCoinDialog extends Component {

 
  renderImages() {

    return this.props.coins.map((detail, i) =>
        <View key={i} style={{ marginBottom: 20 ,alignSelf:'center',alignItems:'center'}}>
{  detail.qty !== 0 &&  
            <View>
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'black', fontSize: 12, textAlign: 'center' }}>{detail.id % 100}.</Text>
                <Text style={{ color: 'black', fontSize: 12, textAlign: 'center', marginLeft: 3 }}>{detail.coin}</Text>
            </View>
            <Image style={{ width: 60, height: 60, alignSelf: 'center',marginTop:2,marginBottom:2 }} source={{ uri: 'https:' + detail.image }}>
            </Image>
            <Text style={{ margin: 1, color: 'black', fontSize: 10, textAlign: 'center' }}>No. of coins {detail.qty}</Text>
            </View>  }
            </View>   
         );
}


  render(){
    const { coins, method } =this.props;

    const scrollViewHeight = this.props.coins && this.props.coins.length && this.props.coins.length > 1 ? deviceHeight * 0.35 : deviceHeight * 0.2; 
  
  return (
    <View style={styles.containerDialog}>


      <View style={{width:deviceWidth-40, backgroundColor: '#f7f7f7',alignItems:'center', justifyContent:'center'}}>
   <View style={{width:deviceWidth-40,backgroundColor:'#e02d2e',flexDirection:'row', 
       
        justifyContent: 'space-between',height:50,alignItems:'center'}}>
   <Text style={{ margin:10,color:'white',textAlign:'center',fontSize:15}} > {this.props.titleText}</Text>

    <TouchableOpacity onPress={()=>this.props.method()}>
      <Image style={{marginRight:10,width:20,height:20,marginTop:0 ,justifyContent:'space-between'}} source={require('../../../img/closeicon.png')}>
       
    
       </Image>
    
    </TouchableOpacity>
   </View>
   <ScrollView style={{height:scrollViewHeight,width:deviceWidth - 40}}>
   <View style={{marginTop:10,alignItems:'center',justifyContent:'center',alignSelf:'center'}}>
   {this.renderImages()}
        </View>
        </ScrollView>
        <ScrollView>
        <Text style={{marginBottom:5,marginHorizontal:10,color:'black',fontSize:13 }}>{this.props.msg}</Text>
        <TouchableOpacity onPress={()=>this.props.method('More')}>
            <Text style={{borderColor:'rgba(216,216,216,1)',borderWidth:1,backgroundColor:'rgba(238,238,238,1)',padding:5,
            margin:10,color:'black',fontSize:13, textAlign:'center',alignSelf:'center' }}> No, I would like to add more World Raott Coins </Text>
            </TouchableOpacity> 
     
        <TouchableOpacity onPress={()=>this.props.method('Yes')}>
            <Text style={{borderColor:'rgba(216,216,216,1)',borderWidth:1,backgroundColor:'rgba(238,238,238,1)',padding:5,
            margin:10,color:'black',fontSize:13, textAlign:'center',alignSelf:'center' }}> Yes, Transfer Now </Text>
            </TouchableOpacity> 

      <TouchableOpacity style={{marginBottom:10}} onPress={()=>this.props.method('Cancel')} >
            <Text style={{borderColor:'rgba(216,216,216,1)',borderWidth:1,backgroundColor:'rgba(238,238,238,1)',padding:5,margin:5,
            color:'black',fontSize:13, textAlign:'center',alignSelf:'center'}}> Cancel Transfer </Text>
            </TouchableOpacity> 
            {/* <View style={{margin:5}} /> */}
            </ScrollView>
      </View>
    
    </View>
  );
}
};

TransferCoinDialog.propTypes = {
  size: PropTypes.string,
};

TransferCoinDialog.defaultProps = {
  size: 'large',
};


export default TransferCoinDialog;


