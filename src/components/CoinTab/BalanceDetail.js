import React, { Component } from "react";

import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Text,
  Right,
  Left,
  Body,
  TabHeading,  ScrollableTab
} from "native-base";
import {View,Dimensions} from 'react-native'
import styles from './styles'

import Common from "./Common.js";
import numeral from 'numeral';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class BalanceDetail extends Component {
  // eslint-disable-line
  	constructor(props){
		super(props)
		this.state = {
			coin: 0,
			noOfCoins: 0,
		}
	}

  render() {


  
    return(
      
      <View>
        {this.state.coin === 0 &&
            <View style={{width:deviceWidth,height:60,borderBottomColor:'rgba(220,220,220,1)',borderBottomWidth:1,alignSelf:'center',justifyContent:'center'}}>
              <Text style={{color:'grey',fontSize:11,textAlign:'center'}}> Raott Coin Balance is </Text>
              <Text numberOfLines={1} style={{color:'#e02d2e',fontSize:16,textAlign:'center'}}>R {numeral(this.props.raotts).format('0,0')} ({numeral(this.props.currency).format('0,0.00')}{this.props.currency.split(' ')[1]})</Text>


        </View>
        }
       </View>   
         
    );
  }
}



