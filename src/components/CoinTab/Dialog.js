import React, { Component } from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import Image from 'react-native-image-progress';

class Dialog extends Component {
    constructor(props) {
        super(props);
 
    }

renderImages() {
  
    return (
         <View><Image style={{ width: 120, height: 120, alignSelf: 'center',marginTop:10 }} 
          /* indicator={ProgressBar}   */
          source={{ uri:'https:'+ this.props.rotating_image }}>
                        </Image></View>
                         
);
  }
    render() {
      
       const { rotating_image , coin,noOfcoins,coinId, index} = this.props;

        return (
           
            <View style={styles.containerDialog}>

	
                <View style={{ width: deviceWidth - 40, backgroundColor: '#f7f7f7' }}>
                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',margin:10}}>
                <Text style={{ color: 'black', fontSize: 12,textAlign:'center' ,fontWeight:'500'}}>{index+1}. </Text>
                <Text style={{color: 'black', fontSize: 12,textAlign:'center',fontWeight:'500' }}>{coin}</Text></View>
                    <View style={{ width: deviceWidth - 40, backgroundColor: '#f7f7f7', alignItems: 'center'}}>
      
                        {this.renderImages()}
                           
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',margin:10}}>
                       <Text style={{  color: 'black', fontSize: 10,textAlign:'center' }}>No. of coin(s) </Text>
                <Text style={{  color: 'black', fontSize: 10,textAlign:'center' }}>{noOfcoins}</Text>
                    </View>
                    {/* <TouchableOpacity onPress={() => this.props.method()}>
                        <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'right' }}>Close</Text>
                    </TouchableOpacity> */}
   <TouchableOpacity style={{alignItems:'flex-end'}}onPress={()=>this.props.method()}>
            <Text style={{width:80,borderColor:'rgba(216,216,216,1)',borderWidth:1,margin: 10, color: 'black', 
                        fontSize: 10, textAlign: 'center',padding:10,backgroundColor:'rgba(238,238,238,1)' }}> CLOSE </Text>
            </TouchableOpacity> 
                   
                </View>

            </View>
     );
    }
};

Dialog.propTypes = {
    size: PropTypes.string,
};

Dialog.defaultProps = {
    size: 'large',
};


export default Dialog;


