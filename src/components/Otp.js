import React, { Component } from "react";
import { ButtonPress } from "./common";
import {
  Text,
  View,
  Dimensions,
  Platform,
  Alert,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import {
  Item,
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Input
} from "native-base";
import Loading from "./Loading/";
import LoginForm from "./LoginForm";
import CreateAccount from "./CreateAccount";
import { InternetCheck, TimeOutError } from "./common/";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      isLoading: false,
      token: "",
      userid: this.props.navigation.state.params.userid
    };
  }

  componentWillmount() {
    InternetCheck();
  }

  render() {
    const { iconStyle, container, contentStyle, mainView, headerView } = styles;
    return (
      <Container style={container}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            style={styles.goBackButton}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon style={{ color: "white" }} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerText}>
            <Text style={{ color: "white" }}>OTP</Text>
          </View>
        </View>

        <Content style={contentStyle}>
          <View style={mainView}>
            <View
              style={{
                marginTop: 10,
                width: deviceWidth - 40,
                alignSelf: "center"
              }}
            >
              <Text style={{ color: "#c2c2c2" }}> OTP</Text>
              <Item
                style={{
                  marginTop: 10,
                  width: deviceWidth - 40,
                  height: 40,
                  backgroundColor: "#f7f7f7",
                  borderRadius: 5
                }}
              >
                <Icon active name="ios-log-in" style={iconStyle} />
                <Input
                  placeholder="Enter OTP"
                  placeholderTextColor="#c2c2c2"
                  style={{ alignSelf: "center", fontSize: 11 }}
                  onChangeText={otp => this.setState({ otp })}
                  value={this.state.otp}
                />
              </Item>
            </View>
            <View style={{ marginTop: 0 }}>{this.renderButton()}</View>
          </View>
        </Content>

        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }

  renderButton() {
    return <ButtonPress onPress={() => this.submitData()}>Submit</ButtonPress>;
  }

  componentDidMount() {
    FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
      // console.log('token')
      // console.log(token)
      this.setState({ token: token });
      // store fcm token in your server
    });
    // console.log('this.state.token')
    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
      // do some component related stuff
      // console.warn('notification');
    });
  }

  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
  }

  async submitData() {
    const SELF = this;
    await AsyncStorage.setItem(
      "@deviceid:key",
      JSON.stringify(SELF.state.token || {})
    );
    SELF.setState({
      isLoading: true
    });

    if (SELF.state.otp == "") {
      Alert.alert("Invalid Credential", "Please enter OTP");
      SELF.setState({
        isLoading: false
      });
    } else {
      axios({
        url: "https://raott.com/api/Auth/signup_step2/",
        type: "POST",
        method: "POST",
        timeout: 30000,
        data: {
          otp: SELF.state.otp,
          userid: SELF.state.userid,
          deviceid: SELF.state.token
        }
      })
        .then(function(response) {
          // console.log('response signup_step2');
          // console.log(response);
          if (response.data.status == "success") {
            AsyncStorage.setItem("apiToken", response.data.apitoken);
            Alert.alert(
              "Success",
              "Please Sign in using your account ",
              [
                {
                  text: "OK",
                  onPress: () => SELF.props.navigation.navigate("LoginForm")
                }
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(response.data.status, response.data.error);
            SELF.setState({ otp: "" });
          }
          SELF.setState({
            isLoading: false
          });
        })
        .catch(function(error) {
          SELF.setState({ isLoading: false });
          TimeOutError(error, () => SELF.buttonpress());
          // console.log(error);
        });
    }
  }
}

const styles = {
  iconStyle: {
    color: "#e02d2e",
    marginLeft: 10
  },

  container: {
    width: deviceWidth,
    backgroundColor: "white"
  },
  contentStyle: {
    marginTop: 60,
    marginBottom: 10
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: "white"
  },
  headerView: {
    width: deviceWidth - 40,
    alignSelf: "center"
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: Platform.OS === "ios" ? 20 : 0,
    flexDirection: "row"
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: "center",
    justifyContent: "center"
  },

  headerText: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginLeft: 5
  }
};
