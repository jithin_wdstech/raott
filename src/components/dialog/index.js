import React from 'react';
import { View, ActivityIndicator, Dimensions,Text,Button,TouchableOpacity,Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const Dialog = (props,children) => {

 
 
  

  
  return (
    <View style={styles.container}>


      <View style={{width:deviceWidth-40, backgroundColor: '#f7f7f7'}}>
   <View style={{width:deviceWidth-40,backgroundColor:'#e02d2e',flexDirection:'row', 
       
        justifyContent: 'space-between',height:50,alignItems:'center'}}>
   <Text style={{ margin:10,color:'white',textAlign:'center'}} > {props.titleText}</Text>

    <TouchableOpacity onPress={()=>props.method()}>
      <Image style={{marginRight:10,width:20,height:20,marginTop:0 ,justifyContent:'space-between'}} source={require('../../../img/closeicon.png')}>
       
    
       </Image>
    
    </TouchableOpacity>
   </View>
        
        <Text style={{marginTop:10,marginLeft:10,marginRight:10,color:'black',fontSize:10 }}>{props.msg}</Text>
        <Text style={{ color:'black',fontSize:10,textAlign:'center',marginBottom:20}} > {props.time}</Text>
        <TouchableOpacity onPress={()=>props.method()}>
            <Text style={{margin:10,color:'black',fontSize:10, textAlign:'right' }}>Close</Text>
            </TouchableOpacity>
      </View>

    </View>
  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};


export default Dialog;


