import React, {Component} from 'react';
import {ButtonPress} from './common';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  Alert,
  TextInput,
  BackHandler,
} from 'react-native';
// import Drawer from './Drawer'
import Moment from 'moment';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  ListItem,
  List,
  Card,
  CardItem,
  Item,
  Input,
} from 'native-base';
// import CreateAccount from "./CreateAccount"
// import { DrawerNavigator } from "react-navigation";
// import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
// import ModalDropdown from 'react-native-modal-dropdown'
// import Hyperlink from 'react-native-hyperlink'
import DrawerHeader from './common/drawerHeader/drawerHeader';
import {PickDate, InternetCheck, TimeOutError} from './common/';
import axios from 'axios';
import Loading from './Loading/';
import ImagePicker from 'react-native-image-picker';

// const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

// More info on all the options is below in the README...just some common use cases shown here
var options = {
  title: 'Select Option',
  allowsEditing: true,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropDownTitle: 'Select Country',
      isDatePickerVisible: false,
      date: '',
      dob: '',
      email: '',
      fullname: '',
      profileImage: '',
      username: '',
      user_type: '',
      isLoading: true,
      password: '',
      re_password: '',
      enableError: false,
      imageSource: null,
      imageBase64: '',
      uploadImage: false,
      countryName: '',
    };

    this._backAndroidPress = this.backAndroidPress.bind(this);
    this.handleDatePicker = this.handleDatePicker.bind(this);
    this.hideDateTimePicker = this.hideDateTimePicker.bind(this);
    this.handleDatePicked = this.handleDatePicked.bind(this);
  }

  componentWillMount() {
    InternetCheck();
    this.getData();
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.props.navigation.navigate('LandingPage');
    return true;
  }

  renderButton() {
    return <ButtonPress onPress={() => this.buttonPress()}>Submit</ButtonPress>;
  }

  handleDatePicker() {
    this.setState({
      isDatePickerVisible: !this.state.isDatePickerVisible,
    });
  }

  hideDateTimePicker() {
    this.setState({
      isDatePickerVisible: false,
    });
  }

  handleDatePicked(date) {
    const dateStr = Moment(date).format('D MMM YYYY ');
    const dobStr = Moment(date).format('DD/MM/YYYY ');
    this.setState({
      date: dateStr,
      dob: dobStr,
    });
    this.hideDateTimePicker();
  }

  showImagePicker() {
    this.setState({isLoading: true});
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        this.setState({isLoading: false});
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = response.uri;
        if (response.fileSize < 4194304) {
          this.setState({
            isLoading: false,
            imageSource: source,
            imageBase64: response.data,
          });
        } else {
          Alert.alert('Error', 'File size is too long');
          this.setState({
            isLoading: false,
          });
        }
        // this.setState({
        //     imageBase64:response.data,
        //     imageSource: source,
        //     isLoading: false,
        //     uploadImage:true
        // });
      }
    });
  }
  render() {
    const {
      iconStyle,
      container,
      contentStyle,
      mainView,
      headerView,
      choseImageButton,
      profileIconStyle,
      changeProfileText,
      textStyle,
      itemContainerStyle,
      contentTextStyle,
      passwordAlertText,
      fullNameText,
      dobButtonStyle,
      dobTextStyle,
      passwordInputField,
    } = styles;
    let img =
      this.state.imageSource == null
        ? this.state.profileImage
        : this.state.imageSource;
    return (
      <Container style={container}>
        <DrawerHeader
          navigation={this.props.navigation}
          username={this.state.fullname}
          pImage={img}
        />
        <TouchableOpacity
          onPress={this.showImagePicker.bind(this)}
          style={choseImageButton}>
          <Icon active name="md-person" style={profileIconStyle} />
          <Text style={changeProfileText}>Change Profile Image</Text>
        </TouchableOpacity>

        <Content style={contentStyle}>
          <View style={mainView}>
            <View style={headerView}>
              <Text style={textStyle}> User Name</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-person" style={iconStyle} />
                <Text style={contentTextStyle}>{this.state.username}</Text>
              </Item>
            </View>

            <View style={headerView}>
              <Text style={textStyle}> Email</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="ios-mail" style={iconStyle} />
                <Text style={contentTextStyle}>{this.state.email}</Text>
              </Item>
            </View>

            <View style={headerView}>
              <Text style={textStyle}> Full Name</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-person" style={iconStyle} />
                <Input
                  placeholder="Full Name"
                  placeholderTextColor="#c2c2c2"
                  style={fullNameText}
                  onChangeText={fullname => this.setState({fullname})}
                  value={this.state.fullname}
                />
              </Item>
            </View>

            <View style={headerView}>
              <Text style={textStyle}> Date Of Birth</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-calendar" style={iconStyle} />
                <Button
                  transparent
                  style={dobButtonStyle}
                  onPress={this.handleDatePicker}>
                  <Text value={this.state.date} style={dobTextStyle}>
                    {this.state.date}
                  </Text>
                </Button>
                <PickDate
                  isDateTimePickerVisible={this.state.isDatePickerVisible}
                  hideDateTimePicker={this.hideDateTimePicker}
                  handleDatePicked={this.handleDatePicked}
                />
              </Item>
            </View>

            {/* </ModalDropdown>  */}
            <View style={headerView}>
              <Text style={textStyle}> Country</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-globe" style={iconStyle} />
                <Text style={contentTextStyle}>{this.state.countryName}</Text>
                <TouchableOpacity
                  onPress={() => this._dropdown_show()}></TouchableOpacity>
              </Item>
            </View>

            <View style={headerView}>
              <Text style={textStyle}> New Password</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-lock" style={iconStyle} />
                <Input
                  placeholder="New Password"
                  secureTextEntry
                  placeholderTextColor="#c2c2c2"
                  style={passwordInputField}
                  onChangeText={password => this.setState({password})}
                  value={this.state.password}
                />
              </Item>
            </View>

            <View style={headerView}>
              <Text style={textStyle}>Confirm New Password</Text>
              <Item style={itemContainerStyle}>
                <Icon active name="md-lock" style={iconStyle} />
                <Input
                  placeholder="Retype Password"
                  secureTextEntry
                  placeholderTextColor="#c2c2c2"
                  style={passwordInputField}
                  onChangeText={re_password => this.setState({re_password})}
                  value={this.state.re_password}
                  onEndEditing={event => {
                    this.submitEditingPassword();
                  }}
                />
              </Item>
              {this.state.enableError == true && (
                <Text style={passwordAlertText}>
                  {' '}
                  Password does not match. Please try again{' '}
                </Text>
              )}
            </View>

            <View style={{marginTop: 0}}>{this.renderButton()}</View>
          </View>
        </Content>

        <View style={{}}></View>
        <Loading isLoading={this.state.isLoading} />
      </Container>
    );
  }
  _dropdown_show() {
    this._dropdown && this._dropdown.show();
  }

  _dropdown_renderRow(rowData, rowID, highlighted) {
    let evenRow = rowID % 2;
    return (
      <TouchableOpacity underlayColor="cornflowerblue">
        <View style={styles.dropdown_row}>
          <Text
            style={[
              styles.dropdown_row_text,
              highlighted && {color: '#188cf0'},
            ]}>
            {`${rowData}`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  _dropdown_onSelect() {}

  submitEditingPassword() {
    if (this.state.password != this.state.re_password) {
      this.setState({enableError: true});
    } else {
      this.setState({enableError: false});
    }
  }

  buttonPress() {
    if (this.state.password && this.state.re_password) {
      if (this.state.password != this.state.re_password) {
        this.setState({enableError: true});
      } else {
        this.submitData();
      }
    } else {
      this.submitData();
    }
  }

  getData() {
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        axios({
          url: 'https://raott.com/api/v2/userProfile/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log('response');
            // console.log(response);
            // console.log(response.data.dob);
            // console.log(response.data.email);
            // console.log(response.data.fullname);
            // console.log(response.data.profile_image);
            // console.log(response.data.username);
            // console.log(response.data.user_type);
            SELF.setState({
              isLoading: false,
            });

            let dateStr = '';
            if (
              response.data &&
              response.data.dob &&
              response.data.dob !== '' &&
              response.data.dob !== '//'
            ) {
              dateStr = Moment(response.data.dob, 'DD/MM/YYYY').format(
                'D MMM YYYY',
              );
            }

            SELF.setState({
              date: dateStr,
              dob: response.data.dob,
              email: response.data.email,
              fullname: response.data.fullname,
              profileImage: response.data.profile_image,
              username: response.data.username,
              user_type: response.data.user_type,
              countryName: response.data.countryName,
            });
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.getData());
          });
      })
      .done(); // ending statement of asyncstorage
  }

  submitData() {
    const SELF = this;
    SELF.setState({isLoading: true});

    if (SELF.state.imageBase64) {
      SELF.updateImage();
    } else {
      let password = '';
      AsyncStorage.getItem('apiToken')
        .then(value => {
          // console.log(value);
          // console.log(SELF.state.password);
          // console.log(SELF.state.username);
          // console.log(SELF.state.fullname);
          // console.log(SELF.state.email);
          // console.log(SELF.state.date);
          let data = {
            username: SELF.state.username,
            fullname: SELF.state.fullname,
            email: SELF.state.email,
          };
          if (
            SELF.state.dob &&
            SELF.state.dob !== '' &&
            SELF.state.dob !== '//'
          ) {
            data['dob'] = SELF.state.dob;
          }
          if (SELF.state.password) {
            data['password'] = SELF.state.password;
          }
          // console.log(data);
          axios({
            url: 'https://raott.com/api/v2/userProfile/',
            type: 'POST',
            method: 'POST',
            timeout: 30000,
            data,
            headers: {
              RaottAuth: value,
            },
          })
            .then(function(response) {
              // console.log('response userProfile');
              // console.log(response);
              SELF.setState({enableError: false});
              if (response.data.status == true) {
                SELF.setState({isLoading: false});
                Alert.alert(
                  'Confirmation',
                  'User details have been updated successfully',
                );
              }
            })
            .catch(function(error) {
              SELF.setState({isLoading: false});
              TimeOutError(error, () => SELF.submitData());
            });
        })
        .done(); // ending statement of asyncstorage
    }
  }

  updateImage() {
    const SELF = this;
    SELF.setState({isLoading: true});
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        axios({
          url: 'https://raott.com/api/v2/updateImage/',
          type: 'POST',
          method: 'POST',
          timeout: 30000,
          data: {
            image: SELF.state.imageBase64,
          },
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log('response updateImage', response)
            if (response.data.response == 'success') {
              SELF.setState({uploadImage: false, imageBase64: ''});
              SELF.submitData();
            } else {
              Alert.alert('Error', 'Unable to upload image . Try again.');
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.updateImage());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

const styles = {
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },
  // width:20
  container: {
    width: deviceWidth,
    backgroundColor: 'white',
  },
  contentStyle: {
    marginTop: 20,
    marginBottom: 10,
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: 'white',
  },
  headerView: {
    marginTop: 10,
    width: deviceWidth - 40,
    alignSelf: 'center',
  },
  choseImageButton: {
    marginTop: 65,
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#e02d2e',
    borderRadius: 5,
    flexDirection: 'row',
  },
  profileIconStyle: {
    color: '#ffffff',
    marginLeft: 10,
    alignSelf: 'center',
  },
  changeProfileText: {
    alignSelf: 'center',
    marginLeft: 10,
    fontSize: 12,
    color: '#ffffff',
    marginRight: 12,
  },
  textStyle: {
    color: '#c2c2c2',
  },
  itemContainerStyle: {
    marginTop: 10,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  contentTextStyle: {
    alignSelf: 'center',
    fontSize: 11,
    color: '#c2c2c2',
  },
  passwordAlertText: {
    alignSelf: 'center',
    fontSize: 11,
    width: deviceWidth - 50,
    color: 'red',
  },
  fullNameText: {
    color: '#000000',
    fontSize: 11,
    height: 40,
  },
  dobButtonStyle: {
    borderWidth: 0,
    width: deviceWidth - 80,
    height: 40,
  },
  dobTextStyle: {
    color: '#000000',
    fontSize: 11,
  },
  passwordInputField: {
    alignSelf: 'center',
    fontSize: 11,
  },
};

EditProfile.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};
