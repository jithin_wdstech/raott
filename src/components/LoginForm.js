import React, {Component} from 'react';
import {ButtonPress} from './common';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  Button,
  Dimensions,
  Alert,
  AsyncStorage,
  Platform,
  ScrollView,
  BackHandler,
  Keyboard,
} from 'react-native';
import {Container, Content, Item, Input, Icon} from 'native-base';
// import CreateAccount from "./CreateAccount/";
// import ForgotPassword from "./ForgotPassword/";
import Loading from './Loading/';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Otp from "./Otp";
import {InternetCheck, TimeOutError} from './common/';
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';
// import { requestPermission } from "react-native-android-permissions";
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      pwd: '',
      isLoading: false,
      token: '',
      isRemeberMe: false,
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);
    // we are getting initial username and password to set isRememberMe state
    AsyncStorage.getItem('username')
      .then(username => {
        if (username !== undefined && username !== null && username !== '') {
          this.setState({uname: username});
        }
        AsyncStorage.getItem('password')
          .then(password => {
            if (
              password !== undefined &&
              password !== null &&
              password !== ''
            ) {
              isRemeberMe = this.state.isRemeberMe;
              if (this.state.uname !== '') {
                isRemeberMe = true;
              }
              this.setState({pwd: password, isRemeberMe});
            }
          })
          .done();
      })
      .done();
    this.handleRememberMe = this.handleRememberMe.bind(this);
    this.loginButtonPress = this.loginButtonPress.bind(this);
    // this.loginButtonPress = .debounce(this.loginButtonPress.bind(this), 1000, {
    //   leading: true,
    //   trailing: false
    // });
  }

  componentWillMount() {
    InternetCheck();
  }

  backAndroidPress() {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    const checkboxcolor = this.state.isRemeberMe ? '#e02d2e' : '#b9bbc1';
    const {
      rememberText,
      mainContainerStyle,
      scrollContainer,
      logoStyle,
      usernameItem,
      iconStyle,
      textInputStyle,
      passwordItem,
      checkboxView,
      checkboxButton,
      forgetPasswordView,
      forgotText,
      bottomMain,
      signUpText,
      textRaottStyle,
    } = styles;
    return (
      <Container style={mainContainerStyle}>
        <Content contentContainerStyle={scrollContainer}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Image
              style={logoStyle}
              source={require('../img/raott_logo.png')}
            />
          </TouchableOpacity>
          <View>
            <Item style={usernameItem}>
              <Icon active name="md-person" style={iconStyle} />
              <Input
                placeholder="User Name"
                placeholderTextColor="#c2c2c2"
                style={textInputStyle}
                onChangeText={uname => this.setState({uname})}
                value={this.state.uname}
                autoCorrect={false}
                blurOnSubmit={true}
              />
            </Item>
            <Item style={passwordItem}>
              <Icon active name="ios-lock" style={iconStyle} />
              <Input
                placeholder="Password"
                secureTextEntry
                placeholderTextColor="#c2c2c2"
                style={textInputStyle}
                onChangeText={pwd => this.setState({pwd})}
                value={this.state.pwd}
                blurOnSubmit={true}
              />
            </Item>
            <View style={checkboxView}>
              <TouchableOpacity
                activeOpacity={1.0}
                onPress={this.handleRememberMe}
                style={checkboxButton}>
                <MaterialIcons
                  name={
                    this.state.isRemeberMe
                      ? 'check-box'
                      : 'check-box-outline-blank'
                  }
                  size={24}
                  style={{color: checkboxcolor}}
                />
                <Text style={rememberText}>Remember Me</Text>
              </TouchableOpacity>
            </View>
            <ButtonPress onPress={() => this.loginButtonPress()}>
              {' '}
              Sign In{' '}
            </ButtonPress>
            <View style={forgetPasswordView}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('ForgotPassword')
                }>
                <Text style={forgotText}>Forgot Password ?</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* <TouchableOpacity style={{ width: 120, height: 120, alignSelf: 'center' }} onPress={() => this.props.navigation.navigate("Category", { screen: "LoginForm" })} >
                    <Image style={{ width: 120, height: 120, alignSelf: 'center' }} source={require('../../img/treasureHunt.png')}></Image>
                </TouchableOpacity> */}
          <View style={bottomMain}>
            <Text style={signUpText}>Sign Up to</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('CreateAccount')}>
              <Text style={textRaottStyle}>Raotts</Text>
            </TouchableOpacity>
          </View>
          {this.state.isLoading && <Loading isLoading={this.state.isLoading} />}
        </Content>
      </Container>
    );
  }
  componentDidMount() {
    FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
      this.storingdeviceId(token);
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
      // do some component related stuff
    });
    //     FCM.on(FCMEvent.Notification, notif => {
    //         if(notif.fcm && notif.fcm.body) {
    //             /* Create local notification for showing in a foreground */
    //             FCM.presentLocalNotification({
    //                body: notif.fcm.body,
    //                priority: "high",
    //                title: notif.fcm.title,
    //                sound: "default",
    //                badge: 0,
    //                "show_in_foreground" :true, /* notification when app is in foreground (local & remote)*/
    //                vibrate: 300, /* Android only default: 300, no vibration if you pass null*/
    //                large_icon: "ic_launcher",                           // Android only
    //                icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
    //                color: "#e02d2e",
    //                status: notif.status
    //            });
    //         }
    //   });
    //   FCM.setBadgeNumber(0);                                       // iOS and supporting android.
    //   FCM.getBadgeNumber().then(number=>console.log(number));
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }
  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }
  handleRememberMe() {
    this.setState({
      isRemeberMe: !this.state.isRemeberMe,
    });
  }
  async loginButtonPress() {
    const SELF = this;
    Keyboard.dismiss();
    SELF.setState({
      isLoading: true,
    });
    // if username or password are empty
    if (this.state.uname == '' || this.state.pwd == '') {
      Alert.alert('Invalid Credential', 'Please enter Email & Password');
      SELF.setState({isLoading: false, uname: '', pwd: ''});
    }
    // checking the validity of username and password
    else {
      // if credentials were already filled bcoz of remember state
      if (this.state.isRemeberMe) {
        try {
          AsyncStorage.setItem('username', SELF.state.uname);
          AsyncStorage.setItem('password', SELF.state.pwd);
          await AsyncStorage.setItem(
            '@deviceid:key',
            JSON.stringify(SELF.state.token || {}),
          );
        } catch (error) {
          // Error saving data
        }
      }

      axios({
        url: 'https://raott.com/api/Auth/authenticate',
        type: 'POST',
        method: 'POST',
        timeout: 30000,
        data: {
          username: SELF.state.uname,
          password: SELF.state.pwd,
          deviceid: SELF.state.token,
        },
      })
        .then(function(response) {
          if (response.data.status == 'success') {
            SELF.setState({isLoading: false});
            try {
              AsyncStorage.setItem('apiToken', response.data.apitoken);
            } catch (error) {
              // Error saving data
            }
            SELF.props.navigation.navigate('LandingPage');
          } else {
            if (response.data.error == 'Account not confirmed') {
              SELF.requestOtp(response.data.userid);
              SELF.setState({isLoading: false});
            } else if (response.data.error == 'Authentication failure!') {
              Alert.alert('Invalid Credential', response.data.error);
              SELF.setState({uname: '', isLoading: false, pwd: ''});
            } else {
              Alert.alert('Error', 'Username or Password is Invalid');
              SELF.setState({uname: '', isLoading: false, pwd: ''});
            }
          }
        })
        .catch(function(error) {
          SELF.setState({isLoading: false});
          TimeOutError(error, () => SELF.loginButtonPress());
        });
    }
  }
  requestOtp(userid) {
    let fetchUserId = userid;
    const SELF = this;
    axios
      .get('https://raott.com/api/Auth/request_otp/' + userid, {
        timeout: 30000,
      })
      .then(function(response) {
        // console.log('response request_otp');
        // console.log(response);
        if (response.data.status == 'success') {
          Alert.alert(
            '',
            'We have sent a verification code to your registered email address. Please enter that code to complete registration.',
            [
              {
                text: 'OK',
                onPress: () =>
                  SELF.props.navigation.navigate('Otp', {userid: userid}),
              },
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(response.data.status, response.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);
        SELF.setState({isLoading: false});
        TimeOutError(error, () => SELF.requestOtp(fetchUserId));
      });
  }
  async storingdeviceId(token) {
    try {
      await AsyncStorage.setItem('@deviceid:key', JSON.stringify(token || {}));
    } catch (error) {
      // Error saving data
    }
    this.setState({token: token});
  }
}

const styles = {
  mainContainerStyle: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'space-around',
  },
  scrollContainer: {
    justifyContent: 'space-around',
    height: deviceHeight,
  },
  textInputStyle: {
    alignSelf: 'center',
    fontSize: 13,
    fontWeight: '600',
  },
  usernameItem: {
    alignSelf: 'center',
    marginTop: 20,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  passwordItem: {
    alignSelf: 'center',
    marginTop: 20,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: 'red',
  },
  bottomMain: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  forgotText: {
    width: deviceWidth - 40,
    height: 30,
    marginTop: 10,
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '600',
    backgroundColor: 'white',
    color: '#c2c2c2',
  },
  signUpText: {
    backgroundColor: 'white',
    color: '#c2c2c2',
    fontSize: 13,
    fontWeight: '600',
  },
  rememberText: {
    width: 120,
    marginLeft: 10,
    color: '#c2c2c2',
    backgroundColor: 'white',
    fontSize: 13,
    fontWeight: '600',
  },
  logoStyle: {
    width: 120,
    height: 120,
    alignSelf: 'center',
    marginTop: 15,
  },
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },
  checkboxView: {
    width: deviceWidth - 40,
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  checkboxButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgetPasswordView: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textRaottStyle: {
    color: '#e02d2e',
    marginLeft: 5,
    backgroundColor: 'white',
    fontSize: 13,
    fontWeight: '600',
  },
};

LoginForm.navigationOptions = {
  header: null,
  gesturesEnabled: false,
};

export default LoginForm;
