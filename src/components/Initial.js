import React, {Component} from 'react';
import {View, AsyncStorage, Image, Platform} from 'react-native';

class Initial extends Component {
  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      const duration = 1000;
      setTimeout(() => {
        this.checkLoginStatus();
      }, duration);
    } else {
      this.checkLoginStatus();
    }
  }

  checkLoginStatus() {
    AsyncStorage.getItem('apiToken')
      .then(value => {
        if (value !== undefined && value !== null && value !== '') {
          this.props.navigation.navigate('AppStackRouter');
        } else {
          this.props.navigation.navigate('LoginStackRouter');
        }
      })
      .done();
  }

  render() {
    if (Platform.OS === 'android') {
      return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Image
            style={{
              width: 120,
              height: 120,
            }}
            source={require('../img/raott_logo.png')}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

Initial.navigationOptions = {
  header: null,
  gesturesEnabled: false,
};

export default Initial;
