import React, { Component } from "react";
import { Image, Dimensions, AsyncStorage, Alert, ScrollView } from "react-native";

import {
	Content,
	Text,
	List,
	ListItem,
	Icon,
	Container,
	Left,
	Right,
	Badge,
	Button,
	View,
	StyleProvider,
	getTheme,
	variables,
} from "native-base";
import axios from 'axios';
import styles from "./style";
import { connect } from "react-redux";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';

const drawerCover = require("../../../img/raott_logo.png");
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const dataInitial = [
	{
		name: "Go to Raott Account",
		route: "LandingPage",
		icon: "md-cash",
		color: '#e02d2e'
	},
	{
		name: "Edit Profile",
		route: "EditProfile",
		icon: "md-person",
		bg: '#e02d2e'
	},
	{
		name: "Home Currency",
		route: "HomeCurrency",
		icon: "logo-usd",
		bg: '#e02d2e'
	},
	{
		name: "What is GeoMint™?",
		route: "Geomint",
		icon: "md-help-circle",
		bg: '#e02d2e'
	},
	{
		name: "FAQ",
		route: "FAQ",
		icon: "md-information-circle",
		bg: '#e02d2e'
	},
	{
		name: "Contact Us",
		route: "ContactUs",
		icon: "call",
		bg: '#e02d2e'
	},
	{
		name: "Sign Out",
		route: "HomeScreen",
		icon: "md-log-out",
		bg: '#e02d2e'
	},
];

navigationOptions: ({ navigation }) => ({
	drawerLockMode: 'locked-closed'
})

let deviceId = ''
let status = false
let invite_icon = require("../../../img/WarningMsg.png")
class SideBar extends Component {
	constructor(props) {
		super(props);
		SELF = this;
		this.state = {
			shadowOffsetWidth: 1,
			shadowRadius: 4
		};
		this.clickDebounce = _.debounce(this.sideBarButtonPressed.bind(this), 1000, { 'leading': true, 'trailing': false });
		this.getdeviceId()
	}

	sideBarButtonPressed(data) {
		// console.log('data')
		// console.log(data)
		if (data.route === "HomeScreen") {
			this.signOut()
		}
		// this.props.navigation.navigate("HomeScreen")
		this.props.navigation.navigate(data.route)
	}

	isFriendRequestAdded() {
		for (const data of dataInitial) {
			if (data.name === 'Invite a Friend') {
				return true;
			}
		}
		return false;
	}
	render() {
		// console.log('this.props.navigation.state.routeName');
		// console.log(this.props.navigation.state.routeName);
		// console.log(this.props.user);
		// console.log('this.props.user');
		// const isShowFriendRequest = true;
		let datas = [].concat(dataInitial);
		// console.log(status)
		if (this.props.user && this.props.user.membership_status == 0) {
			datas.splice(2, 0, {
				name: "Invite A Friend",
				route: "Invite",
				icon: "card-giftcard",
				bg: '#e02d2e',
				isMaterialIcon: true
			});
		}

		return (
			<Container >
				<Content bounces={false} style={{ flex: 1, backgroundColor: "#fff", top: -1, height: deviceHeight }}>
					<Image source={drawerCover} style={styles.drawerCover}>
					</Image>
					<List 
						style={{ backgroundColor: '#f9fafc', borderWidth: 1, borderColor: '#dedede', marginTop: 20, height: deviceHeight - 120 }}
						dataArray={datas}
						renderRow=
						{
							data =>
							<ListItem 
								button 
								noBorder 
								onPress={() => this.clickDebounce(data)}
							>
								<Left>
									{
										data.isMaterialIcon ? <MaterialIcons active name={data.icon} style={{ color: "red", fontSize: 26, width: 30 }} /> :
											<Icon active name={data.icon} style={{ color: "red", fontSize: 26, width: 30 }} />
									}
									<Text style={styles.text}>
										{data.name}
									</Text>
									{
										data.name === 'Invite A Friend' &&
										<Image style={{ width: 20, height: 20 }} source={require("../../../img/WarningMsg.png")}></Image>
									}
								</Left>
								{
									data.types &&
									<Right style={{ flex: 1 }}>
										<Badge
											style={{
												borderRadius: 3,
												height: 25,
												width: 72,
												backgroundColor: data.bg,
											}}
										>
											<Text style={styles.badgeText}>{`${data.types} Types`}</Text>
										</Badge>
									</Right>
								}
							</ListItem>
							}
					/>
				</Content>

			</Container>
		);
	}

	async getdeviceId() {
		deviceId = await AsyncStorage.getItem('@deviceid:key').then(data => data ? JSON.parse(data) : null);
	}

	signOut() {
		// console.warn('signOut')
		AsyncStorage.getItem("apiToken").then((apiToken) => {
			// console.log(apiToken)
			// console.log( deviceId)
			axios({
				url: 'https://raott.com/api/v2/logout',
				type: 'POST',
				method: 'POST',
				data:
					{
						deviceid: deviceId
					},
				headers:
					{
						'RaottAuth': apiToken
					},
			})
				.then(function (response) {
					// console.warn('HERE')
					// this.props.navigation.navigate("HomeScreen")
					// console.log('response');
					// console.log(response);
				})
				.catch(function (error) {
					// console.log(error);
					Alert.alert('Error', error.reason)
				});
			// }).done();
		}).done();

		try {
			// console.warn('SignOut')
			AsyncStorage.setItem('apiToken', '');
			AsyncStorage.setItem('selectedCoin', '');
			AsyncStorage.setItem('inviteCode', '');
			AsyncStorage.setItem('membership_status', '');
			AsyncStorage.setItem('reminder_date', '');
		} catch (error) {
			// Error saving data
		}
	}
}


function bindAction(dispatch) {
	return {
	};
}

const mapStateToProps = state => ({
	user: state.user.data
});

export default connect(mapStateToProps, bindAction)(SideBar);
