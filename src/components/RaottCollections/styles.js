import {Dimensions, Platform } from 'react-native'
const React = require("react-native");

const { StyleSheet } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "#f2f2f2",
    
  },
  mb10: {
    marginBottom: 20
  },

transactionTextStyle:{
  color:'rgba(142,142,142,1)',
  fontSize:12,
},
headerStyle:{
  backgroundColor:"#f2f2f2",
  flexDirection:'row',
  height:44,
  marginTop: (Platform.OS === 'ios') ? 20 : 0,
},
headerTextView:{
  width:deviceWidth-60,
  alignItems:'center',

},
headerText:{
  color:"#e02d2e",
 
},
infoMainView:{
  width:deviceWidth,
  height:60,
  backgroundColor:"#e02d2e",
  alignItems:'center',
  position:'relative'
},
info:{
  position:"absolute",
  alignItems:"center",
  bottom:10
},
balanceText:{
  fontSize:10,
  color:'white',
  marginTop:15
},
raottCoin:{
  color:'white',
  fontSize:18
},
collectionText:{
  width:deviceWidth,
  marginTop:20,
  alignItems:'center'
},
goBackButton:
{
    height:44,
    width:44,
    alignItems:'center',
    justifyContent:'center'
},
headerTitle:
{  
    height:44,
    width: deviceWidth - 98,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 5
},

  
};
