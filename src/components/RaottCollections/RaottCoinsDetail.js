import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import CoinTab from "../CoinTab/CoinTab";
import styles from "./styles";
import numeral from "numeral";
import _ from "lodash";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
let coinImage = null;

class RaottCoinsDetail extends Component {
  constructor(props) {
    super(props);
    this.clickDebounce = _.debounce(this.click.bind(this), 1000, {
      leading: true,
      trailing: false
    });
  }

  click(id) {
    this.props.onCardClick(id);
  }

  render() {
    const { worldRaott, onCardClick, symbol } = this.props;
    const { id, coinType, image, dateAdded, balance, currency } = worldRaott;
    if (coinType == "Crown") {
      coinImage = require("../../../img/rsz_gold_crown.png");
    } else if (coinType == "Ring") {
      coinImage = require("../../../img/rsz_gold_ring.png");
    } else if (coinType == "Cup") {
      coinImage = require("../../../img/rsz_gold_cup.png");
    } else {
      coinImage = null;
    }
    return (
      <Content>
        <TouchableOpacity onPress={() => this.clickDebounce(id)}>
          <View
            style={{
              borderWidth: 0,
              marginLeft: 40,
              marginTop: 10,
              marginRight: 40
            }}
          >
            <Card>
              <CardItem style={{ height: 120 }}>
                <View
                  style={{
                    justifyContent: "center",
                    width: deviceWidth * 0.46 - 30
                  }}
                >
                  <Image
                    style={{ width: 90, height: 90, alignSelf: "center" }}
                    source={coinImage}
                  />
                  {this.props.symbol == coinType && (
                    <Text
                      style={{
                        fontSize: 11,
                        textAlign: "center",
                        marginTop: 5,
                        color: "#91908c"
                      }}
                    >
                      Selected Symbol{" "}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: deviceWidth * 0.46 - 40,
                    height: 120,
                    position: "absolute",
                    right: 0,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#ffffdf"
                  }}
                >
                  <Text
                    numberOfLines={1}
                    style={{ fontSize: 14, textAlign: "center", marginTop: 30 }}
                  >
                    {" "}
                    Total : R {numeral(balance).format("0,0")}{" "}
                  </Text>
                  <Text
                    numberOfLines={1}
                    style={{
                      textAlign: "center",
                      fontSize: 12,
                      color: "#91908c"
                    }}
                  >
                    {numeral(currency).format("0,0.00")}{" "}
                    {currency.split(" ")[1]}
                  </Text>

                  <Text
                    style={{
                      color: "#e02d2e",
                      fontSize: 12,
                      marginTop: 15,
                      alignSelf: "center",
                      textAlign: "center"
                    }}
                  >
                    {parseInt(id) + "" === parseInt(symbol) + ""
                      ? "Your choosen World Raott symbol"
                      : ""}
                  </Text>
                </View>
              </CardItem>
            </Card>
          </View>
        </TouchableOpacity>
      </Content>
    );
  }
}

export default RaottCoinsDetail;
