import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  Platform,
  AsyncStorage,
  Alert,
  TouchableOpacity
} from "react-native";
import {
  Item,
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Input
} from "native-base";
import { ButtonPress } from "./common";
import DrawerHeader from "./common/drawerHeader/drawerHeader";
import axios from "axios";
import Moment from "moment";
import { connect } from "react-redux";
import DialogNotMember from "./TransferCoinToFriends/DialogNotMember.js";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class Invite extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inviteCode: "",
      email: "",
      reminder_date: "",
      showDialog: true
    };

    AsyncStorage.getItem("inviteCode")
      .then(value => {
        this.setState({ inviteCode: value });
      })
      .done();
    AsyncStorage.getItem("reminder_date")
      .then(time => {
        this.setState({ reminder_date: time });
      })
      .done();
  }

  getTimeDifference(latestDate, prevDate) {
    if (!latestDate || !prevDate) {
      return "";
    }

    if (latestDate.getTime() < prevDate.getTime()) {
      return true;
    }

    return false;
  }

  renderDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleModal()}
        titleText="Invitation time expired."
        msg={
          "Sorry. Your one week period to invite a friend has expired. You may get another chance - look out for an email from the Raott Team."
        }
      />
    );
  }
  toggleModal() {
    this.setState({
      showDialog: !this.state.showDialog
    });
    this.props.navigation.goBack();
  }

  render() {
    let reminder = Moment(this.state.reminder_date, "YYYY-MM-DD HH:mm:ss");
    let diffInStr = this.getTimeDifference(reminder.toDate(), new Date());

    const { iconStyle, container, contentStyle, mainView, headerView } = styles;

    return (
      <Container style={container}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            style={styles.goBackButton}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon style={{ color: "white" }} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerText}>
            <Text style={{ color: "white" }}>INVITE YOUR FRIENDS</Text>
          </View>
        </View>
        <Content style={contentStyle}>
          <View style={mainView}>
            <View style={headerView}>
              <Text style={{ color: "#c2c2c2", fontSize: 10 }}>
                {" "}
                Invite Code
              </Text>
              <Item
                style={{
                  marginTop: 10,
                  width: deviceWidth - 40,
                  height: 40,
                  backgroundColor: "#f7f7f7",
                  borderRadius: 5
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: 11,
                    color: "#c2c2c2",
                    margin: 10
                  }}
                >
                  {this.state.inviteCode}
                </Text>
              </Item>
            </View>

            <View
              style={{
                marginTop: 10,
                width: deviceWidth - 40,
                alignSelf: "center"
              }}
            >
              <Text style={{ color: "#c2c2c2", fontSize: 10 }}>
                {" "}
                Enter Friend(s)Email Id :
              </Text>
              <Item
                style={{
                  marginTop: 10,
                  width: deviceWidth - 40,
                  height: 40,
                  backgroundColor: "#f7f7f7",
                  borderRadius: 5
                }}
              >
                <Icon active name="md-person" style={iconStyle} />

                <Input
                  placeholder="Email"
                  placeholderTextColor="#c2c2c2"
                  style={{ alignSelf: "center", fontSize: 11 }}
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                />
              </Item>
            </View>
            <ButtonPress onPress={() => this.buttonPress()}>
              Send Invite Code
            </ButtonPress>
          </View>
        </Content>
        {diffInStr && this.state.showDialog && this.renderDialog()}
      </Container>
    );
  }

  buttonPress() {
    const SELF = this;

    if (this.state.email == "") {
      Alert.alert("Invalid Credential", "Please enter Email ");
      this.setState({ isLoading: false });
      this.setState({ email: "" });
    } else {
      AsyncStorage.getItem("apiToken")
        .then(value => {
          axios({
            url: "https://raott.com/api/v2/inviteCodeSend/",
            type: "POST",
            method: "POST",
            timeout: 30000,
            data: {
              email: SELF.state.email
            },
            headers: {
              RaottAuth: value
            }
          })
            .then(function(response) {
              // console.log('response inviteCodeSend');
              // console.log(response);
              if (response.data.status == "success") {
                Alert.alert("Success", "Email Sent");
              } else {
                Alert.alert("Error", response.data.error);
              }
            })
            .catch(function(error) {
              // console.log(error);
              TimeOutError(error, () => SELF.buttonpress());
            });
        })
        .done(); // ending statement of asyncstorage
    }
  }
}

const styles = {
  iconStyle: {
    color: "#e02d2e",
    marginLeft: 10
  },

  container: {
    width: deviceWidth,
    backgroundColor: "white"
  },
  contentStyle: {
    marginTop: 60,
    marginBottom: 10
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: "white"
  },
  headerView: {
    width: deviceWidth - 40,
    alignSelf: "center"
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: Platform.OS === "ios" ? 20 : 0,
    flexDirection: "row"
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: "center",
    justifyContent: "center"
  },

  headerText: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginLeft: 5
  }
};
