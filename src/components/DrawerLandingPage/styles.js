const React = require("react-native");
import { Dimensions } from "react-native";
const { StyleSheet } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "#e7e4e5"
  },
  mb10: {
    marginBottom: 10
  },
  tabStyle: {
    alignItems: "center",
    width: deviceWidth / 2 - 30,
    height: 120,
    borderRadius: 5,
    backgroundColor: "white"
  },
  imageStyle: {
    width: 50,
    height: 50,
    alignSelf: "center",
    marginTop: 20
  },
  textStyle: {
    textAlign: "center",
    top: 10,
    width: 140,
    fontSize: 11,
    backgroundColor: "rgba(0,0,0,0)"
  },
  containerDialog: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(150,150,150, 0.8)",
    alignItems: "center",
    justifyContent: "center"
  },
  viewStyle: {
    flexDirection: "row",
    width: deviceWidth,
    height: 60,
    marginTop: 60,
    alignSelf: "center"
  }
};
