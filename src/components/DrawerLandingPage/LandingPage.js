import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  Alert,
  BackHandler
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import DialogBox from "../dialogBox/";
import Dialog from "../dialog/";
// import FriendlyReminder from '../dialogBox/FriendlyReminder.js'
import RoattCoins from "../RaottCollections/RoattCoins";
import AccountsDetail from "../ViewTransaction/AccountsDetail";
import styles from "./styles";
import DrawerHeader from "../common/drawerHeader/drawerHeader";
import FriendRequest from "../common/drawerHeader/FriendRequest";
import Deals from "../TreasureChest/Deals.js";
import TransferCoinToAny from "../TransferCoinToAny/";
import FriendsList from "../TransferCoinToFriends/FriendsList.js";
import Category from "../TreasureHunt/Category.js";
// import MapUI from '../TreasureHunt/MapUI.js'
import DealHistory from "../DealHistory/DealHistory.js";
import axios from "axios";
import Loading from "../Loading/";
import Moment from "moment";
import SymbolDialog from "./SymbolDialog";
import { setUser } from "../../actions/user";
import numeral from "numeral";
import { InternetCheck, TimeOutError } from "../common/";
import _ from "lodash";
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

class LandingPage extends Component {
  // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      fullName: " ",
      homeBalance: " ",
      raottsBalance: "",
      profileImage: "",
      isLoading: true,
      firstTimeUser: "",
      selectedCoin: null,
      isVisible: false,
      isVisibleInvite: true,
      isVisibleCongratulations: false,
      membership_status: "",
      showCongratulationsDialog: false,
      symbolConfirmationDialog: false,
      homeCurrency: "",
      timeoutDone: false
    };

    this.categoryClick = _.debounce(this.categoryClick.bind(this), 1000, {
      leading: true,
      trailing: false
    });
    this.yourTreasureCollectionClick = _.debounce(
      this.yourTreasureCollectionClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.treasureChestClick = _.debounce(
      this.treasureChestClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.transferFriendsClick = _.debounce(
      this.transferFriendsClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.transferAnyoneClick = _.debounce(
      this.transferAnyoneClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.raottCollectionClick = _.debounce(
      this.raottCollectionClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.viewTransactionClick = _.debounce(
      this.viewTransactionClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this.dealHistoryClick = _.debounce(this.dealHistoryClick.bind(this), 1000, {
      leading: true,
      trailing: false
    });
    this.homeCurrencyClick = _.debounce(
      this.homeCurrencyClick.bind(this),
      1000,
      { leading: true, trailing: false }
    );
    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentWillMount() {
    InternetCheck();
    this.getData();
  }

  backAndroidPress() {
    BackHandler.exitApp();
    return true;
  }

  componentDidMount() {
    FCM.on(FCMEvent.Notification, notif => {
      if (notif.fcm && notif.fcm.body) {
        /* Create local notification for showing in a foreground */
        FCM.presentLocalNotification({
          body: notif.fcm.body,
          priority: "high",
          title: notif.fcm.title,
          sound: "default",
          badge: 0,
          show_in_foreground: true /* notification when app is in foreground (local & remote)*/,
          vibrate: 300 /* Android only default: 300, no vibration if you pass null*/,
          large_icon: "ic_launcher", // Android only
          icon: "ic_launcher", // as FCM payload, you can relace this with custom icon you put in mipmap
          color: "#e02d2e",
          status: notif.status
        });
      }
    });
    FCM.setBadgeNumber(0); // iOS and supporting android.
    FCM.getBadgeNumber().then(number => console.log(number));
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  // componentDidMount() {
  //   setInterval( () => {
  //     this.forceUpdate();
  //   }, 1000);

  // }

  openDrawer() {
    this.props.navigation.navigate("DrawerOpen");
  }

  toggleModal(e) {
    let id = 0;
    try {
      AsyncStorage.setItem("selectedCoin", e);
    } catch (error) {
      // Error saving data
    }
    this.setState({ isVisible: !this.state.isVisible });
    e = e.toString().toLowerCase();

    if (e == "cup") {
      id = 2;
    } else if (e == "crown") {
      id = 1;
    } else if (e == "ring") {
      id = 3;
    } else {
      id = null;
    }
    this.selectUserCoin(id, e);
  }
  categoryClick() {
    this.props.navigation.navigate("Category", {
      screen: "LandingPage",
      onSelect: this.onSelect
    });
  }
  homeCurrencyClick() {
    this.props.navigation.navigate("HomeCurrency", { screen: "HomeCurrency" });
  }
  treasureChestClick() {
    this.props.navigation.navigate("Deals", {
      screen: "LandingPage",
      onSelect: this.onSelect
    });
  }
  yourTreasureCollectionClick() {
    this.props.navigation.navigate("Treasure", {
      screen: "LandingPage",
      onSelect: this.onSelect
    });
  }
  transferFriendsClick() {
    this.props.navigation.navigate("FriendsList", { onSelect: this.onSelect });
  }
  transferAnyoneClick() {
    this.props.navigation.navigate("TransferCoinToAny", {
      onSelect: this.onSelect,
      navigationFrom: "LandingPage"
    });
  }
  raottCollectionClick() {
    this.props.navigation.navigate("RoattCoins", {
      onSelect: this.onSelect,
      headerTitle: "World Raott Coins",
      screen: "RaottCoins"
    });
  }
  viewTransactionClick() {
    this.props.navigation.navigate("AccountsDetail", {
      onSelect: this.onSelect
    });
  }
  dealHistoryClick() {
    this.props.navigation.navigate("DealHistory", { onSelect: this.onSelect });
  }
  toggleModalInvite() {
    this.setState({
      isVisibleInvite: !this.state.isVisibleInvite
    });
  }

  getTimeDifference(latestDate, prevDate) {
    if (!latestDate || !prevDate) {
      return "";
    }
    if (latestDate.getTime() < prevDate.getTime()) {
      return "";
    }
    const duration = Moment.duration(Moment(latestDate).diff(Moment(prevDate)));
    const days = Math.floor(duration.asDays());
    let hours = Math.floor(duration.asHours());
    let minutes = Math.floor(duration.asMinutes());
    let seconds = Math.floor(duration.asSeconds());
    hours -= days * 24;
    minutes -= (days * 24 + hours) * 60;
    seconds -= ((days * 24 + hours) * 60 + minutes) * 60;
    let str = "";
    if (days > 0) {
      str += days + (days > 1 ? " days " : " day ");
    }
    if (hours > 0 || days > 0) {
      str += hours + (hours > 1 ? " hours " : " hour ");
    }
    if (minutes > 0 || hours > 0 || days > 0) {
      str += minutes + (minutes > 1 ? " minutes " : " minute ");
    }
    if (seconds > 0 || minutes > 0 || hours > 0 || days > 0) {
      str += seconds + (seconds > 1 ? " seconds " : " second ");
    }
    return str;
  }

  renderCongratulationsDialog() {
    return (
      <Dialog
        method={() => this.toggleModalCongratulations()}
        titleText="Congratulations!"
        msg={
          "Congrats! You are now a full member of Raotts. You can start buying deals using World Raott Coins and transfer World Coins to anyone"
        }
      />
    );
  }

  toggleModalCongratulations() {
    this.setState({
      isVisibleCongratulations: !this.state.isVisibleCongratulations
    });
  }

  renderSymbolConfirmationDialog() {
    return (
      <SymbolDialog
        method={(...a) => this.toggleSymbolConfirmation(...a)}
        titleText=""
        msg={`Are you sure you want to select ${
          this.state.selectedCoin
        } Symbol ?`}
      />
    );
  }

  toggleSymbolConfirmation(choose) {
    if (choose == "OK") {
      this.setState({
        symbolConfirmationDialog: !this.state.symbolConfirmationDialog
      });
    } else {
      this.setState({
        symbolConfirmationDialog: !this.state.symbolConfirmationDialog,
        selectedCoin: null,
        isVisible: true
      });
    }
  }
  onSelect = data => {
    this.getData();
  };

  onClickFriendRequest() {
    this.props.navigation.navigate("FriendRequest");
  }

  localToUtc(date, format) {
    if (date && format) {
      const dateMoment = Moment(date, format);
      if (!dateMoment) return "";
      const dateUtc = dateMoment.utc().format(format);
      return dateUtc;
    }
    return date;
  }

  utcToLocal(date, format) {
    if (date && format) {
      const dateMoment = Moment.utc(date, format);
      if (!dateMoment) return "";
      const dateLocal = dateMoment.local().format(format);
      return dateLocal;
    }
    return date;
  }

  render() {
    // console.log(this.props, 'this.props');

    // console.log(this.localToUtc(this.state.reminder_date,'YYYY-MM-DD HH:mm:ss'),"((((((((((((((((&&&&&&&&&&&&");
    let remiderUtc = this.localToUtc(
      this.state.reminder_date,
      "YYYY-MM-DD HH:mm:ss"
    );
    let reminder = Moment(remiderUtc, "YYYY-MM-DD HH:mm:ss");
    let diffInStr = this.getTimeDifference(reminder.toDate(), new Date());
    // console.log(reminder.toDate(),'TIME++++++',new Date(),'******',diffInStr)

    //     console.log(this.state.homeCurrency, ' homeCurrency')
    //     console.log(this.state.symbolConfirmationDialog, 'symbolConfirmationDialog')
    //     console.log(this.state.isVisibleCongratulations, 'isVisibleCongratulations')
    return (
      <Container style={styles.container}>
        <DrawerHeader
          openDrawer={() => this.openDrawer()}
          navigation={this.props.navigation}
          username={this.state.fullName}
          pImage={this.state.profileImage}
          onClickFriendRequest={() => this.onClickFriendRequest()}
        />

        <View style={styles.viewStyle}>
          <TouchableOpacity onPress={this.homeCurrencyClick}>
            <View
              style={{
                width: deviceWidth / 2,
                alignItems: "center",
                marginTop: 10,
                height: 40
              }}
            >
              <Text style={{ color: "#e02d2e", fontSize: 14 }}>
                {" "}
                WORLD RAOTT{" "}
              </Text>
              <Text numberOfLines={1} style={{ fontSize: 12 }}>
                {" "}
                R {numeral(this.state.raottsBalance).format("0,0")}{" "}
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: 1,
              height: 30,
              marginTop: 10,
              backgroundColor: "#e02d2e"
            }}
          />

          <TouchableOpacity onPress={this.homeCurrencyClick}>
            <View
              style={{
                width: deviceWidth / 2,
                alignItems: "center",
                marginTop: 10,
                height: 40,
                marginLeft: 10
              }}
            >
              <Text style={{ color: "#e02d2e", fontSize: 14 }}>
                {" "}
                HOME CURRENCY
              </Text>
              <Text numberOfLines={1} style={{ fontSize: 12 }}>
                {" "}
                {numeral(this.state.homeBalance).format("0,0.00")}{" "}
                {this.state.homeCurrency}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <ScrollView>
          <View style={{ alignItems: "center", marginBottom: 15 }}>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity onPress={this.categoryClick}>
                <View style={[styles.tabStyle, { justifyContent: "center" }]}>
                  <Image
                    // style={{ width: 80, height: 80, marginTop: -10 }}
                    style={[
                      styles.imageStyle,
                      {
                        alignSelf: "center",
                        marginTop: 0,
                        width: 60,
                        height: 60
                      }
                    ]}
                    source={require("../../../img/treasureHunt.png")}
                  />
                  <Text style={styles.textStyle}>Treasure Hunt</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ marginLeft: 20 }}
                onPress={this.treasureChestClick}
              >
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/treasurechest.png")}
                  />
                  <Text style={styles.textStyle}>Treasure Chest</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "row", marginTop: 15 }}>
              <TouchableOpacity onPress={this.transferFriendsClick}>
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/potcoin.png")}
                  />
                  <Text style={styles.textStyle}>
                    Transfer World Raott to friends
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.transferAnyoneClick}
                style={{ marginLeft: 20 }}
              >
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/potcoin.png")}
                  />
                  <Text style={styles.textStyle}>
                    Transfer World Raott to anyone{" "}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "row", marginTop: 15 }}>
              <TouchableOpacity onPress={this.raottCollectionClick}>
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/raottcollection.png")}
                  />
                  <Text style={styles.textStyle}>
                    View all your Raott Collections
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.viewTransactionClick}
                style={{ marginLeft: 20 }}
              >
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/viewtransaction.png")}
                  />
                  <Text style={styles.textStyle}>View Transactions</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "row", marginTop: 15 }}>
              <TouchableOpacity onPress={this.dealHistoryClick}>
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/dealhistory.png")}
                  />
                  <Text style={styles.textStyle}>Deal History</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.yourTreasureCollectionClick}
                style={{ marginLeft: 20 }}
              >
                <View style={styles.tabStyle}>
                  <Image
                    style={styles.imageStyle}
                    source={require("../../../img/potcoin.png")}
                  />
                  <Text style={styles.textStyle}>
                    Your Treasure Collections
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        {this.state.selectedCoin !== null &&
          this.state.isVisibleInvite &&
          this.state.membership_status == "0" &&
          this.state.reminder_date !== "" &&
          diffInStr !== "" && (
            <Dialog
              method={() => this.toggleModalInvite()}
              titleText="A friendly reminder"
              msg={`You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins and buy deals using World Raott coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member. You have remaining time ${diffInStr}`}
              msg={`You are still a guest user of Raotts. Only Full Members can send and receive World Raott Coins and play the Treasure Hunt. Please allow 24 hours for processing to be approved as a Full Member.`}
            />
          )}
        {this.state.selectedCoin === null ||
          (this.state.selectedCoin.toString().length === 0 &&
            this.state.isVisible && (
              <DialogBox
                method={(...a) => this.toggleModal(...a)}
                titleText="Select one symbol"
                msg="The World Raott Coins were minted in 3 powerful symbols . The Crown, the Cup and the Ring."
                msg1="Please decide carefully and choose one symbol to start earning and collecting World Raott Coins."
                msg2="Remember, you can only win deals and cash if you get the right World Raott Coin combination of your chosen symbol. Also note that once you have selected a symbol you cannot change it."
                msg3="Please click below and choose one symbol to start your earnings and collection."
              />
            ))}
        {this.state.isVisibleCongratulations &&
          this.renderCongratulationsDialog()}
        {this.state.symbolConfirmationDialog &&
          this.renderSymbolConfirmationDialog()}
        {<Loading isLoading={this.state.isLoading} />}
      </Container>
    );
  }

  getData() {
    const SELF = this;
    if (!this.state.isLoading) {
      SELF.setState({ isLoading: true, timeoutDone: false });
    }
    AsyncStorage.getItem("apiToken")
      .then(value => {
        axios({
          url: "https://raott.com/api/v2/screenHome/",
          type: "GET",
          method: "GET",
          timeout: 30000,
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            if (response.data.status == "success") {
              AsyncStorage.getItem("membership_status").then(value => {
                if (
                  value &&
                  value != response.data.userinfo.membership_status
                ) {
                  SELF.setState({ isVisibleCongratulations: true });
                }
              }).done;
              if (response.data.userinfo.membership_status == "0") {
                //   if(response.data.reminder_date !== null){
                try {
                  AsyncStorage.setItem("inviteCode", response.data.invite_code);
                  AsyncStorage.setItem(
                    "membership_status",
                    response.data.userinfo.membership_status
                  );
                  AsyncStorage.setItem(
                    "reminder_date",
                    response.data.reminder_date
                  );
                  AsyncStorage.setItem(
                    "fullName",
                    response.data.userinfo.fullname
                  );
                } catch (error) {
                  // Error saving data
                }
              } else {
                try {
                  AsyncStorage.setItem(
                    "membership_status",
                    response.data.userinfo.membership_status
                  );
                  AsyncStorage.setItem(
                    "fullName",
                    response.data.userinfo.fullname
                  );
                } catch (error) {
                  // Error saving data
                }
              }
              if (
                response.data.selectedCoin &&
                response.data.selectedCoin !== ""
              ) {
                try {
                  let type = "";
                  if (response.data.selectedCoin == 2) {
                    type = "Cup";
                  } else if (response.data.selectedCoin == 1) {
                    type = "Crown";
                  } else if (response.data.selectedCoin == 3) {
                    type = "Ring";
                  }
                  AsyncStorage.setItem("selectedCoin", type);
                } catch (error) {
                  // Error saving data
                }
              }
              let fullName = "";
              if (response.data.userinfo.fullname == "") {
                fullName = response.data.userinfo.username;
              } else {
                fullName = response.data.userinfo.fullname;
              }
              let visibility = "";
              if (response.data.selectedCoin) {
                visibilty = false;
              } else {
                visibilty = true;
              }
              SELF.props.setUser({
                membership_status: response.data.userinfo.membership_status,
                reminder_date: response.data.reminder_date,
                userid: response.data.userid
              });
              SELF.setState({
                fullName: fullName,
                homeBalance: response.data.homeBalance,
                raottsBalance: response.data.raottsBalance,
                profileImage: response.data.userinfo.profile_image,
                selectedCoin: response.data.selectedCoin,
                membership_status: response.data.userinfo.membership_status,
                reminder_date: response.data.reminder_date,
                homeCurrency: response.data.homeCurrency,
                isVisible: visibilty,
                isLoading: false
              });
            }
          })
          .catch(function(error) {
            // console.log(error , ' ', SELF.state.timeoutDone);
            if (!SELF.state.timeoutDone) {
              TimeOutError(error, () => SELF.getData());
            }
            SELF.setState({ isLoading: false, timeoutDone: true });
          });
      })
      .done(); // ending statement of asyncstorage
  }

  selectUserCoin(id, e) {
    const SELF = this;
    let idFetch = id;
    let coinFetch = e;
    AsyncStorage.getItem("apiToken")
      .then(value => {
        // console.log(value);

        axios({
          url: "https://raott.com/api/v2/userCoin/",
          type: "POST",
          method: "POST",
          timeout: 30000,
          data: {
            symbol: id
          },
          headers: {
            RaottAuth: value
          }
        })
          .then(function(response) {
            // console.log('response userCoin');
            // console.log(response);
            if (response.data.status == "1") {
              SELF.setState({
                symbolConfirmationDialog: true,
                selectedCoin: e
              });
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.selectUserCoin(idFetch, coinFetch));
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

function bindAction(dispatch) {
  return {
    setUser: data => dispatch(setUser(data))
  };
}

const mapStateToProps = state => ({
  user: state.user.data
});

LandingPage.navigationOptions = {
  drawerLockMode: "locked-closed"
};

export default connect(
  mapStateToProps,
  bindAction
)(LandingPage);
