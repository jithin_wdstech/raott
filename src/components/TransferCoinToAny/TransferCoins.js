import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
  Item,
  Input
} from "native-base";
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  BackHandler
} from "react-native";
import { DrawerNavigator } from "react-navigation";
import dialogBox from "../dialogBox/";
import styles from "./styles";
import CoinsDetail from "./CoinsDetail.js";
import DialogNotMember from "../TransferCoinToFriends/DialogNotMember.js";
import { connect } from "react-redux";
import axios from "axios";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

class TransferCoins extends Component {
  onClick() {
    if (this.props.user && this.props.user.membership_status == 0) {
      this.setState({
        showDialogGuestUserStatus: true,
        clickedFriend: username
      });
    } else if (membership_status == "0") {
      this.setState({ showDialog: true, clickedFriend: username });
    } else {
      this.props.navigation.navigate("RoattCoins", {
        headerTitle: headerTitle + username,
        screen: "FriendList",
        fid: fid
      });
    }
  }

  renderDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleModal()}
        titleText=" Not a Member"
        msg={`You cannot transfer World Roatt to ${this.state.clickedFriend}.${
          this.state.clickedFriend
        } is still a guest user of Raotts. Only Full members can send and receive World Raott Coins.  `}
      />
    );
  }

  toggleModal() {
    this.setState({
      showDialog: !this.state.showDialog
    });
  }

  renderDialogGuestUserStatus() {
    return (
      <DialogNotMember
        method={() => this.toggleModalGuestUserStatus()}
        titleText="Error Transfer Coins"
        msg={`You cannot transfer World Roatt to ${
          this.state.clickedFriend
        }. You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member.  `}
      />
    );
  }
  toggleModalGuestUserStatus() {
    this.setState({
      showDialogGuestUserStatus: !this.state.showDialogGuestUserStatus
    });
  }

  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      friends: [],
      showDialog: false,
      showDialogGuestUserStatus: false
    };
    let username = "";
    let fid = "";
    let membership_status = "";

    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  componentDidMount() {
    console.log("Category page", this.props);
    BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this._backAndroidPress
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }
  render() {
    console.log(
      "response - -- -" +
        JSON.stringify(this.props.navigation.state.params.singleFriend)
    );
    let singleFriend = this.props.navigation.state.params.singleFriend;
    // console.log(singleFriend)
    username = singleFriend.friend.username;
    fid = singleFriend.friend.userid;
    membership_status = singleFriend.friend.membership_status;

    return (
      <Container style={styles.container}>
        <View style={styles.headerStyle}>
          <TouchableOpacity
            onPress={() => this.goBack()}
            style={styles.goBackButton}
          >
            <Icon style={{ color: "white" }} name="arrow-back" />
          </TouchableOpacity>
          <View transparent style={styles.headerTitle}>
            <Text style={{ color: "white" }}> Friend Detail </Text>
          </View>
        </View>
        <CoinsDetail
          singleFriend={singleFriend}
          onClick={() => this.onClick()}
        />

        {this.state.showDialog && this.renderDialog()}
        {this.state.showDialogGuestUserStatus &&
          this.renderDialogGuestUserStatus()}
      </Container>
    );
  }
  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onRefresh) {
      navigation.state.params.onRefresh();
    }
  }
}

function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data
});

export default connect(
  mapStateToProps,
  bindAction
)(TransferCoins);
