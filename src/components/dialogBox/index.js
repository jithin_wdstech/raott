import React from "react";
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image
} from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const DialogBox = (props, children) => {
  return (
    <View style={styles.container}>
      <View style={{ width: deviceWidth - 40, backgroundColor: "#f7f7f7" }}>
        <View
          style={{
            width: deviceWidth - 40,
            backgroundColor: "#e02d2e",
            flexDirection: "row",

            justifyContent: "space-between",
            height: 50,
            alignItems: "center"
          }}
        >
          <Text
            style={{
              margin: 10,
              color: "white",
              textAlign: "center",
              fontSize: 15
            }}
          >
            {" "}
            {props.titleText}
          </Text>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            {/*<Image style={{marginRight:10,width:20,height:20,marginTop:0 ,justifyContent:'space-between'}} source={require('../../../img/closeicon.png')}>
       
    
       </Image>*/}
          </TouchableOpacity>
        </View>

        <Text style={{ margin: 10, color: "black", fontSize: 10 }}>
          {props.msg}
        </Text>
        <Text style={{ margin: 10, color: "black", fontSize: 10 }}>
          {props.msg1}
        </Text>
        <Text
          style={{
            marginLeft: 10,
            marginRight: 10,
            marginBottom: 10,
            color: "black",
            fontSize: 10
          }}
        >
          {props.msg2}
        </Text>
        <Text
          style={{
            marginLeft: 10,
            marginRight: 10,
            marginBottom: 10,
            color: "black",
            fontSize: 10
          }}
        >
          {props.msg3}
        </Text>
        <View
          style={{
            width: deviceWidth - 40,
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "center",
            alignSelf: "center",
            margin: 10
          }}
        >
          <TouchableOpacity onPress={() => props.method("Crown")}>
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../../img/rsz_gold_crown.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => props.method("Cup")}>
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../../img/rsz_gold_cup.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => props.method("Ring")}>
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../../img/rsz_gold_ring.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

DialogBox.propTypes = {
  size: PropTypes.string
};

DialogBox.defaultProps = {
  size: "large"
};

export default DialogBox;
