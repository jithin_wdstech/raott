import React from 'react';
import { View, ActivityIndicator, Dimensions,Text,Button,TouchableOpacity,Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const FriendlyReminder = (props,children) => {


  
  if(props.isVisible == false){
    return null;
  }

  
  return (
    <View style={styles.container}>


      <View style={{width:deviceWidth-40, backgroundColor: '#f7f7f7'}}>
   <View style={{width:deviceWidth-40,backgroundColor:'#e02d2e',flexDirection:'row', 
       
        justifyContent: 'space-between',height:50,alignItems:'center'}}>
   <Text style={{ margin:10,color:'white',textAlign:'center'}} > {props.titleText}</Text>
    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
       {/*<Image style={{marginRight:10,width:20,height:20,marginTop:0 ,justifyContent:'space-between'}} source={require('../../../img/closeicon.png')}>
       
    
       </Image>*/}
    
    </TouchableOpacity>
   </View>
        
        <Text style={{margin:10,color:'black',fontSize:10 }}>{props.msg}</Text>
        {/*<Text style={{margin:10,color:'black',fontSize:10}}>{props.msg1}</Text>
        <Text style={{marginLeft:10,marginRight:10,marginBottom:10,color:'black',fontSize:10}}>{props.msg2}</Text>
         <View style={{ flexDirection: 'row',alignItems: 'center',alignSelf: 'center',margin:30}}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate("DealHistory")}><Image style={{width:100,height:100}} source={require('../../../img/rsz_gold_cup.png')} /></TouchableOpacity>
           <Image style={{width:100,height:100}} source={require('../../../img/rsz_gold_crown.png')} />
            <Image style={{width:100,height:100}} source={require('../../../img/rsz_gold_ring.png')} />
</View>*/}
 <Text style={{margin:10,color:'black',fontSize:10,alignItems:'left' }}>Close</Text>



      </View>

    </View>
  );
};

FriendlyReminder.propTypes = {
  size: PropTypes.string,
};

FriendlyReminder.defaultProps = {
  size: 'large',
};

export default FriendlyReminder;


