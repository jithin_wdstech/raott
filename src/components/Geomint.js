import React, { Component } from 'react'
import { ButtonPress } from './common'
import {
    Text, View, Dimensions, Image, ScrollView,BackHandler
} from 'react-native'
import {
    Container, Header, Title, Content, Button, Icon, Left, Right, Body, ListItem,
    List, Card, CardItem, Item, Input
} from "native-base";
import { InternetCheck } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
class Geomint extends Component {
    constructor(props) {
        super(props);
        this._backAndroidPress = this.backAndroidPress.bind(this);
    }
    componentWillMount() {
        InternetCheck();
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this._backAndroidPress)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this._backAndroidPress)
    }

    backAndroidPress() {

        this.props.navigation.navigate("LandingPage");
        return true;
    }
    render() {
        return (
            <Container style={styles.mainContainer}>
                <View>
                    <Header style={styles.headerStyle} >
                        <Left style={{flex:1}}>
                            <Button
                                transparent onPress={() => this.props.navigation.navigate('DrawerOpen', { test: '123' })} >
                                <Icon name="menu" style={{ fontSize: 25,color: '#e02d2e' }} />
                            </Button>
                        </Left>
                        <Body style={{flex:10, alignItems:'center'}}>
                            <Title style={{ color: '#e02d2e' }}>What is GeoMint™?</Title>
                        </Body>
                        {/* <Right style={{ alignItems: 'center', }}></Right> */}
                    </Header>
                </View>

                <View style={styles.textViewStyle}>
                    {/* <Text style={styles.textHeaderStyle}>
                        Important Note:
                    </Text> */}
                    <Text style={styles.textStyle}>
                    GeoMint™ is finding World Raott Coins hidden in different Geographical locations by playing the World Raott’s Biggest Treasure Hunt available through our website or app.
                    </Text>
                    <Text style={styles.textStyle}>
                    There are exactly 148,940,000 coins to be GeoMinted. This is the approximate land surface area of earth in square kilometres. The treasure hunt concludes as soon as all these coins have been GeoMinted, or when we list in the Forex market, whichever comes first!
	                </Text>
                    
                </View>
            </Container>

        )
    }
}
const styles = {
    mainContainer: {
        flex: 1,
        width: deviceWidth,
        backgroundColor: 'white'
    },
    headerStyle: {
        backgroundColor: '#e7e4e5',
    },
    textViewStyle: {
        flex: 1,
        margin:16
    },
    textHeaderStyle: {
        color: '#e02d2e',
        fontSize: 18,
        lineHeight: 24,
        fontWeight: 'bold',
        marginTop:20,
        marginBottom:20,
        textDecorationLine:'underline'
    },
    textStyle:{ 
        fontSize: 14, 
        lineHeight: 18, 
        // backgroundColor: 'yellow', 
        marginBottom:10 
    }
}

Geomint.navigationOptions = {
    header: null,
    gesturesEnabled: false,
    drawerLockMode: 'locked-closed'
};
export default Geomint

