package com.raottsworld;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;

import android.content.Intent;

import java.util.List;


import android.content.Intent;
import android.net.Uri;

import android.provider.Settings;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.burnweb.rnpermissions.RNPermissionsPackage;

import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "raottsWorld";
  }

  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
       return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
    };
  }

  	private void _askForOverlayPermission() {
	    if (!BuildConfig.DEBUG || android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
	    	return;
	    }

	    if (!Settings.canDrawOverlays(this)) {
	        Intent settingsIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
	                Uri.parse("package:" + getPackageName()));
	        startActivityForResult(settingsIntent, 2);
	    }
	}

	@Override
  	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
      	RNPermissionsPackage.onRequestPermissionsResult(requestCode, permissions, grantResults); // very important event callback 
      	super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  	}

  	@Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Checking permissions on init
        _askForOverlayPermission();
    }
}
